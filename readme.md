## **Team 2 Project Area**

- [Team members](#team-members)
- [Product Owner](#product-owner)
- [Faculty](#faculty)
- [Technical details](#technical-details)
- [Microservices](#microservices)
- [Project plan](#project-plan)

# Team members

* Andrea Hu
* Behrooz Amirzadeh
* Jorge Cotillo
* Loi Cheng
* Robert Terry
* Tofik Sultan

# Product Owner

* Nitya Timalsina

# Faculty

* Annie Kamlang
* Eric Gieseke
* Hannah Riggs

# Technical details

* Programming language
  * Java
  * Javascript
* Naming convention
  * camelCase
  * dash-spaces
* Code Documentation
  * Swagger
  * Java
* Implementation principles
  * docker
  * kubernetes
  * gitlab CI/CD

# Microservices

* Blockchain Services https://www.onestepprojects.tk/onestep/blockchain/
* Authentication Services https://www.onestepprojects.tk/onestep/authentication/
* Accreditation Services https://www.onestepprojects.tk/onestep/accreditation/
* Rewards Services https://www.onestepprojects.tk/onestep/rewards/

# Project plan

* Sprint 1
  * Blockchain Services
  * Authentication Services
* Sprint 2
  * Accreditation Services
* Sprint 3
  * Rewards Services 
