# Creating the dropwizard docker image

First install docker desktop - see instructions on website. https://www.docker.com/

For sample project see https://gitlab.com/cscie599sec1spring21/team-2-project-area/-/tree/main/blockchain-service

## 1) Dockerfile

Once we had an app working on localhost, it can be containzerized and deployed.  Using blockchain-service as example, we took the blockchain-service-dockerfile and edit the configs.  The first line specifies what image to start with.  If using the latest dropwizard, we can start with a maven maven:3.6.3-openjdk-15 image.  The dockerfile looks like below

```
#blockchain-service-dockerfile
FROM maven:3.6.3-openjdk-15 
#image to start with, depending on what you need
#images can be found on https://hub.docker.com/search?q=maven&type=image

COPY . /blockchain-service
#command to copy all the files relative to this dockerfile's location to a directory 
#inside the docker image.  

#startup commands
#run the startup commands in the entrypoint file that was copied over to the image
ENTRYPOINT ["sh","blockchain-service-entrypoint.sh"]
```

The entrypoint file contains the commands to launch dropwizard, which is the same as if done on localhost

```
#blockchain-service-entrypoint.sh
#!/bin/sh
mvn clean install
java -jar target/blockchain-1.0-SNAPSHOT.jar server
```
Any specific project commands were modified, like ```blockchain-1.0-SNAPSHOT.jar```
 
<br>

## 2) Docker Compose

We used docker-compose to launch a container, which is an instance of the image, like how EC2 instances are launched from AWS images.  The configuration is in ```blockchain-service-compose.yml```

```
#blockchain-service-compose.yml
version: '3.7'

services:
  blockchain-service: ##name of service, can be anything 
    restart: always ##always restart the container, like if the server reboots
    build: 
      context: . ##alll the subsequent commands here are relative to this yml's path
      dockerfile: blockchain-service-dockerfile ##use the dockerfile we just made
    env_file:
      - "blockchain-service-env_test.env"  ##add this if you want to use an env file to 
      ## pass  your tokens, apikeys, etc. into your server as environment variables.  
      ## Otherwise remove this part
    ports:
      - "8080:8080"  ## the default dropwizard ports
      - "8081:8081"
```

The dockerfile, docker-compose file, and entrypoint file should be placed in the proper location relative to our dropwizard files.

## 3) Test the image
We built and ran the image into a container locally with this command, with ```$FILE``` being the yml file name, which is ```blockchain-service-compose.yml```
```
FILE=blockchain-service-compose.yml
docker-compose -f $FILE down && docker-compose -f $FILE build --no-cache && docker-compose -f $FILE up
```
The app should be up and running on localhost.  If everything works as expected, the image can be pushed to aws.  Shutdown and delete the container with this command
```
FILE=blockchain-service-compose.yml
docker-compose -f $FILE down
```
These commands will be helpful during the process
```
docker images #list images on your machine
docker ps -a #list all created containers
```
Alternatively to the command line, docker-desktop, visual studio code, or intellij can manage the docker images and containers, see links below:
#### https://www.jetbrains.com/help/idea/docker.html
#### https://code.visualstudio.com/docs/containers/overview

