//import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import Amplify, { Auth, Hub } from 'aws-amplify';


export class AuthHelper {

    async Get(uri : string) {
        try {
            let response =
                await Axios.get(uri, { headers: await this.GetAuthToken() });
            console.log(response);
            return response; 
        } catch (error) {
            console.log(error);
            console.log(`Not able to execute get call, error: ${error}`);
            return error.response;
        }
    }

    async Put(uri : string, data? : any) {
        try {
            let response =
                await Axios.put(uri, data, { headers: await this.GetAuthToken() });
            console.log(response);
            return response;  
        } catch (error) {
            console.log(`Not able to execute get call, error: ${error}`);
        }
    }

    async Patch(uri : string, data? : any) {
        try {
            let response =
                await Axios.patch(uri, data, { headers: await this.GetAuthToken() });
            console.log(response); 
            return response;  
        } catch (error) {
            console.log(`Not able to execute get call, error: ${error}`);
        }
    }

    async Delete(uri : string) {
        try {
            let response =
                await Axios.delete(uri, { headers: await this.GetAuthToken() });
            console.log(response);
            return response;  
        } catch (error) {
            console.log(`Not able to execute get call, error: ${error}`);
        }
    }

    async GetUserAccessToken() {
        try {
            let userInfo = await this.GetUser();
            return userInfo?.signInUserSession.accessToken.jwtToken;
        } catch (error) {
            console.log(`Not able to retrieve user information, error: ${error}`);
        }
    }

    private async GetUser() {
        try {
            return await Auth.currentAuthenticatedUser();
        } catch (error) {
            console.log(`Not signed in, error: ${error}`);
        }
    }

    private async GetAuthToken() {
        try {
            let token = await this.GetUserAccessToken();
            let authHeader = {
                "Authorization" : `${token}` 
            };

            console.log(authHeader);

            return authHeader;
                
        } catch (error) {
            console.log(`Not able to retrieve access token, error: ${error}`);
        }
    }
}
