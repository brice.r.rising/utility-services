import ReactDOM from 'react-dom';
import React, { useEffect, useState } from 'react';
import Amplify, { Auth, Hub } from 'aws-amplify';
import { AuthHelper } from './common/AuthHelper'
import { RewardsWidget, RewardsWidgetNavBar } from "@cscie599sec1spring21/rewards-widget";
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";

Amplify.configure(
  {
      userPoolId: 'us-west-2_B9oGErwyx',
      region: 'us-west-2',
      identityPoolRegion: 'us-west-2',
      userPoolWebClientId: '6vuub2llosaphdiq71tsv5qcqc',
      oauth: {
          domain: 'identity-onesteprelief-dev-dev.auth.us-west-2.amazoncognito.com',
          scope: [
              'phone',
              'email',
              'openid',
              'profile',
              'aws.cognito.signin.user.admin'
          ],
          redirectSignIn: 'http://localhost:3000/',
          redirectSignOut: 'http://localhost:3000/',
          responseType: 'code'
      },
  });

function App() {
  var authHelper = new AuthHelper();
  const [user, setUser] = useState(null);
  useEffect(() => {

    async function setAuthenticatedUser() {
      try {
        var authenticateUser = await Auth.currentAuthenticatedUser();
        setUser(authenticateUser);
      } catch (error) {
        console.log('Not authenticated, redirecting to authentication page.');
        //Auth.federatedSignIn();
      }
    }

    setAuthenticatedUser();

    Hub.listen('auth', async ({ payload: { event, data } }) => {
        switch (event) {
          case 'signIn':
          case 'cognitoHostedUI':
              try {
                  var authenticateUser = await Auth.currentAuthenticatedUser();
                  setUser(authenticateUser);
              } catch (error) {
                  // Note(jcotillo): At this point user should be authenticated, getting an error
                  // means there could be another failure which we should log.
                  console.log(`Not signed in when it should, error: ${error}`);
              }
              break;
          case 'signOut':
              setUser(null);
              break;
          case 'signIn_failure':
          case 'cognitoHostedUI_failure':
              console.log('Sign in failure', data);
              break;
          default:
            console.log(`Event: ${event} not supported`);
        }
    });
  }, []);
  
  async function getCall() {    
    var response = 
      await authHelper.Get('http://localhost:9003/is-authorized?permission=SampeGroup');
    console.log(response);
    if (response.status === 200) {
      const element = (
        <div>
          <h1>Hello, world! I am authorized to show you this info.</h1>
        </div>
      );
      ReactDOM.render(element, document.getElementById('response'));
    }
    else {
      const element = (
        <div>
          <h1>Hello, world! I am NOT authorized to show you this info.</h1>
        </div>
      );
      ReactDOM.render(element, document.getElementById('response'));
    }
  }

  return (
    <div>
      <p>User: {user ? JSON.stringify(user.attributes) : 'None'}</p>
      <RewardsWidget entityId="aade4d3a-6116-475b-a6fa-8c3ee8ef5e5f" width="50"/>
      <RewardsWidgetNavBar entityId="aade4d3a-6116-475b-a6fa-8c3ee8ef5e5f" />
      {user ? 
        (
          <><button onClick={() => Auth.signOut()}>Sign Out</button>
            <button onClick={() => getCall()}>Http call</button>
            <div>
                <a href='https://identity-onesteprelief-dev-dev.auth.us-west-2.amazoncognito.com/login?client_id=56oth90258ft4tequecv90nqh5&response_type=code&scope=aws.cognito.signin.user.admin+email+openid+phone+profile&redirect_uri=https://localhost:44316/'>Fund Service</a>
            </div>
            <div id="response"></div>
          </>
        ) : 
        (
          <button onClick={() => Auth.federatedSignIn()}>Federated Sign In</button>
        )
      }
    </div>
  );
}

export default App;