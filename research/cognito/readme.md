# Installing React

## Steps (Linux)
- ### Install npm
  - ### `sudo apt update`
  - ### `sudo apt install npm`
- ### Install npx
  - ### `sudo npm install -g npx`
- ### Install react sample app
  - ### `sudo npx create-react-app my-app`
  - If you get a **_node version failure_**, then update your node version by following these steps:
    - ### `sudo npm cache clean -f`
    - ### `sudo npm install -g n`
    - ### `sudo n 14.15.5`
      - As of today (Feb 2021) the latest stable version is **14.15.5**, update to the latest version accordingly
    - Close and reopen your terminal
    - Run the same npx command from above
- Finally refer to this [readme](my-app/README.md/README.md) file to run the application

# Setup AWS Amplify

Amplify helps us to build cloud-powered apps by providing a framework to access different AWS services.

## Steps (Linux)

- ### `sudo npm install -g @aws-amplify/cli`
- ### `amplify configure`
  - This step will log you into AWS
  - Make sure to create a user and grant admin access
    - Select programmatic access when creating the user
    - Copy the access key and id
  - Complete the configuration, more information can be found [here](https://blog.logrocket.com/authentication-react-apps-aws-amplify-cognito/)

## Create React Application

## Steps (Linux)

- ### `npx create-react-app react-amplify-identity --typescript && cd react-amplify-identity`
- ### `amplify init`
  - Follow the steps, and just press **enter** on each prompt
- ### `amplify add auth`
  - ### Select the authentication/authorization services that you want to use: `User Sign-Up & Sign-In only (Best used with a cloud API only)`
  - ### Please provide a friendly name for your resource that will be used to label this category in the project: `onestepreliefCognito`
  - ### Please provide a name for your user pool: `one_step_relief_test_user_pool`
  - **Warning: you will not be able to edit these selections.**
  - ### How do you want users to be able to sign in? `Email`
  - ### Do you want to add User Pool Groups? `No`
  - ### Do you want to add an admin queries API? `No`
  - ### Multifactor authentication (MFA) user login options: `OFF`
  - ### Email based user registration/forgot password: `Enabled (Requires per-user email entry at registration)`
  - ### Please specify an email verification subject: `Your verification code`
  - ### Please specify an email verification message: `Your verification code is {####}`
  - ### Do you want to override the default password policy for this User Pool? `No`
  - **Warning: you will not be able to edit these selections.**
  - ### What attributes are required for signing up? `Email`
  - ### Specify the app's refresh token expiration period (in days): `30`
  - ### Do you want to specify the user attributes this app can read and write? `No`
  - ### Do you want to enable any of the following capabilities?
    - ### Do you want to use an OAuth flow? `No`
    - ### Do you want to configure Lambda Triggers for Cognito? `No`
- ### `amplify push`
  - This command creates an AWS Cloudformation Stack to provision AWS Cognito
  - Wait for it to complete
- ### `sudo npm install aws-amplify react-router-dom styled-components antd password-validator jwt-decode`
  - This command installs the required modules
- ### `sudo npm install aws-amplify @aws-amplify/ui-react`

# Amplify reference doc

To get more information about amplify and authentication please follow [this](https://docs.amplify.aws/lib/auth/getting-started/q/platform/js) link.

For **social login** follow [this](https://docs.amplify.aws/lib/auth/social/q/platform/js#amazon-cognito-user-pool-setup) link.

# Known limitations

From amplify doc site: **_"When using the federated OAuth flow with Cognito User Pools, the device tracking and remembering features are currently not available within the library..."_**