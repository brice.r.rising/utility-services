# Running OneStep CLI

## Class diagram

![alt text](Class_Diagram.png "Class Diagram")

## Build release package (Windows commands)

* Linux
  * `dotnet publish OneStepCli\OneStepCli.csproj -r ubuntu.18.04-x64 /p:PublishSingleFile=true -o Your_Output_Folder`
* OSX
  * `dotnet publish OneStepCli\OneStepCli.csproj -r osx.10.11-x64 /p:PublishSingleFile=true -o Your_Output_Folder`
* Windows
  * `dotnet publish OneStepCli\OneStepCli.csproj -r win-x64 /p:PublishSingleFile=true -o Your_Output_Folder`

## Configuring commands

To configure commands there are two files that need to modify:

1. Manifest file - file that contains a list of modules and its respective commands, failing to list a module in the manifest results in commands not executed
2. Module file - this file is optional and contains a list of commands to run, a user can opt for running a single command via the cli

### Manifest file update

```json
{
  "modules": [
    {
      "name": "accreditation",
      "commands": [
        {
          "name": "create-application",
          "uri": "https://www.google.com",
          "httpMethod":  "POST",
          "requestType": "json"
        },
        {
          "name": "get-application",
          "uri": "https://www.google.com",
          "httpMethod": "GET",
          "requestType": "query"
        }
      ]
    }
  ]
}
```

Available `httpMethod` values are (case insensitive):
* GET
* PUT
* POST
* DELETE

Available `requestType` values are (case insensitive):
* json
* query

Note that when passing a module file, `name` property must match the command listed in the module file.

### Module file update (optional)

```json
{
  "name": "accreditation",
  "commands": [
    {
      "name": "get-application",
      "body": "?applicationId=1234"
    },
    {
      "name": "create-application",
      "body": {
          "foo": "bar"
      }
    }
  ]
}
```

In this example, notice that `name` property matches the command name from the manifest file, name property is case insensitive.

`Body` property can be a _query string_ or can be a _json_ body, the CLI knows how to parse both values.

**IMPORTANT:** 

`requestType` property is used to set the expected input body, if a command has a `requestType` equals to _json_ then the `body` **must be** a _json_ object. 

For now this check is not enforced, meaning, if your command expects a _json_ body and you pass a _query string_, the CLI will pass the _query string_ as part of the request body and could result in a failure.

## Executing commands

Prior executing the CLI there are important env variables to set:

* onestep_cli_username
* onestep_cli_password
* onestep_cli_aws_accesskeyid
* onestep_cli_aws_accesskeysecret
* onestep_cli_clientid
* onestep_cli_userpoolid

`onestep_cli_username` & `onestep_cli_password` are used to log a user into a Cognito User Pool, this is the regular email and password a user use to login in the Main UI.

`onestep_cli_aws_accesskeyid` & `onestep_cli_aws_accesskeysecret` are used to authenticate API access to AWS. These values are generated when you create a new user in AWS and grant programmatic access.

`onestep_cli_clientid` & `onestep_cli_userpoolid` are used to indicate the AWS Cognito User Pool to use.

Choose the appropriate release from _**Build release package (Windows commands)**_ step.

Now execute the command as follows:

* Linux
  * chmod 777 ./OneStepCli
    * chmod is important to run to grant the appropriate permission
  * ./OneStepCli /mnt/c/onestep.json --file /mnt/c/accreditation.json

* Windows
  * OneStepCli.exe c:\onestep.json --file c:\accreditation.json

In both examples, `onestep.json` is the manifest file and `accreditation.json` is the file with all commands to execute.

If instead the desire is to execute a single command, use the follow syntax:

```
 OneStepCli.exe c:\onestep.json --module "accreditation" --command "create-application" --request-type "json" --body "{ 'foo' : 'bar' }"
```


