﻿using CommandLine;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using OneStepCli.Service.Entities;
using OneStepCli.Service.Helper;
using OneStepCli.Service.Services;
using System;
using System.Threading.Tasks;

namespace OneStepCli
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddConsole();
            });
            ILogger logger = loggerFactory.CreateLogger<Program>();


            return await Parser.Default.ParseArguments<CommandLineOptions>(args)
            .MapResult(async (CommandLineOptions opts) =>
            {
                try
                {
                    // Initialize CliManager
                    await CliManager.InitializeAsync(manifestPath: opts.ManifestPath);

                    // Create new instance of decorator
                    var cli = new DecoratorCliManager(
                        cliManager: CliManager.Instance,
                        logger: logger);

                    if (!string.IsNullOrWhiteSpace(opts.CommandFile))
                    {
                        // if a command file is passed, it takes precedence

                        var parsedModule = await cli.ParseCommandFileAsync(opts.CommandFile);

                        foreach (var command in parsedModule.Commands)
                        {
                            await cli.ExecuteCommandAsync(parsedModule.Name, command);
                        }
                    }
                    else
                    {
                        GuardHelper.EnsureNotNullOrEmpty(opts.Module, "module");
                        GuardHelper.EnsureNotNullOrEmpty(opts.Command, "command");
                        GuardHelper.EnsureNotNullOrEmpty(opts.Body, "body");
                        GuardHelper.EnsureNotNullOrEmpty(opts.RequestType, "request-type");

                        var manifest = await cli.ReadManifestAsync(opts.ManifestPath);

                        GuardHelper.EnsureNotNull(manifest, nameof(manifest));

                        if (!manifest.TryGetValue(opts.Module, out var commands))
                        {
                            throw new ArgumentException($"Module: '{opts.Module}' is not registered in the manifest.");
                        }

                        if (!commands.TryGetValue(opts.Command, out var commandFromManifest))
                        {
                            throw new ArgumentException($"Command: '{opts.Command}' is not registered in the manifest for module '{opts.Module}'.");
                        }

                        var requestType = (RequestType)Enum.Parse(typeof(RequestType), opts.RequestType, ignoreCase: true);

                        var command = new Command(
                            name: commandFromManifest.Name,
                            uri: commandFromManifest.Uri,
                            httpMethod: commandFromManifest.HttpMethod,
                            body: ParseBody(opts.Body, requestType),
                            requestType: requestType);

                        await cli.ExecuteCommandAsync(opts.Module, command);
                    }

                    return 0;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Unhandled error!");
                    return -3; // Unhandled error
                }
            },
            errs => Task.FromResult(-1)); // Invalid arguments
        }

        static JToken ParseBody(string body, RequestType requestType)
        {
            if (requestType == RequestType.QUERY && body.StartsWith('?'))
            {
                // Query string
                return body;
            }

            return JToken.Parse(body);
        }
    }
}
