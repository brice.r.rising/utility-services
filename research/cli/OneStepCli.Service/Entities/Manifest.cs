﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Entities
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Manifest
    {
        /// <summary>
        /// The list of all available modules.
        /// </summary>
        [JsonProperty("modules")]
        public IList<Module> Modules { get; }

        public Manifest(IList<Module> modules)
        {
            this.Modules = modules;
        }
    }
}