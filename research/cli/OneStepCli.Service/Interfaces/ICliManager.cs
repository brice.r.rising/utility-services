﻿/*
 * OneStep Relief Platform
 */

namespace OneStepCli.Service.Interfaces
{
    using Newtonsoft.Json.Linq;
    using OneStepCli.Service.Entities;
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ICliManager
    {
        /// <summary>
        /// Method that reads a cli manager manifest. A manifest contains a list of all available modules and its actions.
        /// </summary>
        /// <param name="manifestFullPath">The manifest full path (JSON file).</param>
        Task<ImmutableDictionary<string, ImmutableDictionary<string, Command>>> ReadManifestAsync(string manifestFullPath);

        /// <summary>
        /// Method that executes a command.
        /// </summary>
        /// <param name="module">The module where the command exists.</param>
        /// <param name="command">The command to execute.</param>
        Task ExecuteCommandAsync(string module, Command command);

        /// <summary>
        /// Method that parses a command file.
        /// </summary>
        /// <param name="commandFileFullPath">The command file absolute path.</param>
        /// <returns></returns>
        Task<Module> ParseCommandFileAsync(string commandFileFullPath);

        /// <summary>
        /// Method that performs authentication, expects elevated rights to perform all actions.
        /// </summary>
        /// <param name="username">The admin username.</param>
        /// <param name="password">The admin password.</param>
        Task<string> AuthenticateAsync(string username, string password);
    }
}
