# blockchain-service
*addresses are case sensistive*


### Create Account
### WARNING: private key is passed from server, use with caution
GET https://test.onesteprelief.org/onestep/blockchain/account/createAccount \
*Attributes*: \
None
<br><br>
*Returns*: \
json with a new generated address and associated mnemoic, example:
```json
{ 
    "address": "CLTMYXARFCF4WSY6ZYHIV4WERGZUVE7POZFYEEJZOUVXKYXF3JXBKTBVVQ", 
    "mnemonic": "picture bean evidence edit together art level odor dentist crisp stadium flame broccoli traffic bottom cotton project critic drill broccoli slush proof total absent double" 
}
```
<br><br><br>
### Get Balance
GET https://test.onesteprelief.org/onestep/blockchain/account/getBalance/{address}  \
*Attributes*: \
address (string): a public algorand address

*Returns* \
json, example:
```json
{
  "algoBalance": 115142507,
  "assets": [
    {
      "amount": 1003,
      "creator": "UQPAFC6YH3WNJR4ALTKYLXW3CBVH4L56HO4MZ5YZADG6MU6Y3F3KYSDHAA",
      "assetId": 15104989,
      "name": "Step Token"
    },
    {
      "amount": 202999896,
      "creator": "VETIGP3I6RCUVLVYNDW5UA2OJMXB5WP6L6HJ3RWO2R37GP4AVETICXC55I",
      "assetId": 10458941,
      "name": "USDC"
    }
  ]
}
```
<br><br><br>
### Get USDC Balance
GET https://test.onesteprelief.org/onestep/blockchain/account/getBalance/usdc/{address}  \
*Attributes*: \
address (string): a public algorand address

*Returns* \
json, example:
```json
{
  "amount": 103999897,
  "assetId": 10458941,
  "name": "USDC"
}
```

<br><br><br>
### Account History
GET https://test.onesteprelief.org/onestep/blockchain/account/accountHistory/{address} \
*Attributes*: \
address (string): a public algorand address
<br><br>

*Returns*: \
json, example:
```json
{
  "transactions": [
    {
      "date": 1620502128,
      "sender": {
        "address": "A3RWWQ262VEPBAK52IAXBNADOTU26DOFOCTR54BX36V6IQPNAUTLTEYY5Q"
      },
      "receiver": {
        "address": "RUGF3YM47OLWWH74OTU3C2BEQXN3WBIRQ4BOGYPM2H7HIFVIESBCE7WC44"
      },
      "amount": 5000000,
      "note": "Donation to fund ID: 5e194f45-4be8-40f3-863f-c316f8d727a5",
      "type": "axfer",
      "assetID": 10458941,
      "name": "USDC"
    },
    {
      "date": 1620256877,
      "sender": {
        "address": "A3RWWQ262VEPBAK52IAXBNADOTU26DOFOCTR54BX36V6IQPNAUTLTEYY5Q"
      },
      "receiver": {
        "address": "6T3JGVUJD4QXDAQSHKETD7USUU27GO7ZXVNASRFBDYF7WZ4H3GYBQML5HU"
      },
      "amount": 1000000,
      "note": "Donation to fund ID: 8e4a6c48-8e65-419f-ae3a-919e748e737e",
      "type": "pay"
    }
  ]
}
```

<br><br><br>
### Account Algo Balance History
GET https://test.onesteprelief.org/onestep/blockchain/account/{address}/balanceHistory/{days} \
*Attributes*: \
address (string): a public algorand address
days (int): integer for number of days of history
<br><br>

*Returns*: \
json, example:
```json
{
  "balanceHistory": [
    {
      "balance": 27943908,
      "timestamp": 1618192884
    },
    {
      "balance": 27945908,
      "timestamp": 1618106484
    },
    {
      "balance": 27945908,
      "timestamp": 1618020084
    }
  ]
}
```

<br><br><br>

### Send Algo
PayId is required for sender, for receiver will accept either public address or PayId (if applicable) in "address" field
Amount is microAlgos,e.g. 1000000 = 1 Algo
Sender must approve transaction in Pago app

POST https://test.onesteprelief.org/onestep/blockchain/transaction/sendAlgo  \
*Attributes*: \
Json Object, example below:
```json
{
  "sender":{
"payId":"RobertTestAccount2$pagoservices.com"
},
"receiver":{
"address":"7JBKQM3PXALN372QKGINDPGTKPLIBGUSUAMUKSVL2UFGTE5TOKMTHCUIBE"
},
"amount": 5,
"note":"1kB maximum"
}
```

*Returns*: \
json with Pago transaction id, example:
```json
{
  "message": "Pago Request submitted. Pago txId: ",
  "data": "11096337-6b5f-4870-a74b-d86588fbfd00"
}
```


<br><br><br>

### Send USDC
PayId is required for sender, for receive will accept either public address or PayId (if applicable) in "address" field
Amount is microUSDC,e.g. 1000000 = 1 USDC
Sender must approve transaction in Pago app

POST https://test.onesteprelief.org/onestep/blockchain/transaction/sendUsdc \
\
*Attributes*: \
Json Object, example below:
```json
{
    "sender":{
"payId":"RobertTestAccount2$pagoservices.com"
},
   "receiver":{
      "address":"5CKIQSDWYSKRSBMIVISXH7KF3URPF4E6SGYQFUGDDICQAOGVFNRUOXLNSY"
   },
   "amount": 1000000,
   "note":"Pago Wallet Demonstration"
}

```

*Returns*: \
json with Pago transaction id, example:
```json
{
  "message": "Pago Request submitted. Pago txId: ",
  "data": "f0af7401-0421-42aa-a536-efc528a6687d"
}
```


<br><br><br>
### Transaction Status

Returns status of recent transactions. For historic transaction, use onestep/blockchain/transaction/transaction?txId={txId}
GET https://test.onesteprelief.org/onestep/blockchain/transaction/txnStatus/{txnId}  \
*Attributes*: \
txnId (string): \
transaction id


*Returns* \
json, example:
```json
{
  "applicationIndex": null,
  "confirmedRound": 12534440,
  "closingAmount": null,
  "closeRewards": null
}
```

<br><br><br>
### Historic Transaction Status

Returns status of historic transactions. use onestep/blockchain/transaction/transaction?txId={txId}
GET https://test.onesteprelief.org/onestep/blockchain/transaction/transaction?txId={txId}  \
*Attributes*: \
txId (string): \
transaction id


*Returns* \
json, example:
```json
{
  "date": 1620578099,
  "sender": {
    "address": "T3CNEH7KM7TZXWYCHENLG4AFJ5R6HAOEUACQADV36LPHIHA6YVLX4F4FDM"
  },
  "receiver": {
    "address": "64OFVHKKNTXKB24XIGQV2SIPXXBRBXHBTQB4RKF7MJ6RCGBEBO4ETW2ME4"
  },
  "amount": 1500,
  "type": "pay"
}
```

<br><br><br>
### Get Asset

#### Used to obtain asset information, namely the asset name
GET https://test.onesteprelief.org/onestep/blockchain/transaction/getAsset/{assetId}  \
*Attributes*: \
assetid (long): the ID of the asset to lookup. e.g., USDC on TestNet is 10458941

*Returns* \
json, example:
```json
{
  "amount": null,
  "creator": "VETIGP3I6RCUVLVYNDW5UA2OJMXB5WP6L6HJ3RWO2R37GP4AVETICXC55I",
  "assetId": 10458941,
  "name": "USDC"
}
```

<br><br><br>
### Create Asset

#### Used to create a new asset. Should be used by system accounts only since it requires private key.
POST https://test.onesteprelief.org/onestep/blockchain/transaction/createAsset  \
*Attributes*: \
Json Object, example below:
```json
{
	"creator_addr":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
	"creator_mnemonic":"wild sweet laptop oblige trust penalty viable want soup spatial raccoon penalty youth room soup breeze exile aunt close derive special boy mechanic absent message",
"total_amount":"4",
"default_frozen":"false",
"unit_name":"cents",
"asset_name":"Asset12",
"manager_addr":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
"reserve_addr":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
"freeze_addr":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
"clawback_addr":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
"url":"", 
"decimals":"0"
}

```

*Returns* \
json, example:
```json
{
  "message": "Created asset successfully",
  "data": {
    "assetId": 15142448,
    "txId": "IMSMP3K6VB6HTIGF23TQVPMURQSZBPL7KLUA4HRVZICLV5I5C63A"
  }
}
```

<br><br><br>
### Transfer Asset

#### Used to transfer an asset. Should be used by system accounts only since it requires private key.
POST https://test.onesteprelief.org/onestep/blockchain/transaction/transferAsset  \
*Attributes*: \
Json Object, example below:
```json
{
   "sender":{
      "address":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
		 "mnemonic":"wild sweet laptop oblige trust penalty viable want soup spatial raccoon penalty youth room soup breeze exile aunt close derive special boy mechanic absent message"
   },
   "receiver":{
      "address":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q"
   },
   "amount": 0,
   "note":"transfer asset troubleshoot",
	"assetId":"15104989"
}

```

*Returns* \
json, example:
```json
{
  "message": "Asset Transferred Successfully",
  "data": {
    "assetId": 15104989,
    "txId": "VAU3VEJCV6XS245XTFUK7EDWEHDJUDGNCXFKKWCDCQ5CAKAEJ55Q"
  }
}
```

<br><br><br>
### Sign an electronic document; i.e. store a hash value for future reference
Creates a zero amount transaction to self. Transaction fees apply (normally 1000 microAlgos)
'Note' fields can be adjusted as needed.
Sender must approve transaction in Pago app

POST https://test.onesteprelief.org/onestep/blockchain/transaction/signAuth  \
*Attributes*: \
Json Object, example below:
```json
{
  "sender": {
    "payId":"RobertTestAccount2$pagoservices.com"
  },
  "note": {
    "note": "Test sign document 6",
    "docId": "Document ID field",
    "hash": "7d659a2feaa0c55ad01A0f86d081884c"
  }
}

```

*Returns*: \
json with Pago transaction id, example:
```json
{
  "message": "Pago Request submitted. Pago txId: ",
  "data": "03b8a3dd-96dd-4c9b-bedb-edbcf4418fa5"
}
```
<br><br><br>

### Opt in Algorand Account to receive USDC
### WARNING: private key is passed to server, use with caution. Should only be used for internal calls.

This can only be done once per account
Account must have sufficient Algos to process, recommnend minimum 1 Algo balance

POST https://test.onesteprelief.org/onestep/blockchain/transaction/optInUsdc  \
*Attributes*:
Json Object with the account mnemoic, example below:

```json
{
  "mnemonic": "beach vivid manual tourist steak law armor oval rescue prison tumble amount good curve tortoise taxi toss street decide mind lake enroll express ability balance"
}
```

*Returns* \
json of transaction id

```json
{
  "txId": "IDVZIZ7B777PDKR2FSMEJWCPROWM753BQDNSQHDEX7MGZPPIS6LQ"
} 
```

<br><br><br>

### Opt in Algorand Account to receive specific asset
### WARNING: private key is passed to server, use with caution; should only be used for internal calls.

This can only be done once per account
Account must have sufficient Algos to process, recommnend minimum 1 Algo balance

POST https://test.onesteprelief.org/onestep/blockchain/transaction/optInUsdc  \
*Attributes*:
Json Object with the account mnemoic, example below:

```json
{
   "sender":{
      "address":"B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
		 "mnemonic":"wild sweet laptop oblige trust penalty viable want soup spatial raccoon penalty youth room soup breeze exile aunt close derive special boy mechanic absent message"
   },
	"assetId":"15104989"
}
```

*Returns* \
json of transaction id

```json
{
  "message": "Opt in Successful",
  "data": {
    "assetId": 15104989,
    "txId": "YMUVEZCORRFNZNNWRKVNKD2YA4XH4JXNLEMYG63WHYD3DHAMCABQ"
  }
}
```

<br><br><br>
### Provision new 'fund' account
Used to create and provision a 'fund' account. No pre-requisites, as code creates account from scratch.
Creates a new account generating key pairs, provision account with 1.5 Algos and opts-in to USDC. 
POST response returns link to check status of long running operation.

POST https://test.onesteprelief.org/onestep/blockchain/provision/  \
*Attributes*: \
None
<br><br>


*Returns*: \
202 response if successful. Link to check on status of operation in header.

### Provision new 'fund' account status.
Link is provided in the initial call above.
202 response returned if operation is still ongoing, otherwise 200 returned.

POST http://localhost:8080/onestep/blockchain/provision/{requestId}/status  \
*Attributes*:
requestId is obtained from the initial request.



*Returns* \
json with new account info, as well as confirmation of provisioning.
#### Address and mnemonic MUST be saved; it is impossible to retrieve a second time.

```json
{
  "address": "MNNOWJKRJWY4ODRTYNCBWOYOJ2MNBQQ7E6ZHGKINZRSPAOQY4HY5E7AGJA",
  "mnemonic": "pulp stem wave volcano rent hen wood kit wall soon skate butter issue miss mass yellow will salon observe lesson void cushion popular abandon mimic",
  "sendAlgoTxId": "GEVJZCU4EYD5XJVUER2JCXVMVQYAONI33BWHTWRUIODNBNPAE5QA",
  "optInTxId": "ZPGO3MSTLG22LDGT7GORJAPJN4AXLNR6DIVSSOIYOF2F6HFG47EA",
  "balance": {
    "algoBalance": 1500000,
    "assets": []
  }
}
```

### Provision a new user's account with minimal Algos, and opt it in to USDC and Step tokens
Convenience function so that user doesn't have to opt-in. User must have Pago ID.
User will need to approve opt-in transactions via Pago app.


POST https://test.onesteprelief.org/onestep/blockchain/provision/user  \
*Attributes*:
Json Object with the PagoId and callbackURL (not currently implemented) example below:

```json
{
  "sender":{
"payId":"RobertTestAccount2$pagoservices.com"
},
	"callbackURL": "MyURL.com"
}
```

*Returns* \
json of Pago transaction id

```json
{
  "blockchainAddress": "B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q",
  "assets": [
    {
      "assetId": "10458941",
      "txId": "9f02c480-0817-4cdd-bc7a-3b19cf16439d"
    },
    {
      "assetId": "15104989",
      "txId": "28292dca-e52f-4f0e-a13c-a9730c43c604"
    }
  ]
}
```



