package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class SendTransactionRequestTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    SendTransactionRequest request;

    @BeforeEach
    public void beforeEach() {

        request = new SendTransactionRequest(new AccountModel("ADDR6934"), new AccountModel("ADDR4397"),
                "A Note", 1000L, "Pay", 9876L, null);

    }
    @Test
    public void serializeSendMoneyRequest() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("sendMoney.json"), SendTransactionRequest.class));

        assertThat(mapper.writeValueAsString(request)).isEqualTo(response);
    }

    @Test
    public void deserializeSendMoneyRequest() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("sendMoney.json"), SendTransactionRequest.class))
                .isEqualTo(request);
    }
}
