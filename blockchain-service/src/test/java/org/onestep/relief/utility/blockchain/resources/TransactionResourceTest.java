package org.onestep.relief.utility.blockchain.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.checkerframework.checker.units.qual.A;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.blockchain.dto.GenericResponse;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.TransactionConfirmation;
import org.onestep.relief.utility.blockchain.service.impl.CommonService;
import org.onestep.relief.utility.blockchain.service.impl.PagoService;
import org.onestep.relief.utility.blockchain.service.impl.TransactionService;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class TransactionResourceTest {

    private static final TransactionFacade facade = mock(TransactionFacade.class);

    private static final ResourceExtension extension = ResourceExtension.builder()
            .addResource(new TransactionResource(facade))
            .build();

    @BeforeEach
    public void beforeEach() {
    }


    @Test
    public void sendAlgoTest() throws Exception {

        SendTransactionRequest request = new SendTransactionRequest(1000L);

        when(facade.sendAlgo(request)).thenReturn(new GenericResponse<>("txId", "TXN567483"));

        Entity<?> entity = Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("onestep/blockchain/transaction/sendAlgo")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(entity);

        assertThat(response.readEntity(GenericResponse.class).getData()).isEqualTo("TXN567483");
        verify(facade).sendAlgo(request);
    }

    @Test
    public void sendUsdcTest() throws Exception {

        SendTransactionRequest request = new SendTransactionRequest(1000L);

        when(facade.sendUsdc(request)).thenReturn(new GenericResponse<>("USDC Sent. TxId: ", "TXN567483"));

        Entity<?> entity = Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("onestep/blockchain/transaction/sendUsdc")
                .request()
                .post(entity);

        assertThat(response.readEntity(GenericResponse.class).getData()).isEqualTo("TXN567483");
        verify(facade).sendUsdc(request);
    }

    @Test
    public void getTransactionStatusTest() throws Exception {

        TransactionConfirmation confirmation = new TransactionConfirmation();
        confirmation.setApplicationIndex(1L);

        when(facade.getTransactionStatus("ZK6578")).thenReturn(confirmation);

        TransactionConfirmation response = extension
                .target("onestep/blockchain/transaction/txnStatus/ZK6578")
                .request()
                .get(TransactionConfirmation.class);

        assertThat(response.getApplicationIndex()).isEqualTo(1L);
        verify(facade).getTransactionStatus("ZK6578");
    }

    @Test
    public void sendUsdcPagoTest() throws Exception {

        final Client client = new JerseyClientBuilder().build();


        TransactionFacade transactionFacade = new TransactionFacade(new TransactionService(new CommonService(),
                new PagoService(client)), new CommonService(),new PagoService(client));

        SendTransactionRequest request = new SendTransactionRequest();

        request.setAmount(10L);
        request.setNote("regression Unit test");
        request.setSender(new AccountModel(null, null, "RobertTestAccount2$pagoservices.com"));
        request.setReceiver(new AccountModel("B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q"));

        transactionFacade.sendUsdc(request);

    }


//    @Test
//    public void getSignAuthenticateTest() throws  CustomException {
//
//        AuthDocumentTransactionRequest request = new AuthDocumentTransactionRequest();
//
//        when(facade.createAuthDoc(request)).thenReturn("TXN45678");
//
//        Entity<?> entity = Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);
//
//        Response response = extension
//                .target("onestep/blockchain/transaction/signAuth")
//                .request()
//                .post(entity);
//
//        assertThat(response.readEntity(String.class)).isEqualTo("TXN45678");
//        verify(facade).createAuthDoc(request);
//    }
}
