package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.algod.client.ApiException;
import com.algorand.algosdk.transaction.SignedTransaction;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.blockchain.dto.AuthDocumentTransactionRequest;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.NewTransaction;
import org.onestep.relief.utility.blockchain.service.ITransactionService;

import javax.ws.rs.client.Client;
import java.security.GeneralSecurityException;


@ExtendWith(DropwizardExtensionsSupport.class)
public class TransactionServiceTest {

    private ITransactionService service;
    private AccountModel sender = new AccountModel("7JBKQM3PXALN372QKGINDPGTKPLIBGUSUAMUKSVL2UFGTE5TOKMTHCUIBE",
            "toy option range tuition drastic rib equal must web ridge similar dolphin seek seminar fiscal student tell number road squirrel test base art ability tumble");
    private AccountModel receiver = new AccountModel("WKK27ALMTHXLZOUXIWW22AARDLZJF7TAUWKVF5PFKN6C2WDFIATNB7YTZA");

    private NewTransaction newTransaction;

    private final String goodAccount1 = "B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q";
    private final String badAccount1 = "AXKDQBHSBK6VW3SKWDYG4DPU3MHXFB5UOGGG4FWYUWPWWHQICIZJCXNUYI";
    private final String goodAccount2 = "RobertTestAccount$pagoservices.com";

    final Client client = new JerseyClientBuilder().build();


    @BeforeEach
    public void beforeEach() {

        service = new TransactionService(new CommonService(), new PagoService(client));

        newTransaction = new NewTransaction();
        newTransaction.setSender(sender);

    }


    /**
     * Tests that transactions created are built properly and have expected values.
     */
    @Test
    public void createSignedDocTransaction() {

        com.algorand.algosdk.transaction.Transaction transaction;

        AuthDocumentTransactionRequest request = new AuthDocumentTransactionRequest();
        AuthDocumentTransactionRequest.Note note = request.new Note("Note field", "docID field", "hashABDC#E");

        newTransaction.setReceiver(sender);
        newTransaction.setAmount(0L);
        newTransaction.setNote(note.toString());
        newTransaction.setTransactionType("pay");

        transaction = service.createTransaction(newTransaction);

        Assertions.assertTrue(transaction.sender.compareTo(sender.getAddress()));
        Assertions.assertTrue(transaction.receiver.compareTo(sender.getAddress()));
        Assertions.assertTrue(note.toString().equals(new String(transaction.note)));
        Assertions.assertEquals(0,transaction.amount.intValue());
        Assertions.assertEquals("Payment",transaction.type.toString());


    }

    @Test
    public void createAlgoTransaction() {

        com.algorand.algosdk.transaction.Transaction transaction;

        newTransaction.setReceiver(receiver);
        newTransaction.setAmount(10L);
        newTransaction.setNote("note for payment");
        newTransaction.setTransactionType("pay");
        transaction = service.createTransaction(newTransaction);


        Assertions.assertTrue(transaction.sender.compareTo(sender.getAddress()));
        Assertions.assertTrue(transaction.receiver.compareTo(receiver.getAddress()));
        Assertions.assertTrue("note for payment".equals(new String(transaction.note)));
        Assertions.assertEquals(10,transaction.amount.intValue());
        Assertions.assertEquals("Payment",transaction.type.toString());

    }

    /** Regresssion test for our signTransaction method
     * @throws GeneralSecurityException Thrown if algosdk account cannot be created
     * @throws ApiException Thrown if signTransaction has an exception
     */
    @Test
    public void signTransaction() throws GeneralSecurityException, ApiException {
        com.algorand.algosdk.transaction.Transaction transaction;

        newTransaction.setReceiver(receiver);
        newTransaction.setAmount(10L);
        newTransaction.setNote("note for payment");
        newTransaction.setTransactionType("pay");

        transaction = service.createTransaction(newTransaction);

        com.algorand.algosdk.account.Account account =
                new com.algorand.algosdk.account.Account("toy option range tuition drastic rib equal must web ridge similar dolphin seek seminar fiscal student tell number road squirrel test base art ability tumble");

        /** Our method that we're testing/protecting */
        SignedTransaction signedTxn = service.signTransaction(account,transaction);

        /** Direct call to the SDK to create a transaction. This is the 'expected' value */
        SignedTransaction sdkTxn = account.signTransaction(transaction);

        /**
         * First, test that we have created 2 different objects..
         */
        Assertions.assertNotSame(sdkTxn,signedTxn);

        /**
         * But check that they are equal
         */
        Assertions.assertTrue(sdkTxn.equals(signedTxn));
    }

@Disabled
public void createAssetTransaction() {

        com.algorand.algosdk.transaction.Transaction transaction;

        newTransaction.setReceiver(sender);
        newTransaction.setAmount(100L);
        newTransaction.setNote("");
        newTransaction.setTransactionType("acfg");
        transaction = service.createTransaction(newTransaction);

        Assertions.assertTrue(transaction.sender.compareTo(sender.getAddress()));
        Assertions.assertTrue(transaction.receiver.compareTo(sender.getAddress()));
        Assertions.assertTrue("".equals(new String(transaction.note)));
        Assertions.assertEquals(100,transaction.amount.intValue());
        Assertions.assertEquals("Payment",transaction.type.toString());

    }

    @Test
    public void confirmPagoAccountTest() throws Exception {

        Assertions.assertNotNull(service.confirmPagoAccount(goodAccount1));
        Assertions.assertNotNull(service.confirmPagoAccount(goodAccount2));

    }

    @Test
    public void confirmPagoAccountBadAccountTest()  {
        Assertions.assertThrows(Exception.class, () -> {service.confirmPagoAccount(badAccount1);});

    }

    @Test
    public void getAddressFromPagoGoodAddressTest() throws Exception {
        String id;

        id = service.confirmPagoAccount(goodAccount1);
        Assertions.assertNotNull(service.getAddressFromPago(id));
        id = service.confirmPagoAccount(goodAccount2);
        Assertions.assertNotNull(service.getAddressFromPago(id));

    }

    @Test
    public void getAddressFromPagoTest()  {
        Assertions.assertThrows(Exception.class, () -> {service.confirmPagoAccount(badAccount1);});
    }


}
