package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Ignore
public class AccountModelHistoryTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    AccountHistory history;

    @BeforeEach
    public void beforeEach() {

        history = new AccountHistory();

        AbstractTransactionHistory txn = new TransactionHistoryPayment();
        txn.setSender(new AccountModel("ADDR5467"));
        txn.setReceiver(new AccountModel("ADDR7865"));
        txn.setAmount(1000L);

        history.setTransactions(Arrays.asList(txn));
    }

    @Ignore
    public void serializeAccountHistory() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("accountHistory.json"), AccountHistory.class));

        assertThat(mapper.writeValueAsString(history)).isEqualTo(response);
    }

    @Ignore
    public void deserializeAccountHistory() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("accountHistory.json"), AccountHistory.class))
                .isEqualTo(history);
    }
}
