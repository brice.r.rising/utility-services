package org.onestep.relief.utility.blockchain.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.onestep.relief.utility.blockchain.util.AWSConstants.BLOCKCHAIN_SECRETSMANAGER_ID;
import static org.onestep.relief.utility.blockchain.util.AWSConstants.BLOCKCHAIN_SECRETSMANAGER_SECRET;
import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.PURESTAKE_API_KEY;

public class EnvironVarTest {

    private static final Logger logger = LoggerFactory.getLogger(EnvironVarTest.class);


    /**
     * Basic regression test to ensure that they are working correctly and constants are being set correctly
     */
    @Test
    public void getEnvironTest() {

        String result = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_ID");
        logger.info("Result " + result);
        Assertions.assertSame(BLOCKCHAIN_SECRETSMANAGER_ID,result);

        result = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_SECRET");
        Assertions.assertSame(BLOCKCHAIN_SECRETSMANAGER_SECRET,result);

        result = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_SECRET");
        Assertions.assertSame(BLOCKCHAIN_SECRETSMANAGER_SECRET,result);

        result = Config.getEnv("PURESTAKE_API_KEY");
        Assertions.assertSame(PURESTAKE_API_KEY,result);


    }
}
