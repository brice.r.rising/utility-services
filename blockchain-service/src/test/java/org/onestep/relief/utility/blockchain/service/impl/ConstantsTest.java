package org.onestep.relief.utility.blockchain.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;


/**
 * Ensures that constants aren't changed without notification, i.e. failing test
 */
class ConstantsTest {

    @Test
    public void testConstantsValue() {

        Assertions.assertEquals(ALGOD_API_ADDRESS, "https://testnet-algorand.api.purestake.io/ps2");
        Assertions.assertEquals(INDEXER_API_ADDR, "https://testnet-algorand.api.purestake.io/idx2");
        Assertions.assertEquals(ALGOD_PORT,443);
        Assertions.assertEquals(INDEXER_API_PORT,443);
        Assertions.assertEquals(ASSETID_USDC,Long.parseLong("10458941"));
        Assertions.assertEquals(API_KEY_HEADER, "X-API-Key");
        Assertions.assertEquals(ALGOD_API_TOKEN, "");
        Assertions.assertEquals(CONTENT_TYPE, "Content-Type");
        Assertions.assertEquals(X_BINARY_CONTENT_TYPE, "application/x-binary");
        Assertions.assertEquals(TRANSACTION_PENDING, "Transaction Status is pending");
        Assertions.assertEquals(TRANSACTION_TYPE_PAY, "pay");
        Assertions.assertEquals(TRANSACTION_TYPE_AXFER, "axfer");
        Assertions.assertEquals(TRANSACTION_TYPE_ACFG, "acfg");

    }
}