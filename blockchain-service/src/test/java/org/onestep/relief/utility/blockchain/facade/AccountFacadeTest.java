package org.onestep.relief.utility.blockchain.facade;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountHistory;
import org.onestep.relief.utility.blockchain.service.IAccountService;
import org.onestep.relief.utility.blockchain.service.impl.AccountService;
import org.onestep.relief.utility.blockchain.service.impl.CommonService;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.security.GeneralSecurityException;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.mockito.Mockito.when;

public class AccountFacadeTest {

    final Client client = new JerseyClientBuilder().build();


    private static final ObjectMapper mapper = new ObjectMapper();
    private final String MNEMONIC = "toy option range tuition drastic rib equal must web ridge similar dolphin seek seminar fiscal student tell number road squirrel test base art ability tumble";
    private final String ADDRESS = "7JBKQM3PXALN372QKGINDPGTKPLIBGUSUAMUKSVL2UFGTE5TOKMTHCUIBE";
    @Mock
    private IAccountService accountServiceMock;

    @InjectMocks
    private AccountFacade facade;

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void createAccountTest() throws GeneralSecurityException {

        com.algorand.algosdk.account.Account account = new com.algorand.algosdk.account.Account(MNEMONIC);

        when(accountServiceMock.createAccount()).thenReturn(account);

        Assertions.assertEquals(ADDRESS, facade.createAccount().getAddress());
        Assertions.assertEquals(MNEMONIC, facade.createAccount().getMnemonic());

    }


    @Test
    public void getAccountBalanceTest() throws Exception {

        AccountBalance balance = mapper.readValue(fixture("accountBalance.json"), AccountBalance.class);

        when(accountServiceMock.getAccountBalance(ADDRESS)).thenReturn(balance);

        Assertions.assertEquals(facade.getAccountBalance(ADDRESS), balance);
    }


    @Test
    public void getAccountBalanceUSDCTest() throws Exception {

        AccountBalance balance = mapper.readValue(fixture("accountBalanceUSDC.json"), AccountBalance.class);

        when(accountServiceMock.getAccountBalance(ADDRESS)).thenReturn(balance);

        Assertions.assertEquals((long) facade.getAccountBalanceUSDC(ADDRESS).getAmount(), 999983L);
    }

    /**
     * Basic test to ensure that this isn't broken
     * @throws IOException if unable to retrieve
     */
    @Test
    public void getAccountHistoryTest() throws IOException {
        AccountFacade newFacade = new AccountFacade(new AccountService(new CommonService()));
        AccountHistory response =
                newFacade.getAccountHistory("B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q");

        Assertions.assertNotNull(response);
    }

}
