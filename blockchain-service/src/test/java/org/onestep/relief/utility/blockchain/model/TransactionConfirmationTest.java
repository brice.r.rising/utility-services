package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TransactionConfirmationTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    TransactionConfirmation confirmation;

    @BeforeEach
    public void beforeEach() {

        confirmation = new TransactionConfirmation(5734838L,
                27092027L, 76378838L, 637378387L);

    }
    @Test
    public void serializeTransactionConfirmation() throws JsonProcessingException {

        String response = mapper.writeValueAsString(mapper
                .readValue(fixture("transactionConfirmation.json"),
                        TransactionConfirmation.class));

        assertThat(mapper.writeValueAsString(confirmation)).isEqualTo(response);
    }

    @Test
    public void deserializeTransactionConfirmation() throws JsonProcessingException {

        assertThat(mapper.readValue(fixture("transactionConfirmation.json"),
                TransactionConfirmation.class))
                .isEqualTo(confirmation);
    }
}
