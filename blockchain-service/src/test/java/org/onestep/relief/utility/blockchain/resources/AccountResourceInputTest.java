package org.onestep.relief.utility.blockchain.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Asserts that the input we think we are calling the methods with is what we expect.
 * Ensures that if we change the model or input type it is caught in this regression test.
 */
@ExtendWith(MockitoExtension.class)
//@RunWith(JUnitPlatform.class)
public class AccountResourceInputTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    final String ADDRESS = "TFGLWZ7S7GTNF4KIBPHPJDBXBQL4DWHSQ2ZOLH43QBYRNQ26F65ZYN3GFI";
    @InjectMocks
    private AccountFacade facadeInject;
    @Mock
    private AccountResource resourceMock;

    @BeforeEach
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
    }


    // public Response createAccount() - No test required as there are no input arguments to test


    @org.junit.jupiter.api.Test
    public void getAccountBalanceParametersTest() {

        resourceMock.getAccountBalance(ADDRESS);

        ArgumentCaptor<String> captur = ArgumentCaptor.forClass(String.class);
        Mockito.verify(resourceMock).getAccountBalance(captur.capture());
        assertThat(captur.getValue()).isEqualTo(ADDRESS);
    }

    @org.junit.jupiter.api.Test
    public void getAccountHistoryParametersTest() {

        resourceMock.getAccountHistory(ADDRESS);

        ArgumentCaptor<String> captur = ArgumentCaptor.forClass(String.class);
        Mockito.verify(resourceMock).getAccountHistory(captur.capture());
        assertThat(captur.getValue()).isEqualTo(ADDRESS);
    }

}
