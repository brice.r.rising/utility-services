package org.onestep.relief.utility.blockchain.facade;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.*;
import org.onestep.relief.utility.blockchain.dto.*;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.NewTransaction;
import org.onestep.relief.utility.blockchain.service.IPagoService;
import org.onestep.relief.utility.blockchain.service.ITransactionService;
import org.onestep.relief.utility.blockchain.service.impl.CommonService;
import org.onestep.relief.utility.blockchain.service.impl.PagoService;
import org.onestep.relief.utility.blockchain.service.impl.TransactionService;

import javax.ws.rs.client.Client;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.ASSETID_USDC;

/**
 * Asserts that the dependent methods we are calling from with class methods are using correct input.
 * Ensures that if we change the model, facade code, or input type it is caught in this regression test.
 */

public class TransactionFacadeCalledMethodsTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    DozerBeanMapper dozerMapper;

    @Mock
    private ITransactionService transactionServiceMock;

    @Mock
    CommonService commonServiceMock;

    @Mock
    IPagoService pagoServiceMock;

    @InjectMocks
    private TransactionFacade underTest;


    @BeforeEach
    public void setUp()  {

        MockitoAnnotations.initMocks(this);
        dozerMapper = new DozerBeanMapper();
    }

    /**
     * Tests that the parameters passed to service.createAsset are correct
     * @throws JsonProcessingException Unable to read input file or map it.
     * @throws Exception createAsset throws variety of exceptions which must
     *          be caught by caller.
     */
    @org.junit.jupiter.api.Test
    public void createAssetParametersTest() throws Exception {

        CreateAssetRequest request = mapper.readValue(fixture("createAssetTransaction.json"), CreateAssetRequest.class);

        // Not going to verify ALL of the address in the create asset transaction
        String expectedSender = request.getCreator_addr();
        String expectedManager = request.getManager_addr();
        Integer expectedTotalAmount = request.getTotal_amount();
        String expectedType = "acfg";

        // Call our underTest method
        underTest.createAsset(request);

        ArgumentCaptor<NewTransaction> passedArgs = ArgumentCaptor.forClass(NewTransaction.class);

        // get the arguments that were passed to the mocked method
        Mockito.verify(transactionServiceMock).createTransaction(passedArgs.capture());

        // arguments we expect are verified
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(passedArgs.getValue().getManager_addr()).isEqualTo(expectedManager);
        assertThat(passedArgs.getValue().getTotal()).isEqualTo(expectedTotalAmount);
        assertThat(passedArgs.getValue().getTransactionType()).isEqualTo(expectedType);
    }

    /**
     * Tests that the parameters passed to service.createAsset are correct
     * @throws JsonProcessingException Unable to read input file or map it.
     */
    @org.junit.jupiter.api.Test
    public void xferAssetParametersTest() throws Exception {

        SendTransactionRequest request = mapper.readValue(fixture("xferAssetTransaction.json"), SendTransactionRequest.class);

        // Not going to verify ALL of the address in the create asset transaction
        String expectedSender = request.getSender().getAddress();
        String expectedReceiver = request.getReceiver().getAddress();
        Long expectedAmount = request.getAmount();
        Long expectedAssetId = request.getAssetId();
        String expectedType = "axfer";

        // Call our underTest method
        underTest.sendXferAsset(request);

        ArgumentCaptor<NewTransaction> passedArgs = ArgumentCaptor.forClass(NewTransaction.class);

        // get the arguments that were passed to the mocked method
        Mockito.verify(transactionServiceMock).createTransaction(passedArgs.capture());

        // arguments we expect are verified
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(passedArgs.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(passedArgs.getValue().getAmount()).isEqualTo(expectedAmount);
        assertThat(passedArgs.getValue().getAssetId()).isEqualTo(expectedAssetId);
        assertThat(passedArgs.getValue().getTransactionType()).isEqualTo(expectedType);

    }

    /**
     * Tests that the parameters passed to service.createTransaction are correct
     * @throws JsonProcessingException Unable to read input file or map it
     */
    @org.junit.jupiter.api.Test
    public void sendAlgoParametersTest() throws Exception {

        SendTransactionRequest request = mapper.readValue(fixture("sendAlgoTransaction.json"), SendTransactionRequest.class);

        String expectedSender = request.getSender().getAddress();
        String expectedReceiver = request.getReceiver().getAddress();
        Long expectedAmount = request.getAmount();
        String expectedType = "pay";


        // Call our underTest method
        underTest.sendAlgo(request);

        ArgumentCaptor<NewTransaction> passedArgs = ArgumentCaptor.forClass(NewTransaction.class);

        // get the arguments that were passed to the mocked method
        Mockito.verify(transactionServiceMock).createTransaction(passedArgs.capture());

        // arguments we expect are verified
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(passedArgs.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(passedArgs.getValue().getAmount()).isEqualTo(expectedAmount);
        assertThat(passedArgs.getValue().getTransactionType()).isEqualTo(expectedType);
    }


    /**
     * Tests that the parameters passed to service.createTransaction are correct
     * @throws JsonProcessingException Unable to read input file or map it.
     */
    @org.junit.jupiter.api.Test
    public void authDocumentParametersTest() throws Exception {

        AuthDocumentTransactionRequest request = mapper.readValue(fixture("signDoc.json"), AuthDocumentTransactionRequest.class);

        String expectedSender = "CEIZ6LCO4JDQ57I4M7KYHZCECCDHVP4LJHSWCFFWACBCXN5TXZYZWN6YZI";
        String expectedReceiver = expectedSender;
        Long expectedAmount = 0L;
        String expectedType = "pay";


        // Call our underTest method
        underTest.createAuthDoc(request);

        ArgumentCaptor<NewTransaction> passedArgs = ArgumentCaptor.forClass(NewTransaction.class);

        // get the arguments that were passed to the mocked method
        Mockito.verify(transactionServiceMock).createTransaction(passedArgs.capture());

        // arguments we expect are verified
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(passedArgs.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(passedArgs.getValue().getReceiver().getAddress());
        assertThat(passedArgs.getValue().getAmount()).isEqualTo(expectedAmount);
        assertThat(passedArgs.getValue().getTransactionType()).isEqualTo(expectedType);
        assertThat(passedArgs.getValue().getAssetId()).isNull();
    }

    /**
     * Tests that the parameters passed to service.createTransaction are correct
     * @throws JsonProcessingException Unable to read or map input file
     */
    @org.junit.jupiter.api.Test
    public void sendUSDCParametersTest() throws Exception {

        SendTransactionRequest request = mapper.readValue(fixture("sendAlgoUsdc.json"), SendTransactionRequest.class);

        // Not going to verify ALL of the address in the create asset transaction
        String expectedSender = request.getSender().getAddress();
        String expectedReceiver = request.getReceiver().getAddress();
        Long expectedAmount = request.getAmount();
        Long expectedAssetId = ASSETID_USDC;
        String expectedType = "axfer";

        // Call our underTest method
        underTest.sendUsdc(request);

        ArgumentCaptor<NewTransaction> passedArgs = ArgumentCaptor.forClass(NewTransaction.class);

        // get the arguments that were passed to the mocked method
        Mockito.verify(transactionServiceMock).createTransaction(passedArgs.capture());

        // arguments we expect are verified
        assertThat(passedArgs.getValue().getSender().getAddress()).isEqualTo(expectedSender);
        assertThat(passedArgs.getValue().getReceiver().getAddress()).isEqualTo(expectedReceiver);
        assertThat(passedArgs.getValue().getAmount()).isEqualTo(expectedAmount);
        assertThat(passedArgs.getValue().getAssetId()).isEqualTo(expectedAssetId);
        assertThat(passedArgs.getValue().getTransactionType()).isEqualTo(expectedType);

    }


    @org.junit.jupiter.api.Test
    @Disabled
    // Need to look over sender/receiver. Because the 'sender', which is actually the receiver, has a PagoId
    // this is getting routed to PAGO when it shouldn't
    public void provisionUserTest() throws Exception {

        final Client client = new JerseyClientBuilder().build();
        TransactionFacade facade = new TransactionFacade(new TransactionService(new CommonService(),
                new PagoService(client)), new CommonService(), new PagoService(client));

        AccountModel receiver = new AccountModel();
        AccountModel sender = new AccountModel();
        receiver.setAddress("B2GCA7S5A5QFUFXD3BJCFPSSQNWTTSUOA57PC42PSD2ZOMH7UCUEOQLY3Q");
        sender.setPayId("RobertTestAccount2$pagoservices.com");
        SendTransactionRequest transactionRequest = new SendTransactionRequest();
        //Even though it's the receiving account we're using this as the setSender for backwards compatibility
        transactionRequest.setSender(sender);

        ProvisionPayResponse response = facade.provisionUserAccount(transactionRequest);
        Assertions.assertNotNull(response);
    }


    @org.junit.jupiter.api.Test
    @Disabled
    public void sendUSDCInsufficientFundsTest() throws Exception {

        SendTransactionRequest request = mapper.readValue(fixture("sendAlgoUsdc.json"), SendTransactionRequest.class);

        AccountModel senderAccount = new AccountModel("Q3JBCDN5MOTLDL6QW75CR4ITENMSC76R766V3U3AA3TEZA7G5PIY4VKE4I",
                "kitchen ozone evoke entire dynamic pitch peace item setup bleak december surge baby guide link clump thumb raw penalty vanish prefer egg book about grocery");
        request.setSender(senderAccount);
        request.setAmount(2000L);

        // Call our underTest method
        TransactionFacade facade = new TransactionFacade(new TransactionService(new CommonService(),
                new PagoService(new JerseyClientBuilder().build())),new CommonService(),
                new PagoService(new JerseyClientBuilder().build()));
        Assertions.assertThrows(Exception.class, ()->facade.sendUsdc(request));




    }
}
