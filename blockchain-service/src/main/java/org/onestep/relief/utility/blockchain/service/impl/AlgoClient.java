package org.onestep.relief.utility.blockchain.service.impl;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;
import com.algorand.algosdk.v2.client.common.AlgodClient;

/**
 * Implements a singleton for the AlgodClient instance. Less expensive than creating a new client
 * each time we need it. Just need to update parameters when used.
 */
public class AlgoClient extends AlgodClient {

    // Singleton object to return. Only 1 client allowed
    private static AlgodClient instance;

    private AlgoClient(String host, int port, String token) {
        super(host, port, token);
    }


    /**
     * Added for unit testing to ensure clients are setup for correct API
     */
    public String getHost() { return super.getHost(); }

    public int getPort() { return super.getPort(); }

    /**
     * Public method used to create singleton
     * @return single instance of AlgodClient
     */
    //Method to create Singleton
    public static AlgodClient getInstance() {
        if (instance == null) {
            // if instance is null, initialize
            instance =  new AlgoClient(ALGOD_API_ADDRESS, ALGOD_PORT, ALGOD_API_TOKEN);
        }
        return instance;
    }
}
