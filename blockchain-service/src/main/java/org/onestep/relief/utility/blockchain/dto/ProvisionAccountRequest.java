package org.onestep.relief.utility.blockchain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountHistory;
import org.onestep.relief.utility.blockchain.model.Entity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProvisionAccountRequest implements Entity {


    private String address;
    private String mnemonic;

    private String sendAlgoTxId;
    private String optInTxId;

    private AccountBalance balance;


}
