package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TransactionHistoryAsset extends AbstractTransactionHistory{

    @JsonProperty
    private Long assetID;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    public TransactionHistoryAsset(Long date, AccountModel sender,
                                   AccountModel receiver, Long amount,
                                   String note, String type, Long assetID, String name) {

        super(date,sender,receiver,amount,note,type);
        this.assetID = assetID;
        this.name = name;
    }
}
