package org.onestep.relief.utility.blockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.onestep.relief.utility.blockchain.util.PagoEnvelopeConstants.*;


@Data
@AllArgsConstructor
public class TransactionEnvelopeRequest {

    public TransactionEnvelopeRequest() {
        this.name = NAME;
        this.description = DESCRIPTION;
        this.applicationId = APPLICATION_ID;
        this.state = STATE;

    }

    private String name;

    private String sender;

    private String applicationId;

    private String description;


    private String state;

    private TransactionPagoRequest transactionInterfaceRequest;

    @Override
    public String toString() {
        return "{"
                + ",\"name\":\"" + name + "\""
                + ",\"sender\":\"" + sender + "\""
                + ",\"applicationId\":\"" + applicationId + "\""
                + ",\"description\":\"" + description + "\""
                + ",\"state\":\"" + state + "\""
                + ",\"transactionInterfaceRequest\":" + transactionInterfaceRequest
                + "}";
    }
}
