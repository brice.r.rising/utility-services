package org.onestep.relief.utility.blockchain.service.impl;

import org.apache.http.HttpStatus;
import org.dozer.DozerBeanMapper;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.dto.TransactionEnvelopeRequest;
import org.onestep.relief.utility.blockchain.dto.TransactionPagoRequest;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.service.IPagoService;
import org.onestep.relief.utility.blockchain.util.HTTPRequests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;
import static org.onestep.relief.utility.blockchain.util.PagoEnvelopeConstants.*;

public class PagoService implements IPagoService {

    private final DozerBeanMapper dozerMapper;
    private final Logger logger = LoggerFactory.getLogger(TransactionFacade.class);
    Client client;

    public PagoService(Client client) {
        this.client = client;
        this.dozerMapper = new DozerBeanMapper();
    }


    /** Wraps a transaction with a Pago Envelope
     * @param request TransactionPagoRequest. A modified
     * @return
     */
    public TransactionEnvelopeRequest createEnvelope(TransactionPagoRequest request) {

        logger.info("Create envelope called");
        TransactionEnvelopeRequest envelopeRequest = new TransactionEnvelopeRequest();
        // Add the transaction that we are 'wrapping'
        envelopeRequest.setTransactionInterfaceRequest(request);
        envelopeRequest.setSender(request.getSender());

        return envelopeRequest;

    }


    /**
     * Maps an SendMoneyTransactionRequest DTO  to a DTO object to be used for the Pago API
     *
     * @param request SendMoneyTransactionRequest DTO
     * @return DTO Object that we can wrap and submit
     */
    @Override
    public TransactionPagoRequest mapTransaction(SendTransactionRequest request, String blockchainAddress) {

        TransactionPagoRequest pagoTransaction = dozerMapper.map(request, TransactionPagoRequest.class);

        //TODO eventually create custom converter for Account to String but we'll use
        // getter/setter for now

        pagoTransaction.setLease(null);
        pagoTransaction.setType(TRANSFER_REQUEST);

        if (request.getAssetId() != null) {
            pagoTransaction.setAssetId(Math.toIntExact(request.getAssetId()));
        }


        pagoTransaction.setFee(Math.toIntExact(TRANSACTION_FEE));
        pagoTransaction.setSender(request.getSender().getPayId());
        pagoTransaction.setReceiver(request.getReceiver().getAddress());
        pagoTransaction.setId(ENVELOPE_ID);
        pagoTransaction.setAgreementRequest(
                new TransactionPagoRequest.AgreementRequest("text",
                        AGREEMENT_DESCRIPTION,
                        request.getNote()));

        return pagoTransaction;
    }

    /** Used to call GET endpoints from the Pago service.
     * @param route Route to call from the Pago API
     * @return Response object
     */
    @Override
    public Response pagoGetRequest(String route) {

        return HTTPRequests.GetRequest(PAGO_API_ADDR + route);

    }

    @Override
    public String submitTransactionEnvelope(TransactionEnvelopeRequest transactionEnvelopeRequest) throws Exception{

        Response response = HTTPRequests.PostRequest(PAGO_API_ADDR, PAGO_ENVELOPE_ROUTE,
                transactionEnvelopeRequest);

        Optional<Response> optionalResponse = Optional.ofNullable(response);


        if (optionalResponse.isPresent() && optionalResponse.get().getStatus() == HttpStatus.SC_OK) {
            logger.info("submit Transaction Envelope Request was successful");

            Map<String, Object> pagoResponse;
            pagoResponse = optionalResponse.get().readEntity(HashMap.class);

            return pagoResponse.get("id").toString();
        } else {

            logger.error("Error submitting transaction");
            throw new Exception("Error submitting Pago Transaction ");
        }

    }


}
