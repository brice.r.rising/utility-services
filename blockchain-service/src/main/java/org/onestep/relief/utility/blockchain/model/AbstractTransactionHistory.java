package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractTransactionHistory implements Entity{

    @JsonProperty
    private Long date;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AccountModel sender;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AccountModel receiver;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long amount;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String note;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;
}
