package org.onestep.relief.utility.blockchain.service;

import org.onestep.relief.utility.blockchain.dto.BalancesResponse;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountHistory;

import javax.ws.rs.core.Response;
import java.io.IOException;

public interface IAccountService {

    com.algorand.algosdk.account.Account createAccount();

    AccountHistory getAccountHistory(String address) throws IOException;

    AccountBalance getAccountBalance(String address) throws Exception;

    BalancesResponse getBalanceHistory(String accountAddress, int days) throws Exception;

}
