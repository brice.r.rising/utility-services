package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountHistory implements Entity {

    @JsonProperty
    private List<AbstractTransactionHistory> transactions = new ArrayList<>();
}
