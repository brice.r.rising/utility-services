package org.onestep.relief.utility.blockchain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionPagoRequest {


    @JsonProperty
    String sender;

    @JsonProperty
    String receiver;

    @JsonProperty
    Integer fee;

    @JsonProperty
    Integer amount;

    @JsonProperty
    String lease;

    @JsonProperty
    String type;

    @JsonProperty
    String id;

    @JsonProperty
    int assetId;

    @JsonProperty
    AgreementRequest agreementRequest;

    @Override
    public String toString() {
        return "{"
                + "\"agreementRequest\":" + agreementRequest
                + ",\"sender\":\"" + sender + "\""
                + ",\"receiver\":\"" + receiver + "\""
                + ",\"fee\":\"" + fee + "\""
                + ",\"amount\":\"" + amount + "\""
                + ",\"lease\":\"" + lease + "\""
                + ",\"type\":\"" + type + "\""
                + ",\"id\":\"" + id + "\""
                + ",\"assetId\":\"" + assetId + "\""
                + "}";
    }

    @Data
    @AllArgsConstructor
    // TODO this is a temp hack
    public static class AgreementRequest {

        @JsonProperty
        String mediaType;

        @JsonProperty
        String description;

        @JsonProperty
        String body;

        @Override
        public String toString() {
            return "{"
                    + "\"mediaType\":\"" + mediaType + "\""
                    + ",\"description\":\"" + description + "\""
                    + ",\"body\":\"" + body + "\""
                    + "}";
        }
    }




}
