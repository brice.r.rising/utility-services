package org.onestep.relief.utility.blockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.Entity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthDocumentTransactionRequest{

    private AccountModel sender;
    private Note note;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data // Do not remove pls
    public class Note {

        private String note;
        private String docId;
        private String hash;

        @Override
        public String toString() {
            return "{"
                    + "\"note\": \"" + note + "\""
                    + ", \"docId\": \"" + docId + "\""
                    + ",\"hash\": \"" + hash + "\""
                    + "}";
        }
    }




}
