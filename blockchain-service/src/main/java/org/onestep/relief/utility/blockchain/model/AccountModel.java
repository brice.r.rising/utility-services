package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountModel implements Entity {

    public AccountModel(String address) {
        this.address = address;
    }

    public AccountModel(String address, String mnemonic) {
        this(address);
        this.mnemonic = mnemonic;
    }

    private String address;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mnemonic;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String payId;

//    private Long amount; // I don't think this is being used for anything -RT
}
