package org.onestep.relief.utility.blockchain.facade;

import org.dozer.DozerBeanMapper;
import org.onestep.relief.utility.blockchain.dto.BalanceUSDCResponse;
import org.onestep.relief.utility.blockchain.dto.BalancesResponse;
import org.onestep.relief.utility.blockchain.model.*;
import org.onestep.relief.utility.blockchain.service.IAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.ASSETID_USDC;

/**
 * Layer of abstraction to handle exception with logging and propagate it
 */
public class AccountFacade {

    private Logger logger = LoggerFactory.getLogger(AccountFacade.class);

    IAccountService service;

    public AccountFacade(IAccountService service) {
        this.service = service;
    }

    /**
     * Generates a key pair for new account
     * @return Account
     */
    public AccountModel createAccount() {

        com.algorand.algosdk.account.Account account = this.service.createAccount();

        return new AccountModel(account.getAddress().toString(), account.toMnemonic());
    }

    /**
     * Checks balance of existing account
     * @param address
     * @return AccountBalance
     * @throws Exception
     */
    public AccountBalance getAccountBalance(String address) throws Exception {

        try {
            return service.getAccountBalance(address);
        } catch (Exception e) {
            logger.error("Unable to get account balance:", e.getMessage());
            throw e;
        }
    }

    /**
     * Checks USDC balance of existing account
     * @param address public Algorand address
     * @return
     * @throws Exception
     */
    public BalanceUSDCResponse getAccountBalanceUSDC(String address) throws Exception {
        AccountBalance balance = null;
        BalanceUSDCResponse response = null;
        DozerBeanMapper dozerMapper = new DozerBeanMapper();

        try {
            balance = service.getAccountBalance(address);
            Asset usdc = balance.getAssets().stream()
                    .filter(asset -> ASSETID_USDC.equals(asset.getAssetId()))
                    .findFirst()
                    .orElse(new Asset("USDC", ASSETID_USDC, 0));

            response = dozerMapper.map(usdc, BalanceUSDCResponse.class);

        } catch (Exception e) {
            logger.error("Unable to get account balance", e.getMessage());
            throw e;
        }
        return response;
    }

    /**
     * Returns transaction history of existing accounts
     * @param address
     * @return AccountHistory
     * @throws IOException
     */
    public AccountHistory getAccountHistory(String address) throws IOException {

        return service.getAccountHistory(address);
    }

    public BalancesResponse getBalanceHistory(String address, int days) throws Exception {

       return service.getBalanceHistory(address,days);
    }
}
