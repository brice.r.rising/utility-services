package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Asset implements Entity {

    public Asset (long amount, String creator, long assetId) {
        this.amount = amount;
        this.creator = creator;
        this.assetId = assetId;
    }

    public Asset (String name, long assetId, long amount) {
        this.name = name;
        this.assetId = assetId;
        this.amount = amount;
    }

    @JsonProperty
    private Long amount;

    @JsonProperty
    private String creator;

    @JsonProperty
    private long assetId;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

}
