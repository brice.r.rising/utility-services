package org.onestep.relief.utility.blockchain.util;

public class BlockchainConstants {

    public static final String ALGOD_API_ADDRESS = Config.getProperty("ALGOD_API_ADDRESS");

    public static final String INDEXER_API_ADDR = Config.getProperty("INDEXER_API_ADDR");

    public static final String ALGO_EXPLORER_API = Config.getProperty("ALGO_EXPLORER_API");

    public static final int ALGOD_PORT = Integer.parseInt(Config.getProperty("ALGOD_PORT"));

    public static final int INDEXER_API_PORT = Integer.parseInt(Config.getProperty("INDEXER_API_PORT"));

    public static final String API_KEY_HEADER = Config.getProperty("API_KEY_HEADER");

    public static final String PURESTAKE_API_KEY = Config.getEnv("PURESTAKE_API_KEY");

    public static final String ALGOD_API_TOKEN = Config.getProperty("ALGOD_API_TOKEN");

    public static final String CONTENT_TYPE = Config.getProperty("CONTENT_TYPE");

    public static final String X_BINARY_CONTENT_TYPE = Config.getProperty("X_BINARY_CONTENT_TYPE");

    public static final String TRANSACTION_PENDING = Config.getProperty("TRANSACTION_PENDING");

    public static final String TRANSACTION_TYPE_PAY = "pay";

    public static final String TRANSACTION_TYPE_AXFER = "axfer";

    public static final String TRANSACTION_TYPE_ACFG = "acfg";

    public static final Long ASSETID_USDC = Long.parseLong(Config.getProperty("ASSETID_USDC"));

    public static final Long ASSETID_STEPTOKEN = Long.parseLong(Config.getProperty("ASSETID_STEPTOKEN"));

    public static final Long TRANSACTION_FEE = Long.parseLong(Config.getProperty("TRANSACTION_FEE"));

    public static final String GENESIS_HASH = Config.getProperty("GENESIS_HASH");

    public static final String GENESIS_ID = Config.getProperty("GENESIS_ID");

    /* PAGO Constants **********/

    public static final String PAGO_ENVELOPE_ROUTE = Config.getProperty("PAGO_ENVELOPE_ROUTE");

    public static final String PAGO_API_ADDR = Config.getProperty("PAGO_API_ADDR");


}
