package org.onestep.relief.utility.blockchain.util;

public class PagoEnvelopeConstants {

    public static final String TRANSFER_REQUEST = Config.getPagoProperty("TRANSFER_REQUEST");
    public static final String ENVELOPE_ID = Config.getPagoProperty("ENVELOPE_ID");
    public static final String AGREEMENT_DESCRIPTION = Config.getPagoProperty("AGREEMENT_DESCRIPTION");
    public static final String ID = Config.getPagoProperty("ID");
    public static final String NAME = Config.getPagoProperty("NAME");
    public static final String DESCRIPTION = Config.getPagoProperty("DESCRIPTION");
    public static final String APPLICATION_ID = Config.getPagoProperty("APPLICATION_ID");
    public static final String STATE = Config.getPagoProperty("STATE");


}
