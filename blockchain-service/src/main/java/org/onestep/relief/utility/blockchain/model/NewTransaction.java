package org.onestep.relief.utility.blockchain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewTransaction {


    private AccountModel sender;

    private AccountModel receiver;

    private String note;

    private Long amount;

    private String transactionType;

    private Long assetId;

    private String name;

    private Integer total;

    private String unit;

    private Integer decimals;

    private Boolean frozen;

    private String manager_addr;

    private String reserve_addr;

    private String freeze_addr;

    private String clawback_addr;

    private String  url;

}
