package org.onestep.relief.utility.blockchain.service.impl;

import com.algorand.algosdk.crypto.Address;
import com.algorand.algosdk.v2.client.common.Response;
import com.algorand.algosdk.v2.client.model.AssetHolding;
import com.algorand.algosdk.v2.client.model.Transaction;
import com.algorand.algosdk.v2.client.model.TransactionsResponse;
import org.onestep.relief.utility.blockchain.dto.BalanceHistoryResponse;
import org.onestep.relief.utility.blockchain.dto.BalancesResponse;
import org.onestep.relief.utility.blockchain.model.AbstractTransactionHistory;
import org.onestep.relief.utility.blockchain.model.AccountBalance;
import org.onestep.relief.utility.blockchain.model.AccountHistory;
import org.onestep.relief.utility.blockchain.model.Asset;
import org.onestep.relief.utility.blockchain.service.IAccountService;
import org.onestep.relief.utility.blockchain.util.HTTPRequests;
import org.onestep.relief.utility.blockchain.util.TransactionHistoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

public class AccountService implements IAccountService {

    private Logger logger = LoggerFactory.getLogger(AccountService.class);

    private CommonService service;
    private HTTPRequests httpRequests;

    public AccountService(CommonService service) {

        this.service = service;
    }

    @Override
    public com.algorand.algosdk.account.Account createAccount() {

        com.algorand.algosdk.account.Account account = null;

        try {

            account = new com.algorand.algosdk.account.Account();

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account: " + e.getMessage());
        }

        return account;
    }

    @Override
    public AccountBalance getAccountBalance(String accountAddress) throws Exception {

        Address account;
        Response<com.algorand.algosdk.v2.client.model.Account> response;
        AccountBalance balance = new AccountBalance();

        try {
            account = new Address(accountAddress);

            response = AlgoClient.getInstance().AccountInformation(account)
                    .execute(new String [] {API_KEY_HEADER}, new String [] {PURESTAKE_API_KEY});

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account: " + e.getMessage());
            throw e;
        }

        if (Objects.nonNull(response)) {

            List<AssetHolding> assets = response.body().assets;

            for(AssetHolding asset:assets){
                String name = null;
                if (asset.assetId.equals(ASSETID_USDC)) {
                    name = "USDC";
                } else  if (asset.assetId.equals(ASSETID_STEPTOKEN)) {
                    name = "Step Token";
                }
                balance.getAssets().add(new Asset(asset.amount.longValue(), asset.creator,
                        asset.assetId, name));
            }

            balance.setAlgoBalance(response.body().amount);
        }

        return balance;

    }

    @Override
    public AccountHistory getAccountHistory(String accountAddress) {

        Address account;
        Response<TransactionsResponse> response = null;
        AccountHistory history = new AccountHistory();

        try {
            account = new Address(accountAddress);

            response = IndexClient.getInstance().lookupAccountTransactions(account)
                    .execute(new String [] {API_KEY_HEADER}, new String [] {PURESTAKE_API_KEY});

        } catch (GeneralSecurityException e) {
            logger.error("Unable to create account" + e.getMessage());
        } catch (Exception e) {
            logger.error("Unable to get account information" + e.getMessage());
        }


        if (Objects.nonNull(response)) {

            List<Transaction> transactions = response.body().transactions;

            for(com.algorand.algosdk.v2.client.model.Transaction txn :transactions){

                AbstractTransactionHistory transaction = TransactionHistoryFactory.createTransaction(txn);

                history.getTransactions().add(transaction);
            }
        }

        return history;
    }

    @Override
    public BalancesResponse getBalanceHistory(String accountAddress, int days)  {

        javax.ws.rs.core.Response response;
        String path = ALGO_EXPLORER_API;
        path = path.replace("{address}", accountAddress);
        path = path.replace("{days}",String.valueOf(days));

        try {
            response = HTTPRequests.GetRequest(path);
        } catch (Exception e){
            throw e;
        }
        ArrayList<BalanceHistoryResponse> balances = (ArrayList<BalanceHistoryResponse>) response.readEntity(List.class);
        logger.info(String.valueOf(balances));

        BalancesResponse balancesResponse = new BalancesResponse();
        balancesResponse.setBalanceHistory(balances);
        return balancesResponse;

    }
}
