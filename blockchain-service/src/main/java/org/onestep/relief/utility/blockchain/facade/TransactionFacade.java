package org.onestep.relief.utility.blockchain.facade;

import com.algorand.algosdk.account.Account;
import com.algorand.algosdk.builder.transaction.AssetAcceptTransactionBuilder;
import com.algorand.algosdk.transaction.SignedTransaction;
import com.algorand.algosdk.transaction.Transaction;
import com.algorand.algosdk.v2.client.common.AlgodClient;
import com.algorand.algosdk.v2.client.model.AssetHolding;
import com.algorand.algosdk.v2.client.model.PendingTransactionResponse;
import com.algorand.algosdk.v2.client.model.TransactionParametersResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.onestep.relief.utility.blockchain.dto.*;
import org.onestep.relief.utility.blockchain.model.*;
import org.onestep.relief.utility.blockchain.service.ICommonService;
import org.onestep.relief.utility.blockchain.service.IPagoService;
import org.onestep.relief.utility.blockchain.service.ITransactionService;
import org.onestep.relief.utility.blockchain.service.impl.AlgoClient;
import org.onestep.relief.utility.blockchain.service.impl.CommonService;
import org.onestep.relief.utility.blockchain.service.impl.PagoService;
import org.onestep.relief.utility.blockchain.util.BlockchainUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.GeneralSecurityException;
import java.util.*;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.*;

/**
 * Layer of abstraction to handle exception with logging and propagate it
 */
public class TransactionFacade {

    private final Logger logger = LoggerFactory.getLogger(TransactionFacade.class);

    private final ITransactionService service;
    private final ICommonService commonService;
    private final IPagoService pagoService;
    private final DozerBeanMapper dozerMapper;

    private final String[] headers = {API_KEY_HEADER};
    private final String[] values = {PURESTAKE_API_KEY};


    public TransactionFacade(ITransactionService service, CommonService commonService, PagoService pagoService) {
        this.service = service;
        this.commonService = commonService;
        this.dozerMapper = new DozerBeanMapper();
        this.pagoService = pagoService;

    }


    /**
     * Send Algos. If mnemonic isn't passed then it tries to create a Pago transaction
     *
     * @param request sendTransactionRequest. If public/private key are passed the creates directly
     *                on Blockchain, otherwise checks if payId/public key are valid Pago and then sends
     *                to Payment gateway.
     * @return Generic response object containing either Blockchain TxId or PagoId. PagoId can later be checked to
     * determine the status.
     * @throws Exception Any errors such as invalid Id, etc.
     */
    public GenericResponse<String> sendAlgo(SendTransactionRequest request) throws Exception {

        Transaction transaction;

        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        newTransaction.setTransactionType("pay");

        if (request.getSender().getMnemonic() != null &&
                !request.getSender().getMnemonic().equals("") ) {
            try {

                transaction = service.createTransaction(newTransaction);

                SignedTransaction signedTransaction = service.signTransaction(
                        new com.algorand.algosdk.account.Account(request.getSender().getMnemonic()),
                        transaction);

                return new GenericResponse<>("Algos sent. TxId: ",
                        service.postTransaction(signedTransaction));

            } catch (GeneralSecurityException e) {
                logger.error("Unable to send Algos: " + e.getMessage());
                throw e;
            }

        } else {
            AccountModel requestingAccount = dozerMapper.map(request.getSender(), AccountModel.class);

            String pagoId = requestingAccount.getPayId() != null ? requestingAccount.getPayId() : requestingAccount.getAddress();
            String pagoAccountId;

            /*
             * 1) Confirm Pago account
             */
            pagoAccountId = service.confirmPagoAccount(pagoId);

            /*
             * 2) If Pago account then can create a Pago transaction
             */
            var blockchainAddress = service.getAddressFromPago(pagoAccountId);

            TransactionEnvelopeRequest envelopeRequest =
                    pagoService.createEnvelope(pagoService.mapTransaction(request, blockchainAddress));

            String pagoIxId = pagoService.submitTransactionEnvelope(envelopeRequest);

            return new GenericResponse<>("Pago Request submitted. Pago txId: ", pagoIxId);


        }
    }


    /**
     * Send USDC. If mnemonic isn't passed then it tries to create a Pago transaction
     *
     * @param request sendTransactionRequest. If public/private key are passed the creates directly
     *                on Blockchain, otherwise checks if payId/public key are valid Pago and then sends
     *                to Payment gateway.
     * @return Generic response object containing either Blockchain TxId or PagoId. PagoId can later be checked to
     * determine the status.
     * @throws Exception Any errors such as invalid Id, etc.
     */
    public GenericResponse<String> sendUsdc(SendTransactionRequest request) throws Exception {

        Transaction transaction;

        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        newTransaction.setTransactionType("axfer");
        newTransaction.setAssetId(ASSETID_USDC);
        if (request.getSender().getMnemonic() != null &&
                !request.getSender().getMnemonic().equals("")) {
            try {

                transaction = service.createTransaction(newTransaction);

                SignedTransaction signedTransaction = service.signTransaction(
                        new com.algorand.algosdk.account.Account(request.getSender().getMnemonic()),
                        transaction);

                return new GenericResponse<>("USDC sent. TxId: ",
                        service.postTransaction(signedTransaction));

            } catch (GeneralSecurityException e) {
                logger.error("Unable to send usdc: " + e.getMessage());
                throw e;
            }

        } else {
            AccountModel requestingAccount = dozerMapper.map(request.getSender(), AccountModel.class);

            String pagoId = requestingAccount.getPayId() != null ? requestingAccount.getPayId() : requestingAccount.getAddress();
            String pagoAccountId;

            /*
             * 1) Confirm Pago account
             */

            pagoAccountId = service.confirmPagoAccount(pagoId);

            /*
             * 2) If Pago account then can create a Pago transaction
             */
            var blockchainAddress = service.getAddressFromPago(pagoAccountId);

            request.setAssetId(ASSETID_USDC);

            TransactionEnvelopeRequest envelopeRequest =
                    pagoService.createEnvelope(pagoService.mapTransaction(request, blockchainAddress));

            String pagoIxId = pagoService.submitTransactionEnvelope(envelopeRequest);

            return new GenericResponse<>("Pago Request submitted. Pago txId: ", pagoIxId);
        }
    }

    public GenericResponse<AssetIdResponse> sendXferAsset(SendTransactionRequest request) throws Exception {

        Transaction transaction;

        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        newTransaction.setTransactionType("axfer");
        Long assetId = newTransaction.getAssetId();

        if (request.getSender().getMnemonic() != null &&
                !request.getSender().getMnemonic().equals("")) {
            try {

                transaction = service.createTransaction(newTransaction);

                // The transaction must be signed by the sender account
                SignedTransaction signedTx = service.signTransaction(
                        new com.algorand.algosdk.account.Account(request.getSender().getMnemonic()),
                        transaction);

                String txId = service.postTransaction(signedTx);
                AssetIdResponse assetIdResponse = new AssetIdResponse(assetId, txId);
                return new GenericResponse<>("Asset Transferred Successfully", assetIdResponse);

            } catch (GeneralSecurityException e) {
                logger.error("Unable to transfer asset: " + e.getMessage());
                throw e;
            }
        } else {
            AccountModel requestingAccount = dozerMapper.map(request.getSender(), AccountModel.class);

            String pagoId = requestingAccount.getPayId() != null ? requestingAccount.getPayId() : requestingAccount.getAddress();
            String pagoAccountId;

            /*
             * 1) Confirm Pago account
             */

            pagoAccountId = service.confirmPagoAccount(pagoId);

            /*
             * 2) If Pago account then can create a Pago transaction
             */
            var blockchainAddress = service.getAddressFromPago(pagoAccountId);

            request.setAssetId(request.getAssetId());
            TransactionEnvelopeRequest envelopeRequest =
                    pagoService.createEnvelope(pagoService.mapTransaction(request, blockchainAddress));

            String pagoIxId = pagoService.submitTransactionEnvelope(envelopeRequest);

            AssetIdResponse assetIdResponse = new AssetIdResponse(assetId, pagoIxId);
            return new GenericResponse<>("Pago Request submitted ", assetIdResponse);
        }
    }


    /**
     * Takes a JSON Post request and creates a o amount transaction on the blockchain.
     * Used to store an immutable hash of an electronic object.
     *
     * @param request Object mapped from the JSON POST request
     * @return Transaction ID if successfully posted
     */
    public GenericResponse<String> createAuthDoc(AuthDocumentTransactionRequest request) throws Exception {

        Transaction transaction;
        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        newTransaction.setTransactionType("pay");
        newTransaction.setAmount(0L);

        if (request.getSender().getMnemonic() != null &&
                !request.getSender().getMnemonic().equals("")) {
            try {
                newTransaction.setReceiver(new AccountModel(request.getSender().getAddress()));

                transaction = service.createTransaction(newTransaction);

                SignedTransaction signedTransaction = service.signTransaction(
                        new com.algorand.algosdk.account.Account(request.getSender().getMnemonic()),
                        transaction);

                return new GenericResponse<>("Auth doc transaction created. TxId: ",
                        service.postTransaction(signedTransaction));

            } catch (GeneralSecurityException e) {
                logger.error("Unable to create transaction: " + e.getMessage());
                throw e;
            }

        } else {
            AccountModel requestingAccount = dozerMapper.map(request.getSender(), AccountModel.class);

            String pagoId = requestingAccount.getPayId() != null ? requestingAccount.getPayId() : requestingAccount.getAddress();
            String pagoAccountId;


            /*
             * 1) Confirm Pago account
             */
            pagoAccountId = service.confirmPagoAccount(pagoId);

            /*
             * 2) If Pago account then can create a Pago transaction
             */
            //Need to convert from AuthDocumentTransactionRequest to SendTransactionRequest
            SendTransactionRequest pagoTransactionRequest = dozerMapper.map(request, SendTransactionRequest.class);
            var blockchainAddress = service.getAddressFromPago(pagoAccountId);
            pagoTransactionRequest.setReceiver(new AccountModel(blockchainAddress));

            TransactionEnvelopeRequest envelopeRequest =
                    pagoService.createEnvelope(pagoService.mapTransaction(pagoTransactionRequest, blockchainAddress));

            String pagoIxId = pagoService.submitTransactionEnvelope(envelopeRequest);

            return new GenericResponse<>("Pago Request submitted. Pago txId: ", pagoIxId);
        }
    }


    /**
     * Takes a JSON Post request and creates a new asset
     *
     * @param request Object mapped from the JSON POST request
     * @return Transaction ID if successfully posted
     */
    public GenericResponse<AssetIdResponse> createAsset(CreateAssetRequest request) throws Exception {

        AlgodClient algoClient = AlgoClient.getInstance();
        Long assetID;

        Transaction transaction;
        NewTransaction newTransaction = dozerMapper.map(request, NewTransaction.class);
        AccountModel sender = new AccountModel(request.getCreator_addr(), request.getCreator_mnemonic());
        newTransaction.setSender(sender);
        newTransaction.setTransactionType("acfg");

        try {

            transaction = service.createTransaction(newTransaction);

            SignedTransaction signedTransaction = service.signTransaction(
                    new com.algorand.algosdk.account.Account(sender.getMnemonic()),
                    transaction);

            String txId = service.postTransaction(signedTransaction);
            System.out.println("Transaction ID: " + txId);

            // method expects a JSON object for txId, because it's used by other classes
            // that pass this. So must convert
            //TODO unify input to String not JSON for this method

            // TODO Hack to fix the unit testing.
            PendingTransactionResponse pTrx;
            AssetIdResponse assetIdResponse = null;
            if (txId != null) {
                pTrx = BlockchainUtils.waitForConfirmation(algoClient,
                        new JSONObject().put("txId", txId).toString(), headers, values);


                // Now that the transaction is confirmed we can get the assetID
                assetID = pTrx.assetIndex;
                System.out.println("AssetID = " + assetID.toString());

                assetIdResponse = new AssetIdResponse(assetID, txId);
            }
            return new GenericResponse<>("Created asset successfully", assetIdResponse);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    public GenericResponse<String> destroyAsset(Long assetId) throws Exception {

        AlgodClient algoClient = AlgoClient.getInstance();

        Map<String, Object> response = BlockchainUtils.getMnemonic();

        String mnemonic = response.get("mnemonic").toString();
        String address = response.get("address").toString();

        Transaction transaction;
        NewTransaction newTransaction = new NewTransaction();
        AccountModel sender = new AccountModel(address, mnemonic);
        newTransaction.setTransactionType("destroy");
        newTransaction.setSender(sender);
        newTransaction.setAssetId(assetId);

        try {

            transaction = service.createTransaction(newTransaction);

            SignedTransaction signedTransaction = service.signTransaction(
                    new com.algorand.algosdk.account.Account(sender.getMnemonic()),
                    transaction);

            String txId = service.postTransaction(signedTransaction);
            System.out.println("Transaction ID: " + txId);

            // method expects a JSON object for txId, because it's used by other classes
            // that pass this. So must convert
            //TODO unify input to String not JSON for this method

            // Don't care about response, just waiting for transaction to be confirmed
            BlockchainUtils.waitForConfirmation(algoClient,
                    new JSONObject().put("txId", txId).toString(), headers, values);

            return new GenericResponse<>("Asset Destroyed", txId);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    /**
     * Opt in to Receiving Asset
     * https://algorand.github.io/java-algorand-sdk/com/algorand/algosdk/builder/transaction/AssetAcceptTransactionBuilder.html
     *
     * @return json string with transaction
     * @throws Exception bad transaction
     */
    public GenericResponse<AssetIdResponse> optIn(SendTransactionRequest request) throws Exception {

        try {
            if (!hasEnoughAlgos(request.getSender().getAddress(), 1)) {
                logger.error("Insufficient Algos to Opt-in asset: ");
                throw new Exception("Insufficient Algos to Opt-in asset: ");
            }

        } catch (Exception e) {
            logger.error("Account access error" + e.getMessage());
            throw e;
        }

        request.setReceiver(request.getSender());
        request.setAmount(0L);
        request.setNote("Opt in Asset Id: " + request.getAssetId());

        GenericResponse<AssetIdResponse> response = sendXferAsset(request);
        response.setMessage("Opt in Successful");
        return response;


    }


    /**
     * Opt in to Receiving USDC
     * https://algorand.github.io/java-algorand-sdk/com/algorand/algosdk/builder/transaction/AssetAcceptTransactionBuilder.html
     *
     * @return json string with transaction
     * @throws Exception bad transaction
     */
    public String optInUsdc(SendTransactionRequest request) throws Exception {

        Account account = new Account(request.getSender().getMnemonic());

        try {
            if (hasUsdc(account)) {
                logger.error("account already opted in for USDC");
                throw new Exception("account already opted in for USDC");
            }

            if (!hasEnoughAlgos(account.getAddress().toString(), 1)) {
                logger.error("not enough algos to opt in for USDC");
                throw new Exception("not enough algos to opt in for USDC");
            }

        } catch (Exception e) {
            logger.error("account access error: " + e.getMessage());
            throw e;
        }

        try {

            TransactionParametersResponse params = service.getTransactionParameters();
            params.fee = TRANSACTION_FEE;

            Transaction tx = AssetAcceptTransactionBuilder.Builder()
                    .acceptingAccount(account.getAddress())
                    .assetIndex(ASSETID_USDC)
                    .suggestedParams(params)
                    .noteUTF8("Opt in USDC: Asset ID " + ASSETID_USDC)
                    .build();

            SignedTransaction signedTx = account.signTransaction(tx);
            //return service.postTransaction(signedTx);

            return new JSONObject().put("txId", service.postTransaction(signedTx)).toString();

        } catch (Exception e) {
            logger.error("unable to opt in for USDC: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Check if the account has usdc already
     *
     * @return boolean
     * @throws Exception on error
     */
    private Boolean hasUsdc(com.algorand.algosdk.account.Account account) throws Exception {

        com.algorand.algosdk.v2.client.model.Account accountInfo = commonService
                .getAccountInfo(account.getAddress().toString());

        for (AssetHolding asset : accountInfo.assets) {

            if (ASSETID_USDC == asset.assetId || ASSETID_USDC.equals(asset.assetId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if enough Algos to opt-in
     *
     * @param address Account algorand address
     * @return boolean
     * @throws Exception errors
     */
    private Boolean hasEnoughAlgos(String address, long numNewAssets) throws Exception {

        Long minBalance = 100000L + TRANSACTION_FEE + (numNewAssets * 100000); //100K for blockchain, 100K per asset, + fee
        com.algorand.algosdk.v2.client.model.Account accountInfo = commonService
                .getAccountInfo(address);

        // Get count of current assets to calculate minBalance
        int assetCount = accountInfo.createdAssets.size();
        minBalance += assetCount * 100000L;

        return accountInfo.amount >= minBalance;
    }


    public Asset getAsset(long assetId) throws Exception {

        try {
            return service.getAsset(assetId);
        } catch (Exception e) {
            logger.error("Unable to get asset: " + e.getMessage());
            throw e;
        }
    }

    public AbstractTransactionHistory getTransactionFromIndexer(String transactionID) throws Exception {

        try {
            return service.getTransaction(transactionID);
        } catch (Exception e) {
            logger.error("Unable to get transaction " + e.getMessage());
            throw e;
        }
    }

    public Entity getTransactionStatus(String transactionId) throws Exception {

        PendingTransactionResponse pendingResponse;

        try {
            pendingResponse = service.getTransactionStatus(transactionId);

            if (Objects.nonNull(pendingResponse)) {

                TransactionConfirmation response = new TransactionConfirmation();
                response.setApplicationIndex(pendingResponse.applicationIndex);
                response.setClosingAmount(pendingResponse.closingAmount);
                response.setConfirmedRound(pendingResponse.confirmedRound);
                response.setCloseRewards(pendingResponse.closeRewards);

                return response;
            }
        } catch (Exception e) {
            logger.error("Unable to get transaction status: " + e.getMessage());
            throw e;
        }

        return new ErrorResponse(TRANSACTION_PENDING);
    }

    public ProvisionPayResponse provisionUserAccount(SendTransactionRequest request) throws Exception {

        AccountModel requestingAccount = dozerMapper.map(request.getSender(), AccountModel.class);

        // So the account to provision can either pass in their payId OR their public address
        String pagoId = requestingAccount.getPayId() != null ? requestingAccount.getPayId() : requestingAccount.getAddress();
        String pagoAccountId;
        String addressToProvision;
        AccountModel accountToOptIn = new AccountModel();
        ProvisionPayResponse provisionResponse = new ProvisionPayResponse();

        /*
         * 1) Confirm Pago account
         */

        pagoAccountId = service.confirmPagoAccount(pagoId);

        /*
         * 2) If Pago account add Algos if needed
         */


        /*
         * 2A) Get public blockchain address
         */
        addressToProvision = service.getAddressFromPago(pagoAccountId);

        /*
         * 2B) check Algo balance
         */
        if (!this.hasEnoughAlgos(addressToProvision, 2)) {

            /* Need to add Algos
             * 2C) Get 'faucet' account credentials from AWS Secrets Manager
             */
            String faucetAccount;
            try {
                faucetAccount = BlockchainUtils.getSecret();

            } catch (Exception e) {
                logger.info(e.getMessage());
                throw e;

            }
            //Map the address & mnemonic retrieved from AWS so that we can get the private key later
            Map<String, Object> response;
            try {
                response = new ObjectMapper().readValue(faucetAccount, HashMap.class);
            } catch (Exception e) {
                logger.info(e.getMessage());
                throw e;

            }

            String faucetMnemonic = response.get("mnemonic").toString();
            String faucetAddress = response.get("address").toString();

            String txId;

            SendTransactionRequest requestAddAlgos = new SendTransactionRequest()
                    .setAmount(400000L)
                    .setNote("Initial new account provisioning")
                    .setReceiver(accountToOptIn)
                    .setSender(new AccountModel(faucetAddress, faucetMnemonic));

            try {
                txId = this.sendAlgo(requestAddAlgos).getData();

            } catch (Exception e) {
                logger.info(e.getMessage());
                throw e;
            }

            AlgodClient algoClient = AlgoClient.getInstance();

            //Need to wait until the add Algos is on the blockchain, otherwise the opt-in
            // transactions below will fail.
            try {
                //Ignore the response, we don't need it.
                BlockchainUtils.waitForConfirmation(algoClient, txId, headers, values);
            } catch (Exception e) {
                logger.info(e.getMessage());
                throw e;
            }


        }
        /*
         * Enough Algos now so we can opt in
         *
         * Opt in USDC
         */
        accountToOptIn.setAddress(addressToProvision);
        accountToOptIn.setPayId(requestingAccount.getPayId());

        request.setSender(accountToOptIn);
        request.setAssetId(ASSETID_USDC);
        request.setReceiver(accountToOptIn);
        request.setAmount(0L);
        request.setNote("Opt in Asset USDC");

        // Add blockchain address to return

        provisionResponse.setBlockchainAddress(addressToProvision);


        /*
         * 4) Send to Pago API for approval
         */
        GenericResponse<AssetIdResponse> response = sendXferAsset(request);
        ProvisionPayResponse.Asset toAdd = new ProvisionPayResponse.Asset(response.getData().getAssetId().toString(),
                response.getData().getTxId());
        provisionResponse.addAsset(toAdd);
        /*
         * 5) Opt-in StepTokens
         */
        request.setAssetId(ASSETID_STEPTOKEN);
        request.setNote("Opt in Asset StepTokens");

        /*
         * 6) Send to Pago API for approval
         */
        response = sendXferAsset(request);
        toAdd = new ProvisionPayResponse.Asset(response.getData().getAssetId().toString(),
                response.getData().getTxId());
        provisionResponse.addAsset(toAdd);

        return provisionResponse;
    }
}