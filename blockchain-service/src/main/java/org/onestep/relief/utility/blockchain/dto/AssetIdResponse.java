package org.onestep.relief.utility.blockchain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetIdResponse {

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long assetId;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String txId;


}
