package org.onestep.relief.utility.blockchain.service;

import com.algorand.algosdk.v2.client.model.BlockResponse;
import com.algorand.algosdk.v2.client.model.NodeStatusResponse;

import java.security.NoSuchAlgorithmException;

public interface ICommonService {

    NodeStatusResponse checkNodeStatus();

    BlockResponse getBlockInfo();

    com.algorand.algosdk.v2.client.model.Account getAccountInfo(String address) throws Exception;
}

