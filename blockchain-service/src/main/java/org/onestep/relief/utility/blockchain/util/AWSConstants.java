package org.onestep.relief.utility.blockchain.util;

/**
 * AWS Secrets Manager is used to store public/private keys of a Blockchain 'faucet' account, used for
 * provisioning new account with nominal Algos. This is the login info for the Secrets Manager.
 */
public class AWSConstants {


    public static final String BLOCKCHAIN_SECRETSMANAGER_ID = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_ID");
    public static final String BLOCKCHAIN_SECRETSMANAGER_SECRET = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_SECRET");
    public static final String AWS_SECRETSMANAGER_REGION = Config.getProperty("AWS_SECRETSMANAGER_REGION");
    public static final String SECRETS_MANAGER_KEY = Config.getProperty("SECRETS_MANAGER_KEY");

}
