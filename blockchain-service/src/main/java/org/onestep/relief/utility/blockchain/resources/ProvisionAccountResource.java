package org.onestep.relief.utility.blockchain.resources;

import com.algorand.algosdk.v2.client.common.AlgodClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.utility.blockchain.dto.GenericResponse;
import org.onestep.relief.utility.blockchain.dto.ProvisionAccountRequest;
import org.onestep.relief.utility.blockchain.facade.AccountFacade;
import org.onestep.relief.utility.blockchain.facade.TransactionFacade;
import org.onestep.relief.utility.blockchain.model.AccountModel;
import org.onestep.relief.utility.blockchain.model.ErrorResponse;
import org.onestep.relief.utility.blockchain.dto.SendTransactionRequest;
import org.onestep.relief.utility.blockchain.service.impl.AlgoClient;
import org.onestep.relief.utility.blockchain.util.BlockchainUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.API_KEY_HEADER;
import static org.onestep.relief.utility.blockchain.util.BlockchainConstants.PURESTAKE_API_KEY;

/**
 * Source - https://github.com/logicappsio/LogicAppsAsyncResponseSample/blob/master/AsyncResponse/Controllers/AsyncController.cs
 */
@Path("onestep/blockchain/provision")
@Produces(MediaType.APPLICATION_JSON)
public class ProvisionAccountResource {

    //State dictionary for sample - stores the state of the working thread
    private static Map<String, Boolean> runningTasks = new HashMap<>();

    // If we get exceptions on our new thread we'll store the first one and then pass it back
    // when polled
    private static Map<String, String> exceptions = new HashMap<>();
    private Logger logger = LoggerFactory.getLogger(ProvisionAccountResource.class);
    /**
     * Facade classes that interfaces with Algorand SDK.
     * Hides the implementation details from Api clients
     */
    private TransactionFacade transactionFacade;
    private AccountFacade accountFacade;
    private ProvisionAccountRequest accountResponseDTO;
    private SendTransactionRequest sendTransactionRequest;


    public ProvisionAccountResource(TransactionFacade transactionFacade, AccountFacade accountFacade) {

        this.transactionFacade = transactionFacade;
        this.accountFacade = accountFacade;
        //Holds txId's, balances, private & public keys to return to client
        accountResponseDTO = new ProvisionAccountRequest();
        sendTransactionRequest = new SendTransactionRequest();
    }

    /** Used to create and provision a 'fund' account. No pre-requisites, as code
     * creates account from scratch.
     *
     * Creates a new account generating key pairs, provision account with 1.5 Algos
     * and opts-in to USDC. POST response returns link to check status of
     * long running operation.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response provisionFundAccount() {

        String id = UUID.randomUUID().toString();  //Generate tracking Id
        runningTasks.put(id, false);  //Job isn't done yet

        String[] headers = {API_KEY_HEADER};
        String[] values = {PURESTAKE_API_KEY};

        // run everything in new Thread so that  this POST request can return immediately
        // with a '202 Accepted' and the link to check on status
        new Thread(() -> {
            //1) Create new account and capture address/mnemonic
            AccountModel newAccount = accountFacade.createAccount();
            accountResponseDTO.setAddress(newAccount.getAddress());
            accountResponseDTO.setMnemonic(newAccount.getMnemonic());

            sendTransactionRequest.setSender(newAccount);

            //2a) Get 'faucet' account credentials from AWS Secrets Manager
            String sourceAccount = null;
            try {
                sourceAccount = BlockchainUtils.getSecret();

            } catch (Exception e) {
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage());

            }
            //Map the address & mnemonic retrieved from AWS so that we can get the private key later
            Map<String, Object> response = null;
            try {
                response = new ObjectMapper().readValue(sourceAccount, HashMap.class);
            } catch (Exception e) {
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage()); //i.e. log the first exception only

            }

            String mnemonic = null;
            String address = null;
            if (response != null) {
                mnemonic = response.get("mnemonic").toString();
                address = response.get("address").toString();
            }

            //2) Add nominal amount of Algos
            GenericResponse<String> txId = null;

            SendTransactionRequest request = new SendTransactionRequest()
                    .setAmount(1500000L)
                    .setNote("Initial new account provisioning")
                    .setReceiver(newAccount)
                    .setSender(new AccountModel(address, mnemonic));


            try {
                txId = transactionFacade.sendAlgo(request);
                accountResponseDTO.setSendAlgoTxId(txId.getData());
                logger.info(String.valueOf(txId));

            } catch (Exception e) {
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage());
            }

            AlgodClient algoClient = AlgoClient.getInstance();

            try {
                //Ignore the response, we don't need it.
                logger.info("Trying for confirmation "+ txId.getData());
                BlockchainUtils.waitForConfirmation(algoClient, accountResponseDTO.getSendAlgoTxId(), headers, values);
            } catch (Exception e) {
                logger.info("Catching. see below...");
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage());
            }

            //3) Now opt-in to USDC for new account, which was the whole purpose of this exercise
            String txId2;
            try {
                txId2 = transactionFacade.optInUsdc(sendTransactionRequest);
                accountResponseDTO.setOptInTxId(BlockchainUtils.parseJson(txId2, "txId"));
            } catch (Exception e) {
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage());
            }

            try {
                accountResponseDTO.setBalance(accountFacade.getAccountBalance(newAccount.getAddress()));
            } catch (Exception e) {
                logger.info(e.getMessage());
                exceptions.putIfAbsent(id, e.getMessage());
            }

            runningTasks.put(id, true);  //Job is complete
        }).start();
        return Response.status(202)
                .header("location", "onestep/blockchain/provision/" + id + "/status")
                .header("retry-after", "5")
                .build();
    }


    @GET
    @Path("/{uuid}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus(@PathParam("uuid") String uuid) {

        if (exceptions.containsKey(uuid)) {
            String exception = exceptions.get(uuid);

            exceptions.remove(uuid); // Clear exception from Map
            runningTasks.remove(uuid); // Clear current thread from runningTasks

            return Response.status(500)
                    .entity(new ErrorResponse("Unable to provision new account: "
                            + exception)).build();
        }
        //If the job is complete
        else if (runningTasks.containsKey(uuid) && runningTasks.get(uuid)) {
            runningTasks.remove(uuid);
            return Response.status(200)
                    .entity(accountResponseDTO).build();

        }
        //If the job is still running
        else if (runningTasks.containsKey(uuid)) {
            return Response.status(202)
                    .header("location", "onestep/blockchain/provision/" + uuid + "/status")
                    .header("retry-after", "5")
                    .build();
        } else {
            return Response.status(400, "No job exists with the specified ID")
                    .build();
        }
    }

    /** Provisions a new user's account by adding minimal amount of Algos and then opting in to USDC and
     * StepTokens. User MUST alread have valid PayId
     * @param request Transactin request with basic information required
     * @return Reponse object with Pago ID's
     */
    @POST
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response provisionUserAccount(SendTransactionRequest request) {

        try {
            return Response.status(200)
                    .entity(transactionFacade.provisionUserAccount(request)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to provision user: "+ e.getMessage())).build();
        }
    }
}
