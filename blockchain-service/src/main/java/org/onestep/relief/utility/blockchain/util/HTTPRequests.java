package org.onestep.relief.utility.blockchain.util;


import org.glassfish.jersey.client.JerseyClientBuilder;
import org.onestep.relief.utility.blockchain.dto.TransactionEnvelopeRequest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class HTTPRequests {

    static Client client = new JerseyClientBuilder().build();

    public HTTPRequests(Client client) {
        this.client = client;
    }


    public static Response GetRequest(String path) {

        WebTarget webTarget = client.target(path);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.get();
    }

    //TODO Generic input vice SendTransactionRequest
    public  static <T> Response PostRequest(String url, String path,T request) {


        WebTarget webTarget = client.target(url).path(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildPost(Entity.entity(request, MediaType.APPLICATION_JSON));

        Response response = invocation.invoke();

        return response;
    }

}

