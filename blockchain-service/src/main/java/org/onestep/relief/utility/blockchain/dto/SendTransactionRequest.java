package org.onestep.relief.utility.blockchain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.utility.blockchain.model.AccountModel;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendTransactionRequest {


    public SendTransactionRequest(Long amount) {
        this.amount = amount;
    }

    private AccountModel sender;
    private AccountModel receiver;

    private String note;

    private Long amount;

    private String transactionType;

    private Long assetId;

    private String callbackURL;

    /**
     * builder pattern for ease of construction of object
    */
    public SendTransactionRequest setSender(AccountModel sender)
    { this.sender = sender;
        return this;
    }

    public SendTransactionRequest setReceiver(AccountModel receiver)
    { this.receiver = receiver;
        return this;
    }

    public SendTransactionRequest setNote(String note)
    { this.note = note;
        return this;
    }

    public SendTransactionRequest setAmount(Long amount)
    { this.amount = amount;
        return this;
    }

    public SendTransactionRequest setTransactionType(String transactionType)
    { this.transactionType = transactionType;
        return this;
    }

    public SendTransactionRequest setAssetId(Long assetId)
    { this.assetId = assetId;
        return this;
    }
}
