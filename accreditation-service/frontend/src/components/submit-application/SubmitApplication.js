import React, { useEffect, useState } from 'react'
import { useLocation, Link, useHistory } from 'react-router-dom'
import {
  TextField,
  Box,
  InputLabel,
  Select,
  MenuItem,
  Button,
  capitalize
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Lightbox from 'react-image-lightbox'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'

const useStyles = makeStyles((theme) => ({
  entityName: {
    minWidth: 700
  },
  icon: {
    marginLeft: 20,
    minWidth: 665
  },
  role: {
    minWidth: 785
  },
  document: {
    minWidth: 600
  },
  notes: {
    minWidth: 600
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

const retrieveData = async (path, authHelper) => {
  const res = await authHelper.Get(
    `${process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL}/${path}`
  )
  return res.data
}

const supportedFileTypes =
  /(\.jpg|\.jpeg|\.pdf|\.bmp|\.gif|\.tiff|\.raw|\.png)$/i

export const SubmitApplication = (props) => {
  const authHelper = props.authHelper

  const [entity, setEntity] = useState('')
  const [roles, setRoles] = useState([])
  const [notes, setNotes] = useState('')
  const [entityType, setEntityType] = useState('')
  const [selectedRole, setSelectedRole] = useState('')
  const [requiredDocuments, setRequiredDocuments] = useState([])
  const [uploadedDocuments, setUploadedDocuments] = useState([])
  const [accreditationState, setAccreditationState] = useState('')
  const [accreditationId, setAccreditationId] = useState('')
  const [selectedFiles, setSelectedFiles] = useState([])
  const [unsupportedFileTypeError, setUnsupportedFileTypeError] =
    useState(false)
  // eslint-disable-next-line no-unused-vars
  const [documentTypeNotSelectedError, setDocumentTypeNotSelectedError] =
    useState(false)
  const [emptyFieldsError, setEmptyFieldsError] = useState(false)

  const classes = useStyles()
  const location = useLocation()
  const history = useHistory()

  const [popImage, setPopImage] = useState('')
  const [isOpen, setIsOpen] = useState(false)
  const [openModal, setOpenModal] = React.useState(false)

  const handleOpenModal = () => {
    setOpenModal(true)
  }

  const handleCloseModal = () => {
    setOpenModal(false)
  }

  // if entity type passed in the URL as a request param
  if (props && props.entityType) {
    setEntityType(props.entityType)
  }

  const path = location.pathname.split('/')

  // Obtain accreditationId from route
  const accrId = path[path.length - 1]

  // Obtain action from route
  const action = path[path.length - 2]

  const isViewMode = action === 'view'
  const isApplicationLocked = accreditationState === 'Approved'

  useEffect(async () => {
    const retrieveRoles = async (_path) => {
      const data = await retrieveData(_path, authHelper)
      setRoles(data.roles)
    }

    const retrieveOrgData = async (_path) => {
      const orgData = await authHelper.Get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/${_path}/token`
      )
      setEntity(orgData.data)
      retrieveRoles(`application/${entity.uuid}/roles`)
    }

    const retrieveApplication = async (_path) => {
      const data = await retrieveData(_path, authHelper)

      console.log('app 1 ', data)

      setEntityType(data.entityType)

      if (data && data.accreditationState) {
        setAccreditationState(data.accreditationState)
      }
      setSelectedRole(data.role)
      setAccreditationState(data.accreditationState)

      if (data && data.accreditationId) {
        setAccreditationId(data.accreditationId)
      }

      let docs = []

      if (data && data.documents) {
        for (const doc of data.documents) {
          docs = [
            ...docs,
            {
              document: doc.docType,
              imageUrl: doc.tempLink,
              docType: doc.docType,
              fileName: doc.fileName
            }
          ]
        }
      }

      setRequiredDocuments(docs)

      if (data && data.userNote) {
        setNotes(data.userNote.comments)
      }
    }

    if (isViewMode || action === 'edit') {
      retrieveApplication('application/' + accrId)
    }

    retrieveOrgData('persons/requester')
  }, [])

  const accreditation = {
    accreditationId: accreditationId || null,
    entityId: entity.uuid,
    payId:
      entity.paymentAccount && entity.paymentAccount.payID
        ? entity.paymentAccount.payID
        : 'Default Pay ID',
    address:
      entity.paymentAccount && entity.paymentAccount.blockchainAddress
        ? entity.paymentAccount.blockchainAddress
        : 'Default Blockchain Address',
    userNote: {
      date: new Date().toString(),
      comments: notes || 'Documents have been provided'
    },
    documents: uploadedDocuments,
    role: selectedRole,
    entityType: entityType || 'PER',
    accreditationState: accreditationState || 'Pending'
  }

  const handleShowDialog = (img) => {
    setIsOpen(true)
    setPopImage(img)
  }

  const handleSelectedRole = (event) => {
    setSelectedRole(event.target.value)

    roles.forEach((aRole) => {
      if (aRole.role === event.target.value) {
        let docs = []

        for (const doc of aRole.documents) {
          docs = [
            ...docs,
            { document: doc, imageUrl: null, docType: doc, fileName: null }
          ]
        }
        setRequiredDocuments(docs)
      }
    })
  }

  const handleNotes = (event) => {
    setNotes(event.target.value)

    if (event.target.value.length > 3) {
      setEmptyFieldsError(false)
    }
  }

  const handleUploadingFile = (event, doc) => {
    setUploadedDocuments([
      ...uploadedDocuments,
      {
        docType: doc,
        fileName: event.target.files[0].name
      }
    ])

    // Only the first document is considered
    const file = event.target.files[0]

    const objectUrl = URL.createObjectURL(file)

    if (supportedFileTypes.exec(file.name)) {
      setUnsupportedFileTypeError(false)
    }

    let docs = []

    for (const aDoc of requiredDocuments) {
      if (aDoc.document === doc) {
        docs = [
          ...docs,
          { document: aDoc.document, imageUrl: objectUrl, fileName: file.name }
        ]
      } else {
        docs = [...docs, aDoc]
      }
    }

    setRequiredDocuments(docs)

    setSelectedFiles([...selectedFiles, file])
  }

  const submitApplication = async (formData, _path) => {
    const response = await authHelper.Post(
      `${process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL}/${_path}`,
      formData
    )

    if (response.status === 200) {
      history.push('/accreditation')
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    handleOpenModal()

    let error = false

    selectedFiles.forEach((file) => {
      // Only a limited type of documents are supported
      if (!supportedFileTypes.exec(file.name)) {
        setUnsupportedFileTypeError(true)
        error = true
      } else {
        setUnsupportedFileTypeError(false)
      }
    })

    // (todo_comment) - do this validations again
    if (
      !unsupportedFileTypeError &&
      !documentTypeNotSelectedError &&
      !emptyFieldsError
    ) {
      const formData = new FormData()

      for (const file of selectedFiles) {
        formData.append('file', file)
      }

      formData.append('accreditation', JSON.stringify(accreditation))

      if (!error) {
        submitApplication(formData, 'application')
      }
    }
  }

  return (
    <div style={{ justifyContent: 'center', marginTop: 50, marginLeft: 300 }}>
      <Box fontWeight='fontWeightBold' ml={5} mb={3}>
        Accreditation Management
      </Box>
      <Box display='flex' p={1}>
        <Box px={5}>
          <InputLabel>Applicant or Organization Name</InputLabel>
          <TextField
            className={classes.entityName}
            id='entity-name'
            disabled
            multiline
            value={entity.name || ''}
          />
        </Box>
      </Box>
      <Box display='flex' p={1} ml={5} my={2}>
        <Box px={5}>
          <InputLabel>Applying for</InputLabel>
          <TextField
            className={classes.entityName}
            id='entity-type'
            disabled
            multiline
            style={{ textTransform: capitalize }}
            value={entityType || 'Person' || 'Person'}
          />
        </Box>
      </Box>
      <Box display='flex' pt={3} pl={1}>
        <Box px={5} className={classes.role}>
          <InputLabel>Role</InputLabel>
          <Select
            labelId='role'
            id='role'
            defaultValue=''
            value={selectedRole}
            onChange={handleSelectedRole}
            fullWidth
          >
            {roles &&
              roles.map((aRole) => (
                <MenuItem value={aRole.role} key={aRole.role}>
                  {aRole.role}
                </MenuItem>
              ))}
          </Select>
        </Box>
      </Box>
      <Box display='flex' pt={3} pl={1}>
        <Box px={5}>
          <div>
            <div style={{ minWidth: 200 }}>
              {requiredDocuments &&
                requiredDocuments.map((doc) => {
                  return (
                    <div key={doc.fileName} style={{ marginTop: 25 }}>
                      <InputLabel>{doc.document}</InputLabel>
                      <div
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          flexWrap: 'wrap'
                        }}
                      >
                        {doc && doc.imageUrl && (
                          <img
                            className='image'
                            src={doc.imageUrl || ''}
                            onClick={() => handleShowDialog(doc.imageUrl)}
                            alt='no image'
                            height='100'
                            width='100'
                          />
                        )}
                        {doc && doc.fileName && (
                          <InputLabel style={{ marginLeft: 25 }}>
                            {doc.fileName || ''}
                          </InputLabel>
                        )}
                        <Button
                          variant='contained'
                          component='label'
                          style={{ marginLeft: 200, marginRight: 100 }}
                        >
                          <span style={{ textTransform: 'capitalize' }}>
                            Upload Document
                          </span>
                          <input
                            type='file'
                            hidden
                            onChange={(e) =>
                              handleUploadingFile(e, doc.document)
                            }
                          />
                        </Button>
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>
        </Box>
      </Box>
      <Box display='flex'>
        <Box px={5} mt={4}>
          <InputLabel>
            Additional Notes (Explain if you can not supply some or all of
            required documents)
          </InputLabel>
          <TextField
            id='notes'
            multiline
            rows={6}
            fullWidth
            className={classes.notes}
            value={notes || ''}
            variant='outlined'
            onChange={handleNotes}
          />
        </Box>
      </Box>
      {unsupportedFileTypeError && (
        <span className='error' style={{ color: 'red' }}>
          * Unsupported file type. Supported file types are (jpg, jpeg, pdf,
          bmp, gif, tiff, raw, png) <br />
        </span>
      )}
      {documentTypeNotSelectedError && (
        <span className='error' style={{ color: 'red' }}>
          * You need to select the nature of the document you uploaded from the
          dropdown <br />
        </span>
      )}
      {emptyFieldsError && (
        <span className='error' style={{ color: 'red' }}>
          * You need to upload required documents or explain why you can not do
          so <br />
        </span>
      )}
      {isOpen && (
        <div style={{ alignItems: 'center' }}>
          <Lightbox
            mainSrc={popImage}
            onCloseRequest={() => setIsOpen(false)}
          />
        </div>
      )}
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-around',
          width: 600,
          marginLeft: 70,
          marginTop: 40
        }}
      >
        <Button
          variant='contained'
          onClick={handleSubmit}
          color='primary'
          disabled={isViewMode || isApplicationLocked}
        >
          <span style={{ textTransform: 'capitalize' }}>Submit</span>
        </Button>
        <Button
          variant='outlined'
          onClick={handleSubmit}
          color='secondary'
          disabled={isViewMode || isApplicationLocked}
        >
          <span style={{ textTransform: 'capitalize' }}>Save for later</span>
        </Button>
        <Link to='/accreditation'>
          <Button variant='outlined' color='primary'>
            <span style={{ textTransform: 'capitalize' }}>
              Back to Homepage
            </span>
          </Button>
        </Link>
      </div>
      <Modal
        aria-labelledby='transition-modal-title'
        aria-describedby='transition-modal-description'
        className={classes.modal}
        open={openModal}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={openModal}>
          <div className={classes.paper}>
            <h2 id='transition-modal-title'>Saving your Application</h2>
            <p id='transition-modal-description'>Processing your Application</p>
          </div>
        </Fade>
      </Modal>
    </div>
  )
}

export default SubmitApplication
