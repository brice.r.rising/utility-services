import React, { useEffect, useState } from 'react'
import {
  Box,
  Button,
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper
} from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  table: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: 50,
    marginLeft: theme.spacing(40),
    width: 800
  }
}))

const retrieveData = async (path, authHelper) => {
  const res = await authHelper.Get(
    `${process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL}/${path}`
  )
  return res.data
}

export const HomePage = (props) => {
  const authHelper = props.authHelper

  const [entity, setEntity] = useState('')
  const [applications, setApplications] = useState([])
  const history = useHistory()
  const classes = useStyles()

  useEffect(async () => {
    const retrievePastApplications = async (path) => {
      const applications = await retrieveData(path, authHelper)
      setApplications(applications)
    }

    const retrieveOrgData = async (path) => {
      const orgData = await authHelper.Get(
        `${process.env.REACT_APP_ORG_MODULE_API_URL}/${path}/token`
      )
      retrievePastApplications(`application/${orgData.data.uuid}/entity`)
      setEntity(orgData.data)
    }

    retrieveOrgData('persons/requester')
  }, [])

  const handleSubmitNewApplication = () => {
    history.push('/accreditation/apply')
  }

  const deleteApplication = async (accrId) => {
    const response = await authHelper.Delete(
      `${process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL}/application/${accrId}`
    )

    if (response.status === 200) {
      alert('Successfully deleted application')
      window.location.reload()
    }
  }

  const handleRemove = (accrId) => {
    const popup = window.confirm('Are you sure you want to delete this item?')
    if (popup) {
      deleteApplication(accrId)
    }
  }

  return (
    <div>
      <Box fontWeight='fontWeightBold' mt={5} ml={5}>
        Accreditation Management
      </Box>
      <Box mt={3} ml={5} fontStyle='italic'>
        Welcome, {entity.name}
      </Box>
      <div style={{ marginLeft: 35, marginTop: 25 }}>
        <Button
          variant='contained'
          color='primary'
          onClick={handleSubmitNewApplication}
        >
          Submit a new Application
        </Button>
      </div>
      {/* Shows a list of previously submitted applications for the user */}
      <TableContainer component={Paper} className={classes.table}>
        <Box my={3} ml={5}>
          <span fontStyle='bold'>Accreditation Summary for</span>
          <span fontStyle='italic'> {entity.name}</span>
        </Box>
        <Table aria-label='Past Applications'>
          <TableHead>
            <TableRow>
              <TableCell align='left'>Role</TableCell>
              <TableCell align='left'>Status</TableCell>
              <TableCell align='left'>Submission Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {applications &&
              applications.map((application) => (
                <TableRow key={application.accreditationId || ''}>
                  <TableCell align='left'>{application.role || ''}</TableCell>
                  <TableCell align='left'>
                    {application.accreditationState || ''}
                  </TableCell>
                  <TableCell align='left'>{application.date || ''}</TableCell>
                  <TableCell align='left'>
                    <Button
                      href={
                        '/accreditation/apply/edit/' +
                        application.accreditationId
                      }
                      disabled={application.accreditationState === 'Approved'}
                      variant='outlined'
                      color='primary'
                    >
                      Edit
                    </Button>
                  </TableCell>
                  <TableCell align='left'>
                    <Button
                      href={
                        '/accreditation/apply/view/' +
                        application.accreditationId
                      }
                      variant='outlined'
                      color='primary'
                    >
                      View
                    </Button>
                  </TableCell>
                  <TableCell align='left'>
                    <Button
                      onClick={() => handleRemove(application.accreditationId)}
                      variant='contained'
                      color='secondary'
                    >
                      Remove
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}
