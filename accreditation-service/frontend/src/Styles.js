import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: 100
  },
  formParent: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: 50,
    marginLeft: theme.spacing(40),
    width: 800
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch'
  },
  phoneIcon: {
    marginLeft: theme.spacing(1)
  },
  radioRight: {
    marginRight: theme.spacing(5)
  },
  formControl: {
    margin: theme.spacing(2),
    minWidth: 400
  },
  table: {
    marginTop: 50,
    marginBottom: 70
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1,
    color: 'white'
  },
  tabs: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  }
}))

export default useStyles
