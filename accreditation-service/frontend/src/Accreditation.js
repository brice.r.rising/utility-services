import SubmitApplication from './components/submit-application/SubmitApplication'
import React, { useEffect, useState } from 'react'
import { Auth, Hub } from 'aws-amplify'
import useStyles from './Styles'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { HomePage } from './components/landing-page/HomePage'
import { AuthHelper } from '@cscie599sec1spring21/authentication-helper'
import * as QueryString from 'query-string'

export const Accreditation = (props) => {
  const [user, setUser] = useState(null)

  let authHelper = null

  // If main UI doesn't pass authHelper prop
  if (!props || !props.authHelper) {
    useEffect(async () => {
      try {
        const authenticateUser = await Auth.currentAuthenticatedUser()
        // This line sets the user in the cache (by invoking useState from above)
        setUser(authenticateUser)
        console.log('User ', user)
      } catch (error) {
        console.log('Not authenticated, redirecting to authentication page.')
        // If .currentAuthenticatedUser throws, it means user is not logged in
        // proceed to call the federated login
        Auth.federatedSignIn()
      }

      // This is aws-amplify specific code, this listens for callbacks from aws-cognito
      Hub.listen('auth', async ({ payload: { event, data } }) => {
        switch (event) {
          case 'signIn':
          case 'cognitoHostedUI':
            try {
              const authenticateUser = await Auth.currentAuthenticatedUser()
              setUser(authenticateUser)
            } catch (error) {
              // At this point user should be authenticated, getting an error
              // means there could be another failure which we should log.
              console.log(`Not signed in when it should, error: ${error}`)
            }
            break
          case 'signOut':
            // Signing out event, clear user from cache, to logout a user call:
            // Auth.signOut()
            setUser(null)
            break
          case 'signIn_failure':
          case 'cognitoHostedUI_failure':
            console.log('Sign in failure', data)
            break
        }
      })
    }, [])

    authHelper = new AuthHelper()
  } else {
    authHelper = props.authHelper
  }

  const classes = useStyles()

  let url = ''

  // If path is passed a prop
  if (props && props.match && props.match.path) {
    url = props.match.path
  } else {
    url = '/accreditation'
  }

  let entityId = null
  let entityType = null

  if (location.search) {
    const queryParams = QueryString.parse(location.search)

    if (queryParams) {
      if (queryParams.entityId) {
        entityId = queryParams.entityId
      }
      if (queryParams.entityType) {
        entityType = queryParams.entityType
      }
    }
  }

  return (
    <Router>
      <div className={classes.root}>
        <Route
          path={url}
          exact
          render={() => <HomePage authHelper={authHelper} />}
        />
        <Route
          path={`${url}/apply`}
          exact
          render={() => (
            <SubmitApplication
              authHelper={authHelper}
              entityId={entityId}
              entityType={entityType}
            />
          )}
        />
        <Route
          path={`${url}/apply/edit/:accrId`}
          exact
          render={() => (
            <SubmitApplication
              authHelper={authHelper}
              entityId={entityId}
              entityType={entityType}
            />
          )}
        />
        <Route
          path={`${url}/apply/view/:accrId`}
          exact
          render={() => (
            <SubmitApplication
              authHelper={authHelper}
              entityId={entityId}
              entityType={entityType}
            />
          )}
        />
      </div>
    </Router>
  )
}

export default Accreditation
