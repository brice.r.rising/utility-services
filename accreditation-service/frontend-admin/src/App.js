import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import React, { useEffect, useState } from 'react'

import AppHelper from './components/AppHelper'
import Amplify, { Auth, Hub } from 'aws-amplify'

if (process.env.REACT_APP_REACT_APP_ENVIRONMENT == 'DEBUG') {
  Amplify.configure({
    userPoolId: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_ID}`,
    region: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_REGION}`,
    identityPoolRegion: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_IDENTITY_POOL_REGION}`,
    userPoolWebClientId: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_USER_POOL_WEB_CLIENT_ID}`,
    oauth: {
      domain: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_DOMAIN}`,
      scope: [
        'phone',
        'email',
        'openid',
        'profile',
        'aws.cognito.signin.user.admin'
      ],
      redirectSignIn: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_REDIRECT_SIGN_IN}`,
      redirectSignOut: `${process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_USER_POOL_REDIRECT_SIGN_OUT}`,
      responseType: 'code'
    }
  })
}

export default function AccreditationAdmin({ authHelper }) {
  if (process.env.REACT_APP_REACT_APP_ENVIRONMENT == 'DEBUG') {
    console.log('DEBUG MODE')

    const [user, setUser] = useState(null)

    useEffect(async () => {
      try {
        var authenticateUser = await Auth.currentAuthenticatedUser()
        // This line sets the user in the cache (by invoking useState from above)
        setUser(authenticateUser)
        console.log(`user info is: ${user}`)
      } catch (error) {
        console.log('Not authenticated, redirecting to authentication page.')
        // If .currentAuthenticatedUser throws, it means user is not logged in
        // proceed to call the federated login
        Auth.federatedSignIn()
      }

      // This is aws-amplify specific code, this listens for callbacks from aws-cognito
      Hub.listen('auth', async ({ payload: { event, data } }) => {
        switch (event) {
          case 'signIn':
          case 'cognitoHostedUI':
            try {
              var authenticateUser = await Auth.currentAuthenticatedUser()
              setUser(authenticateUser)
            } catch (error) {
              // At this point user should be authenticated, getting an error
              // means there could be another failure which we should log.
              console.log(`Not signed in when it should, error: ${error}`)
            }
            break
          case 'signOut':
            // Signing out event, clear user from cache, to logout a user call:
            // Auth.signOut()
            setUser(null)
            break
          case 'signIn_failure':
          case 'cognitoHostedUI_failure':
            console.log('Sign in failure', data)
            break
        }
      })
    }, [])
  }

  return (
    <div>
      <AppHelper authHelper={authHelper} />
    </div>
  )
}
//
