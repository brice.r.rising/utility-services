import React, { useEffect, useState } from 'react'
import Details from './Details'
import Bar from './Bar'
import Tables from './Tables'
import { AuthHelper } from '@cscie599sec1spring21/authentication-helper'

/**
 * Layout and Data Controller
 * @returns {JSX.Element}
 * @constructor
 */
export default function AppHelper({ authHelper }) {
  // only make new authHelper in DEBUG
  if (!authHelper) {
    authHelper = new AuthHelper()
  }

  // table data
  const [tableJson, setTableJson] = useState([])

  // control which table is shown ; set by sidebar
  // eslint-disable-next-line no-unused-vars
  const [tableState, setTableState] = useState(null)

  // callback to change table
  const updateTableCallback = (text) => {
    setTableState(text)
    setViewState('Table')

    let url

    // choose which set of applications to get
    switch (text) {
      case 'Pending Approval':
        url = new URL(
          '?state=Pending',
          process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
        ).href
        break
      case 'Approved':
        url = new URL(
          '?state=Approved',
          process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
        ).href
        break
      case 'Rejected':
        url = new URL(
          '?state=Rejected',
          process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
        ).href
        break
      default:
        url = new URL(
          '?state=Pending',
          process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
        ).href
        break
      // code block
    }

    // call accreditation service, get list of applications to show on screen
    authHelper
      .Get(url)
      .then((response) => {
        if (response.statusText === 'OK') {
          // change null to string "null", to prevent crash when building table
          const table = JSON.parse(
            JSON.stringify(response.data).replace(/null/g, '"-NONE-"')
          )
          // sort by date
          // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare
          table
            .sort((rowA, rowB) => {
              return (
                new Date(rowA.date).getTime() - new Date(rowB.date).getTime()
              )
            })
            .reverse()
          // first show table without names
          setTableJson(table)
          let count = 0
          // map user name from org module
          // eslint-disable-next-line array-callback-return
          table.map((row) => {
            const url =
              process.env
                .REACT_APP_ACCREDITATION_FRONTEND_ADMIN_ORGANIZATION_URI +
              'persons/' +
              row.entityId
            authHelper
              .Get(url)
              .then((response) => {
                if (response.statusText === 'OK') {
                  row.applicantName = response.data.name
                  // update table with names
                } else {
                  console.log('Name not found error CUYYZAT ' + row.entityId)
                }
                count++
                // refresh table if all rows were processed
                if (count >= Object.keys(table).length) {
                  setTableJson([]) // need to set blank to recognize state change
                  setTableJson(table)
                }
              })
              .catch((error) => {
                console.log('Name request error PRCCEUX ', error)
              })
          })
        } else {
          alert('Data not found error DZTVMRY')
        }
      })
      .catch((error) => {
        console.log('Data access error VENQUCL ' + error)
      })
  }

  // store single applicant data
  const [appJson, setAppJson] = useState('')

  // control if table or detail is shown
  const [viewState, setViewState] = useState('Table')

  // call back to switch between table or application
  const updateViewCallback = async (viewSelect) => {
    if (viewSelect !== 'Table') {
      const url = new URL(
        viewSelect.accreditationId,
        process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
      ).href
      authHelper
        .Get(url)
        .then((response) => {
          if (response.statusText === 'OK') {
            setAppJson(
              JSON.parse(
                JSON.stringify(response.data).replace(/null/g, '"-NONE-"')
              )
            )
            setViewState(
              JSON.parse(
                JSON.stringify(response.data).replace(/null/g, '"-NONE-"')
              )
            )
          } else {
            alert('Application Not Found Error EGWVNVY')
          }
        })
        .catch((error) => {
          console.log('Data request error EFDLUWK ' + error)
        })
    }
  }

  // sub-layout
  const MainContents = () => {
    return (
      <div>
        {/* show table */}
        {viewState === 'Table' && (
          <Tables
            tableJson={tableJson}
            updateViewCallback={updateViewCallback}
          />
        )}
        {/* show application details */}
        {viewState !== 'Table' && (
          <Details applicantJson={appJson} authHelper={authHelper} />
        )}
      </div>
    )
  }

  // Show pending apps on load
  useEffect(() => {
    // Run this only once on load
    updateTableCallback('Pending')
  }, [])

  // main layout
  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='col-12 px-0 position-relative navbarClass'>
          <Bar
            updateViewCallback={updateViewCallback}
            updateTableCallback={updateTableCallback}
          />
        </div>
      </div>
      <div className='appHeaderClass'>
        <h4>Accreditation Management</h4>
      </div>
      <div className='row'>
        <div className='col-12 px-0 position-relative mainContentClass'>
          <MainContents />
        </div>
      </div>
    </div>
  )
}
