import React, { useState } from 'react'
import { Card, Table } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid'

// create your forceUpdate hook
function useForceUpdate() {
  // eslint-disable-next-line no-unused-vars
  const [value, setValue] = useState(0) // integer state
  return () => setValue((value) => value + 1) // update the state to force render
}

/**
 * Render table of applications
 * @param tableJson
 * @param updateViewCallback
 * @returns {JSX.Element}
 * @constructor
 */
export default function Tables({ tableJson, updateViewCallback }) {
  const forceUpdate = useForceUpdate()

  const [sortKeyState, setSortKeyState] = useState('date')
  const [sortOrderState, setSortOrderState] = useState('desc')

  const onSort = (sortKey) => {
    if (sortKey === sortKeyState) {
      if (sortOrderState === 'desc') {
        tableJson.sort((a, b) => {
          return (a[sortKey] || '-NONE-').localeCompare(b[sortKey] || '-NONE-')
        })
        setSortOrderState('asc')
      } else {
        tableJson
          .sort((a, b) => {
            return (a[sortKey] || '-NONE-').localeCompare(
              b[sortKey] || '-NONE-'
            )
          })
          .reverse()
        setSortOrderState('desc')
      }
    } else {
      tableJson.sort((a, b) => {
        return (a[sortKey] || '-NONE-').localeCompare(b[sortKey] || '-NONE-')
      })
      setSortOrderState('asc')
    }
    setSortKeyState(sortKey)
    forceUpdate()
  }

  const dateSort = (sortKey) => {
    if (sortKey === sortKeyState) {
      if (sortOrderState === 'desc') {
        tableJson.sort((a, b) => {
          return new Date(a[sortKey]).getTime() - new Date(b[sortKey]).getTime()
        })
        setSortOrderState('asc')
      } else {
        tableJson
          .sort((a, b) => {
            return (
              new Date(a[sortKey]).getTime() - new Date(b[sortKey]).getTime()
            )
          })
          .reverse()
        setSortOrderState('desc')
      }
    } else {
      tableJson
        .sort((a, b) => {
          return new Date(a[sortKey]).getTime() - new Date(b[sortKey]).getTime()
        })
        .reverse()
      setSortOrderState('desc')
    }
    setSortKeyState(sortKey)
    forceUpdate()
  }

  // person or org
  const typeMap = {
    PER: 'Person',
    ORG: 'Organization'
  }

  // show applicants table
  return (
    <div className='tableDivClass'>
      <Card className='cardTableClass'>
        <Table bordered hover className='tableClass'>
          <thead>
            <tr>
              <th className='thClass' onClick={() => dateSort('date')}>
                Date
              </th>
              <th className='thClass' onClick={() => onSort('applicantName')}>
                Name
              </th>
              <th className='thClass' onClick={() => onSort('role')}>
                Role
              </th>
              <th
                className='thClass'
                onClick={() => onSort('accreditationState')}
              >
                State
              </th>
              <th className='thClass' onClick={() => onSort('accreditationId')}>
                Accreditation Id
              </th>
              <th className='thClass' onClick={() => onSort('entityType')}>
                Type
              </th>
            </tr>
          </thead>
          <tbody>
            {tableJson.map((row) => (
              <tr
                key={row.accreditationId + uuidv4()}
                onClick={() => updateViewCallback(row)}
              >
                <td className='tdClass'>
                  {new Date(row.date).toLocaleString([], {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                  })}
                </td>
                <td className='tdClass'>{row.applicantName || '-NONE-'}</td>
                <td className='tdClass'>{row.role}</td>
                <td className='tdClass'>{row.accreditationState}</td>
                <td className='tdClass'>{row.accreditationId}</td>
                <td className='tdClass'>
                  {typeMap[row.entityType] || '-NONE-'}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Card>
    </div>
  )
}
