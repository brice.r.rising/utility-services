import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Nav, Navbar } from 'react-bootstrap'

/**
 * Renders side navbar
 * @param updateViewCallback
 * @param updateTableCallback
 * @returns {JSX.Element}
 * @constructor
 */
export default function Bar({ updateViewCallback, updateTableCallback }) {
  const [selectionState, setSelectionState] = useState('Pending Approval')
  return (
    <Navbar className='navbarClass'>
      <Nav>
        {/* create top bar options */}
        {['Pending Approval', 'Approved', 'Rejected'].map((text, index) => (
          <Nav.Link
            key={text}
            className={
              selectionState === text
                ? 'linkSelectClass border'
                : 'linkUnselectClass'
            }
            href='#'
            onClick={() => {
              setSelectionState(text)
              updateTableCallback(text)
              updateViewCallback('Table')
            }}
          >
            {text}
          </Nav.Link>
        ))}
      </Nav>
    </Navbar>
  )
}
