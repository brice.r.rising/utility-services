import React, { useRef, useState } from 'react'
import { Card, Table } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'

const yaml = require('js-yaml')

/**
 * Render Applicant Details
 * @param applicantJson
 * @returns {JSX.Element}
 * @constructor
 */
export default function Details({ applicantJson, authHelper }) {
  // prevent undefined access of vars
  let adminNote = ''
  let userNote = ''
  if (applicantJson.adminNote !== undefined) {
    adminNote = yaml.safeDump(applicantJson.adminNote.comments)
  }
  if (applicantJson.userNote !== undefined) {
    userNote = yaml.safeDump(applicantJson.userNote.comments)
  }

  // state of applicant name
  const [applicantName, setApplicantName] = useState(' ')

  // get applicant name
  const url =
    process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_ORGANIZATION_URI +
    'persons/' +
    applicantJson.entityId
  authHelper
    .Get(url)
    .then((response) => {
      if (response.statusText === 'OK') {
        setApplicantName(response.data.name)
      } else {
        setApplicantName('-NONE-')
      }
    })
    .catch((error) => {
      console.log('Technical error RRKUMFE', error)
    })

  // link to approve
  const handleApprove = (id, notes) => {
    const url = new URL(
      id + '/approve',
      process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
    ).href
    const data = {
      comments: notes.current.value,
      date: new Date(),
      user: applicantName
    }

    authHelper
      .Post(url, data)
      .then((response) => {
        if (response.statusText === 'OK' || response.status === 200) {
          alert('Application Approved')
          location.reload()
        } else {
          alert('Technical Error; please contact the administrator UKUZZNB')
        }
      })
      .catch((error) => {
        alert(
          'Technical Error; please contact the administrator SBPEMCR ' + error
        )
      })
  }

  // link to reject
  const handleReject = (id, notes) => {
    const url = new URL(
      id + '/reject',
      process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
    ).href
    const data = {
      comments: notes.current.value,
      date: new Date(),
      user: applicantName
    }

    authHelper
      .Post(url, data)
      .then((response) => {
        if (response.statusText === 'OK' || response.status === 200) {
          alert('Application Rejected')
          location.reload()
        } else {
          alert('Technical Error; please contact the administrator GMLYULP')
        }
      })
      .catch((error) => {
        alert(
          'Technical Error; please contact the administrator RQMSKDX ' + error
        )
      })
  }

  // link to audit
  const handleAudit = (fileName, id) => {
    const url = new URL(
      `audit?fileName=${fileName}&accreditationId=${id}`,
      process.env.REACT_APP_ACCREDITATION_FRONTEND_ADMIN_APPLICATION_URI
    ).href

    authHelper
      .Post(url)
      .then((response) => {
        if (response.statusText === 'OK' || response.status === 200) {
          if (
            response.data === true ||
            response.data === 'true' ||
            response.data === 'True'
          ) {
            alert(`${fileName} Audit PASSED!`)
          } else if (
            response.data === false ||
            response.data === 'false' ||
            response.data === 'False'
          ) {
            alert(`${fileName} Audit FAILED!`)
          } else {
            alert('Technical Error; please contact the administrator SRSCJCF')
          }
        } else {
          alert('Technical Error; please contact the administrator YYFEJXL')
        }
      })
      .catch((error) => {
        alert(
          'Technical Error; please contact the administrator VVJGYGG ' + error
        )
      })
  }

  // set to open modal image
  const handleImageClick = (img) => {
    setPopImage(img)
    setIsOpen(true)
  }

  const [popImage, setPopImage] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const approveRejectNotes = useRef(null)

  return (
    <div className='detailClass'>
      <Card className='cardTableClass'>
        <h4>Application {applicantJson.accreditationId}</h4>
        <div>
          <input className='spanClass' disabled={true} value='Name:' />
          <input className='inputClass' disabled={true} value={applicantName} />
        </div>
        <div>
          <input className='spanClass' disabled={true} value='User Id:' />
          <input
            className='inputClass'
            disabled={true}
            value={applicantJson.entityId}
          />
        </div>
        <div>
          <input className='spanClass' disabled={true} value='State:' />
          <input
            className='inputClass'
            disabled={true}
            value={applicantJson.accreditationState}
          />
        </div>
        <div>
          <input className='spanClass' disabled={true} value='Date:' />
          <input
            className='inputClass'
            disabled={true}
            value={applicantJson.date}
          />
        </div>
        <div>
          <input className='spanClass' disabled={true} value='Role:' />
          <input
            className='inputClass'
            disabled={true}
            value={applicantJson.role}
          />
        </div>
        <div>
          <input className='spanClass' disabled={true} value='User Note:' />
        </div>
        <div>
          <textarea
            className='textareaClass'
            disabled={true}
            value={userNote}
          />
        </div>
        {(applicantJson.accreditationState === 'Approved' ||
          applicantJson.accreditationState === 'Rejected') && (
          <div>
            <input className='spanClass' disabled={true} value='Admin Note:' />
          </div>
        )}
        {(applicantJson.accreditationState === 'Approved' ||
          applicantJson.accreditationState === 'Rejected') && (
          <div>
            <textarea
              className='textareaClass'
              disabled={true}
              value={adminNote}
            />
          </div>
        )}
        <div className='text-right'>
          {applicantJson.accreditationState === 'Pending' && (
            <div>
              <textarea
                className='adminNoteClass'
                placeholder='Add approve or reject comments'
                ref={approveRejectNotes}
              />
            </div>
          )}
          {applicantJson.accreditationState === 'Pending' && (
            <button
              type='button'
              className='btn btn-primary decisionButtonClass'
              onClick={() =>
                handleApprove(applicantJson.accreditationId, approveRejectNotes)
              }
            >
              <span className='buttonClass'>Approve</span>
            </button>
          )}
          {applicantJson.accreditationState === 'Pending' && (
            <button
              type='button'
              className='btn btn-primary decisionButtonClass'
              onClick={() =>
                handleReject(applicantJson.accreditationId, approveRejectNotes)
              }
            >
              <span className='buttonClass'>Reject</span>
            </button>
          )}
        </div>
      </Card>

      <div className='docClass'>
        <Card className='cardDocClass'>
          <h4>Documents</h4>
          <Table className='tableClass'>
            <thead>
              <tr>
                <th className='thClass'>Date</th>
                <th className='thClass'>Type</th>
                <th className='thClass'>Name</th>
                <th className='thClass'>Preview</th>
                <th className='thClass'></th>
              </tr>
            </thead>
            <tbody>
              {applicantJson.documents.map((doc) => (
                <tr key={doc.bucket + doc.fileName + uuidv4()}>
                  <td className='tdClass'>{doc.date}</td>
                  <td className='tdClass'>{doc.docType}</td>
                  <td className='tdClass'>
                    <a href='#' onClick={() => handleImageClick(doc.tempLink)}>
                      {doc.fileName}
                    </a>
                  </td>
                  <td className='tdClass'>
                    <img
                      src={doc.tempLinkThumb}
                      onClick={() => handleImageClick(doc.tempLink)}
                    />
                  </td>
                  <td className='tdClass'>
                    {applicantJson.accreditationState === 'Approved' && (
                      <button
                        type='button'
                        className='btn btn-primary auditButtonClass'
                        onClick={() =>
                          handleAudit(
                            doc.fileName,
                            applicantJson.accreditationId
                          )
                        }
                      >
                        <span className='buttonClass'>Audit</span>
                      </button>
                    )}
                    {applicantJson.accreditationState !== 'Approved' && (
                      <button
                        disabled
                        type='button'
                        className='btn btn-primary auditButtonClass'
                        onClick={() =>
                          handleAudit(
                            doc.fileName,
                            applicantJson.accreditationId
                          )
                        }
                      >
                        <span className='buttonClass'>Audit</span>
                      </button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      </div>
      {isOpen && (
        <Lightbox mainSrc={popImage} onCloseRequest={() => setIsOpen(false)} />
      )}
    </div>
  )
}
