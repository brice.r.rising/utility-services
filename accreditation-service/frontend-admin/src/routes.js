import React from 'react'
import { Redirect } from 'react-router-dom'

// Layout Types
import { DefaultLayout } from './layouts'

// Route Views
import Table from './components/Tables'

export default [
  {
    path: '/',
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to='/blog-overview' />
  },
  {
    path: '/tables',
    layout: DefaultLayout,
    component: Table
  }
]
