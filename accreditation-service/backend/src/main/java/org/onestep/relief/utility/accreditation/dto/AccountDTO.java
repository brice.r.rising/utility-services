package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {

    public AccountDTO(String address) {
        this.address = address;
    }

    private String address;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mnemonic;
}
