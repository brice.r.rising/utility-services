package org.onestep.relief.utility.accreditation.health;

import com.codahale.metrics.health.HealthCheck;

public class AppHealthCheck extends HealthCheck
{
    @Override
    protected HealthCheck.Result check() throws Exception {

        if(true){
            return Result.healthy();
        }

        return Result.unhealthy("Error message");
    }
}
