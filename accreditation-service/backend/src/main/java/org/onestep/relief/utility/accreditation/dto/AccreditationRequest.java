package org.onestep.relief.utility.accreditation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccreditationRequest {

    private String accreditationId;

    private String entityId;

    private String payId;

    private String address;

    private NoteDTO userNote;

    private List<DocumentRequest> documents = new ArrayList<>();

    @Mapping("this")
    private String role;

    @Mapping("this")
    private String entityType;

    @Mapping("this")
    private String accreditationState;


}

