package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueRequest {

    @JsonProperty
    private String reason;

    @JsonProperty
    private String rewardType;

    @JsonProperty
    @NotNull
    private Integer amount;

    @JsonProperty
    private String receiverId;

    @JsonProperty
    private ReceiverType receiverType;

    public enum ReceiverType {
        Person,
        Org
    }

}

