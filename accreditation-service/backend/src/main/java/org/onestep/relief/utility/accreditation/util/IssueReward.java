package org.onestep.relief.utility.accreditation.util;

import org.onestep.relief.utility.accreditation.dto.IssueRequest;
import org.onestep.relief.utility.accreditation.dto.IssueResponse;
import org.onestep.relief.utility.accreditation.dto.ServiceLoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.ACCREDITATION_SERVICE_LOGIN_ID;
import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.ACCREDITATION_SERVICE_LOGIN_KEY;

public class IssueReward {


    private static final Logger logger = LoggerFactory.getLogger(IssueReward.class);
    RewardsClient rewardsClient;

    public IssueReward(RewardsClient rewardsClient) {
        this.rewardsClient = rewardsClient;
    }


    public boolean issueReward(String reason, int amount, String personId) throws Exception {
        IssueRequest issueRequest = new IssueRequest(reason, "Step Token", amount, personId, IssueRequest.ReceiverType.Person);

        try {
            ServiceLoginResponse loginResponse = HTTPRequests.serviceLogin(ACCREDITATION_SERVICE_LOGIN_ID,
                    ACCREDITATION_SERVICE_LOGIN_KEY);
            String serviceAuthToken = loginResponse.getAccess_token();
            String serviceTokenType = loginResponse.getToken_type();
            IssueResponse issueResponse = rewardsClient.issueReward(issueRequest, serviceTokenType, serviceAuthToken);
            logger.info(issueResponse.toString());
            return true;
        }
        // rewards are a non-critical activity, so we simply log failure
        catch (Exception e) {
            throw e;
        }
    }
}
