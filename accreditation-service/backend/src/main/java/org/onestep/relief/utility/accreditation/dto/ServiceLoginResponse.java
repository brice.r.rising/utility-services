package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode()
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceLoginResponse {

    @JsonProperty
    private String access_token;

    @JsonProperty
    private long expires_in;

    @JsonProperty
    private String token_type;

}

