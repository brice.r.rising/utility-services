package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.List;

/**
 * AccreditationDTO Response object.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccreditationResponse {

    @JsonProperty
    private String date;

    @JsonProperty
    private String accreditationId;

    @JsonProperty
    private String entityId;

    @JsonProperty
    @Mapping("this")
    private String entityType;

    @JsonProperty
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Mapping("this")
    private String role;

    @JsonProperty
    @Mapping("this")
    private String accreditationState;

    @JsonProperty
    private List<DocumentResponse> documents;

    @JsonProperty("adminNote")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private NoteDTO adminNote;

    @JsonProperty("userNote")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private NoteDTO userNote;

}

