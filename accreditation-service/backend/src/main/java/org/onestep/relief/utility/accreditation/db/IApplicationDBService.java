package org.onestep.relief.utility.accreditation.db;

import org.onestep.relief.utility.accreditation.model.Accreditation;

import java.util.List;

public interface IApplicationDBService {

    Accreditation load(String accreditationId);

    String save(Accreditation a);

    void delete(String AccreditationId);

    List<Accreditation> queryByState(String state, String entityType, String applicationDate);

    List<Accreditation> queryByEntityId(String entityId);
    //FIXME Why do we need this AND load(String accreditationId)?
    Accreditation queryByApplicationId(String accreditationId) throws IndexOutOfBoundsException;

    List<Accreditation> retrieveAllApplications();

    List<Accreditation> queryByRole(String role);

    List<String> queryByEntityIdUniqueRoles(String entityId);






}
