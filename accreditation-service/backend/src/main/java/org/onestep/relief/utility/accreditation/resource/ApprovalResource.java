package org.onestep.relief.utility.accreditation.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/onestep/accrediation/approval")
public class ApprovalResource {

    // DEBUG
    public static String file;

    /**
     * GET Download created unsigned transaction
     *
     * @param applicationId application id
     * @param token         auth token
     * @param publicAddress public algo address of the signer
     * @return json         response
     */
    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@HeaderParam("Authorization") String token, @QueryParam("applicationId") String applicationId, @QueryParam("publicAddress") String publicAddress) throws JsonProcessingException {

        // DEBUG
        System.out.println("GET called");
        System.out.println("DATA RECEIVED");
        System.out.println(token);
        System.out.println(applicationId);
        System.out.println(publicAddress);
        System.out.println(new JSONObject().put("authToken", token)
                .put("applicationId", applicationId)
                .put("publicAddress", publicAddress)
                .toString());
        System.out.println(file);

        try {
            // Make unsigned transaction
            // TODO

            return Response.status(200)
                    .entity(
                            new JSONObject() // DEBUG
                                    .put("authToken", token)
                                    .put("applicationId", applicationId)
                                    .put("publicAddress", publicAddress)
                                    .toString())
                    .build();

        } catch (Exception e) {
            // not ok
            return Response.status(500).entity(new JSONObject().put("error", "download error").toString()).build();
        }
    }

    /**
     * POST Upload signed transaction
     *
     * @param token  auth token
     * @param txJson json wrapped unsigned transaction file
     * @return json     response
     */
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(@HeaderParam("Authorization") String token, TxJson txJson) {

        // DEBUG
        System.out.println("POST called");
        System.out.println(token);
        System.out.println(txJson.encodedTxn.substring(0, 50));
        file = txJson.encodedTxn;

        // check token
        // TODO

        // decode and process txn
        // TODO

        // return response
        try {
            return Response.status(200)
                    .entity(
                            new JSONObject()
                                    .put("authToken", token)
                                    .put("file", txJson.encodedTxn.substring(0, 50))
                                    .toString())
                    .build();
        } catch (Exception e) {
            return Response.status(500).entity(new JSONObject().put("error", "upload error").toString()).build();
        }
    }


    // Json to create unsigned txn
    @AllArgsConstructor
    @NoArgsConstructor
    public class CreateTxJson {

        @JsonProperty("applicationId")
        public String applicationId;

        @JsonProperty("publicAddress")
        public String publicAddress;

        @Override
        public String toString() {
            return "applicationJson {" +
                    ", applicationId='" + applicationId + '\'' +
                    ", publicAddress='" + publicAddress + '\'' +
                    '}';
        }
    }


    // Json to upload txn
    @AllArgsConstructor
    @NoArgsConstructor
    public class TxJson {

        @JsonProperty("encodedTxn")
        public String encodedTxn;

        @Override
        public String toString() {
            return "TxJson{" +
                    "encodedTxn='" + encodedTxn + '\'' +
                    '}';
        }
    }
}


