package org.onestep.relief.utility.accreditation.util;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.*;
import static org.onestep.relief.utility.accreditation.util.Constants.TEMPFILES;

public class CreateURL {

    private static final Logger logger = LoggerFactory.getLogger(CreateURL.class);

    public static String createPresignedURL(String fileName) {

        URL url = null;

        try {

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(S3_REGION)
                    .withCredentials(new AWSStaticCredentialsProvider(new
                            BasicAWSCredentials(ACCREDITATION_S3_ACCESS_KEY, ACCREDITATION_S3_SECRET_ACCESS_KEY)))
                    .build();

            // Set the presigned URL to expire after 12 hours.
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 60 *12;
            expiration.setTime(expTimeMillis);

            // Generate the presigned URL.
            logger.info("Generating pre-signed URL.");
            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(TEMPFILES,
                            fileName)
                            .withMethod(HttpMethod.GET)
                            .withExpiration(expiration);
            url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

            logger.info("Pre-Signed URL: " + url.toString());
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            logger.warn("Unable to create url " + e.getMessage());
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            logger.warn("Unable to create url " + e.getMessage());
        }

        if (url != null) {
            return url.toString();
        } else return "";

    }
}
