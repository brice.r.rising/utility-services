package org.onestep.relief.utility.accreditation;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.onestep.relief.utility.accreditation.db.ApplicationDBService;
import org.onestep.relief.utility.accreditation.resource.*;
import org.onestep.relief.utility.accreditation.health.AppHealthCheck;
import org.onestep.relief.utility.accreditation.service.impl.AccreditationService;
import org.onestep.relief.utility.accreditation.util.HTTPRequests;
import org.onestep.relief.utility.accreditation.util.IssueReward;
import org.onestep.relief.utility.accreditation.util.RewardsClient;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.ws.rs.client.Client;
import java.util.EnumSet;

public class AccreditationServiceApplication extends Application<AccreditationServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new AccreditationServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "AccreditationService";
    }

    @Override
    public void run(final AccreditationServiceConfiguration configuration,
                    final Environment environment) {

        //Now we added REST Client Resource named RESTClientController
        final Client client = new JerseyClientBuilder().build();
        
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // enable CORS 
	    cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        //Application health check
        environment.healthChecks().register("AppHealthCheck", new AppHealthCheck());
        environment.jersey().register(new ApplicationResource(new AccreditationService(new ApplicationDBService(),
                new IssueReward(new RewardsClient()))));
        environment.jersey().register(new ApprovalResource());
        environment.jersey().register(MultiPartFeature.class);
    }

}
