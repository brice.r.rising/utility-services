package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueResponseData {

    @JsonProperty
    private String reason;

    @JsonProperty
    private String rewardType;

    @JsonProperty
    private Integer amount;

    @JsonProperty
    private String issuerId;

    @JsonProperty
    private String receiverId;

    @JsonProperty
    private ReceiverType receiverType;

    @JsonProperty
    private String transactionId;

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date = Calendar.getInstance().getTime();

    public enum ReceiverType {
        Person,
        Org
    }

}

