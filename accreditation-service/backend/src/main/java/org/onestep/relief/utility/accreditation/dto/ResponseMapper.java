package org.onestep.relief.utility.accreditation.dto;

import org.dozer.DozerBeanMapper;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class ResponseMapper {

    final static DozerBeanMapper dozerMapper = new DozerBeanMapper();

    private static Logger logger = LoggerFactory.getLogger(ResponseMapper.class);

    /** Manually maps several fields to AccreditationResponse object
     * @param accreditation Accreditation object retrieved from database
     * @return DTO object, with default values for null fields if applicable.
     */
    public static AccreditationResponse accreditationResponseMapper(Accreditation accreditation) {

        AccreditationResponse response = dozerMapper.map(accreditation,AccreditationResponse.class);

        if (accreditation.getEntityType() != null) {
            response.setEntityType(accreditation.getEntityType().toString());
        }

        if (accreditation.getAccreditationState() == null) {
            response.setAccreditationState("Pending");  // Default if no state
        } else {
            response.setAccreditationState(accreditation.getAccreditationState().toString());
        }


        if (accreditation.getRole() != null) {
            response.setRole(accreditation.getRole().toString());
        }
        return response;
    }


}
