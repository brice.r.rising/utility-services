package org.onestep.relief.utility.accreditation.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.dozer.DozerBeanMapper;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.onestep.relief.utility.accreditation.dto.AccreditationRequest;
import org.onestep.relief.utility.accreditation.dto.NoteDTO;
import org.onestep.relief.utility.accreditation.model.ErrorResponse;
import org.onestep.relief.utility.accreditation.service.IAccreditationService;
import org.onestep.relief.utility.accreditation.service.impl.AccreditationService;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.util.List;

@Path("/onestep/accreditation/application")
@Produces(MediaType.APPLICATION_JSON)
public class ApplicationResource {

    private final DozerBeanMapper dozerMapper = new DozerBeanMapper();
    IAccreditationService service;


    /**
     * Public constructor
     *
     * @param service AccreditationService object dependency injected
     *                This should occur in {@link org.onestep.relief.utility.accreditation.AccreditationServiceApplication} class
     */
    public ApplicationResource(AccreditationService service) {
        this.service = service;
    }

    /**
     * List application by state. entityType & startDate are optional
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listApplications(@QueryParam("state") String state,
                                     @QueryParam("entityType") String entityType,
                                     @QueryParam("startDate") String startDate) {
        return Response.status(200).entity(service.listApplicationsByState(state, entityType,
                startDate)).build();
    }

    /**
     * Retrieve by role
     */
    @GET
    @Path("/{role}/role")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listApplicationsByRole(@PathParam("role") String role) {
        return Response.status(200).entity(service.listApplicationsByRole(role)).build();
    }

    /**
     * Retrieve by entityId
     */
    @GET
    @Path("/{entityId}/entity")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listApplicationsByEntity(@PathParam("entityId") String entityId) {
        return Response.status(200).entity(service.listApplicationsByEntity(entityId)).build();
    }

    /**
     * Retrieve All applications
     *
     * @return List<Accreditation>
     */
    @GET
    @Path("/allApplications")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveALlApplications() {
        return Response.status(200).entity(service.retrieveAllApplications()).build();
    }

    /**
     * Applicant submits an application
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response submitApplication(
            @FormDataParam("file") List<FormDataBodyPart> parts,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @QueryParam("version") String version,
            @FormDataParam("accreditation") String accreditationString
    ) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        AccreditationRequest accreditationRequest = objectMapper.readValue(accreditationString, AccreditationRequest.class);

        return Response.status(200).entity(service.createOrUpdateApplication(parts, accreditationRequest)).build();
    }

    /**
     * Admin approves an application
     *
     * @param accreditationId App ID to approve
     * @param version         Not implemented
     * @return Response JSON
     * @throws NotFoundException If application is not found.
     */

    @POST
    @Path("/{accreditationId}/approve")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response accreditationApplicationApprove(@PathParam("accreditationId") String accreditationId,
                                                    @QueryParam("version") String version,
                                                    NoteDTO adminNote) throws Exception {
        try {
            return Response.status(200).entity(service.accreditationApplicationApprove(accreditationId, adminNote)).build();
        } catch (IndexOutOfBoundsException e) {
            return Response.status(404)
                    .entity(new ErrorResponse("Accreditation application does not exist. " + e.getMessage())).build();
        } catch (NullPointerException n) {
            return Response.status(500)
                    .entity(new ErrorResponse("Accreditation application does not exist. " + n.getMessage())).build();
        }
    }

    @POST
    @Path("/{accreditationId}/reject")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response accreditationApplicationReject(@PathParam("accreditationId") String accreditationId,
                                                    @QueryParam("version") String version,
                                                    NoteDTO adminNote) throws Exception {
        try {
            return Response.status(200).entity(service.accreditationApplicationReject(accreditationId, adminNote)).build();
        } catch (IndexOutOfBoundsException e) {
            return Response.status(404)
                    .entity(new ErrorResponse("Accreditation application does not exist. " + e.getMessage())).build();
        } catch (NullPointerException n) {
            return Response.status(500)
                    .entity(new ErrorResponse("Accreditation application does not exist. " + n.getMessage())).build();
        }
    }


    @DELETE
    @Path("/{accreditationId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response accreditationApplicationDelete(@ApiParam(value = "Accreditation unique identifier.", required = true) @PathParam("accreditationId") String accreditationId
            , @ApiParam(value = "Accreditation Api version.", required = true) @QueryParam("version") String version)
            throws NotFoundException {


        try {
            service.accreditationApplicationDelete(accreditationId);
            return Response.status(200)
                    .entity("OK -- Accreditation application deleted.").build();
        } catch (Exception e) {
            return Response.status(204)
                    .entity(new ErrorResponse("Accreditation application does not exist. " + e.getMessage())).build();
        }
    }

    @GET
    @Path("/{accreditationId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response accreditationApplicationGetById(@ApiParam(value = "Accreditation unique identifier.", required = true) @PathParam("accreditationId") String accreditationId
            , @ApiParam(value = "Accreditation Api version.", required = true) @QueryParam("version") String version
            , @Context SecurityContext securityContext)
            throws NotFoundException, IOException {
        return Response.status(200).entity(service.listByApplicationId(accreditationId)).build();
    }

    @GET
    @Path("/roleDocumentation")
    @Produces({"application/json"})
    public Response roleDocumentation(@QueryParam("version") String version)
            throws NotFoundException {
        try {
            return Response.status(200)
                    .entity(service.listUniqueRoles(null)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to retrieve roles: " + e.getMessage())).build();
        }
    }

    @POST
    @Path("/audit")
    @Produces({"application/json"})
    public Response applicationAudit(@QueryParam("fileName") String fileName
            , @QueryParam("version") String version
            , @QueryParam("accreditationId") String accreditationId)
            throws NotFoundException {
        return Response.status(200).entity(service.applicationAudit(fileName, accreditationId)).build();
    }

    //TODO remove after UI has upgraded to POST
    @GET
    @Path("/audit")
    @Produces({"application/json"})
    public Response applicationAuditGet(@QueryParam("fileName") String fileName
            , @QueryParam("version") String version
            , @QueryParam("accreditationId") String accreditationId)
            throws NotFoundException {
        return Response.status(200).entity(service.applicationAudit(fileName, accreditationId)).build();
    }

    /**
     * Retrieve roles that user hasn't already applied for
     */
    @GET
    @Path("/{entityId}/roles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listUniqueRoles(@PathParam("entityId") String entityId) {
        try {
            return Response.status(200)
                    .entity(service.listUniqueRoles(entityId)).build();
        } catch (Exception e) {
            return Response.status(500)
                    .entity(new ErrorResponse("Unable to retrieve roles: " + e.getMessage())).build();
        }
    }

}
