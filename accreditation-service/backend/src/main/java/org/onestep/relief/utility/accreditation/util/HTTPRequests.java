package org.onestep.relief.utility.accreditation.util;


import org.apache.http.HttpStatus;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.onestep.relief.utility.accreditation.dto.ServiceLoginResponse;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Base64;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.SERVICE_LOGIN_URL;

public class HTTPRequests {

    static Client client = new JerseyClientBuilder().build();

    public HTTPRequests(Client client) {
        this.client = client;
    }


    public static Response GetRequest(String path) {

        WebTarget webTarget = client.target(path);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.get();
    }

    public  static <T> Response PostRequest(String url, String path,T request) {


        WebTarget webTarget = client.target(url).path(path);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON).buildPost(Entity.entity(request, MediaType.APPLICATION_JSON));

        Response response = invocation.invoke();

        return response;
    }

    public  static ServiceLoginResponse serviceLogin(String clientId, String secretKey) throws Exception {


        ClientConfig clientConfig = new ClientConfig();
        Client client = ClientBuilder.newClient(clientConfig);

        // encode clientId:secretKey as base64
        String concatIdKey = clientId + ":" + secretKey;
        String encodedString = Base64.getEncoder().encodeToString(concatIdKey.getBytes());


        WebTarget webTarget = client.target(SERVICE_LOGIN_URL);
        Invocation invocation = webTarget.request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic " + encodedString)
                .buildPost(Entity.json(null));
        Response response = invocation.invoke();

        if (response != null && response.getStatus() == HttpStatus.SC_OK) {
            ServiceLoginResponse serviceLoginResponse = response.readEntity(ServiceLoginResponse.class);
            return serviceLoginResponse;
        } else {
            throw new Exception(response.getStatus() + "Failed to retrieve auth token for service");
        }

    }

}

