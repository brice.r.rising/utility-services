package org.onestep.relief.utility.accreditation.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Data
@AllArgsConstructor
@DynamoDBDocument
public class Document implements Entity {

    public Document() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.date = dateFormat.format(date);
    }

    @JsonProperty("bucket")
    @DynamoDBAttribute
    private String bucket = null;

    @JsonProperty("folder")
    @DynamoDBAttribute
    private String folder = null;

    @JsonProperty("fileName")
    @DynamoDBAttribute
    private String fileName = null;

    @JsonProperty("docType")
    @DynamoDBAttribute
    private String docType = null;

    @JsonProperty("date")
    @DynamoDBAttribute
    private String date;

    @JsonProperty("txId")
    @DynamoDBAttribute
    private String txId;

    @JsonProperty("hash")  // Only stored here to make transaction. The real hash is in the blockchain
    @DynamoDBAttribute
    private String hash;

    @JsonProperty("tempLink")
    private String tempLink;

    @JsonProperty("tempLinkThumb")
    private String tempLinkThumb;

    /**
     * Builder pattern to make setting multiple items easier
     */
    public Document withHash(String hash) {
        this.hash = hash;
        return this;
    }

    public Document withFolder(String folder) {
        this.folder = folder;
        return this;
    }

    public Document withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public Document withBucket(String bucket) {
        this.bucket = bucket;
        return this;
    }

    public Document withDocType(String docType) {
        this.docType = docType;
        return this;
    }




}
