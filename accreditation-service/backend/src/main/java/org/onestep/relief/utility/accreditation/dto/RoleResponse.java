package org.onestep.relief.utility.accreditation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResponse {

    private String role;
    private List<String> documents;
    private String comments;
}
