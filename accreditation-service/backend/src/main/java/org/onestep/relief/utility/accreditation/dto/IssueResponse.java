package org.onestep.relief.utility.accreditation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueResponse {

    @JsonProperty
    private String message;

    @JsonProperty
    private IssueResponseData data;

}

