package org.onestep.relief.utility.accreditation.util;

import org.json.JSONObject;
import org.onestep.relief.utility.accreditation.db.ApplicationDBService;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.onestep.relief.utility.accreditation.model.Document;
import org.onestep.relief.utility.accreditation.service.impl.AccreditationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.s3.S3Client;

import javax.ws.rs.core.Response;
import java.util.List;

import static org.onestep.relief.utility.accreditation.util.Constants.*;

public class AuditFile {

    private static final Logger logger = LoggerFactory.getLogger(AccreditationService.class);


    public static boolean auditFile(String fileName, String accreditationId) {

        String txId = null;
        String bucketName = null;
        String folder = null;

        String blockchainHash;


        ApplicationDBService dbService = new ApplicationDBService();
        Accreditation accreditation = dbService.queryByApplicationId(accreditationId);

        // 1) Get S3 file location and transactionID from database
        List<Document> documents = accreditation.getDocuments();
        logger.info(documents.toString());

        for (Document document : documents) {

            if (document.getFileName().equals(fileName)) {
                bucketName = document.getBucket();
                folder = document.getFolder();
                txId = document.getTxId();
                logger.info(document.getFileName());
                break;
            }
        }

        // 1a) Get file from S3
        S3Client s3Client = S3Operations.createS3Client();
        byte[] fileToAudit = S3Operations.getFile(s3Client,
                folder + PATH_SLASH + fileName, bucketName);
        byte[] encryptedKey = S3Operations.getFile(s3Client,
                folder + PATH_SLASH + fileName + KEY_SUFFIX, bucketName);

        // 2) Decrypt file
        byte[] fileToAuditDecrypted = KMSOperations.decryptUsingDataKey(encryptedKey, fileToAudit);

        // 3) calculate hash
        String hashCurrent = S3Operations.calculateHash(fileToAuditDecrypted);
        logger.info("current hash "+ hashCurrent);

        // 4) get hash from blockchain
        String params = TXN_PREFIX + txId;

        Response response = HTTPRequests.GetRequest(BLOCKCHAIN_URI + TXN_URI + "?" + params);
        JSONObject object = new JSONObject(response.readEntity(String.class));
        blockchainHash = (String) object.get("note");

        // 5) compare hashes

        return hashCurrent.equals(blockchainHash);
    }
}
