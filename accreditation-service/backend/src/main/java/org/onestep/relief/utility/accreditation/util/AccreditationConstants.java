package org.onestep.relief.utility.accreditation.util;


public class AccreditationConstants {


    // KMS constants.
    public static final String ACCREDITATION_KMS_ACCESS_KEY = Config.getEnv("ACCREDITATION_KMS_ACCESS_KEY");
    public static final String ACCREDITATION_KMS_SECRET_ACCESS = Config.getEnv("ACCREDITATION_KMS_SECRET_ACCESS");
    public static final String KMS_REGION_RT = Config.getProperty("KMS_REGION_RT");
    public static final String ACCREDITATION_KMS_CMK_STRING = Config.getEnv("ACCREDITATION_KMS_CMK_STRING");

    //S3 and DynamoDb constants are created separately, although initally they have the same values.
    //Should give more flexibility in the future.
    public static final String ACCREDITATION_S3_ACCESS_KEY = Config.getEnv("ACCREDITATION_S3_ACCESS_KEY");
    public static final String ACCREDITATION_S3_SECRET_ACCESS_KEY = Config.getEnv("ACCREDITATION_S3_SECRET_ACCESS_KEY");
    public static final String S3_REGION = Config.getProperty("S3_REGION");


    public static final String ACCREDITATION_DB_ACCESS_KEY = Config.getEnv("ACCREDITATION_DB_ACCESS_KEY");
    public static final String ACCREDITATION_DB_SECRET_ACCESS_KEY = Config.getEnv("ACCREDITATION_DB_SECRET_ACCESS_KEY");
    public static final String DB_REGION = Config.getProperty("DB_REGION");



    public static final String BLOCKCHAIN_SECRETSMANAGER_ID = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_ID");
    public static final String BLOCKCHAIN_SECRETSMANAGER_SECRET = Config.getEnv("BLOCKCHAIN_SECRETSMANAGER_SECRET");
    public static final String AWS_SECRETSMANAGER_REGION = Config.getProperty("AWS_SECRETSMANAGER_REGION");

    // This is the name of the 'key' stored in AWS Secrets Manager, as there can be multiple keys to fetch
    public static final String SECRETS_MANAGER_KEY = Config.getProperty("SECRETS_MANAGER_KEY");


    public static final String DELETED = Config.getProperty("DELETED");

    public static final String ENTITY_ID_INDEX = Config.getProperty("ENTITY_ID_INDEX");

    public static final String ROLE_INDEX = Config.getProperty("ROLE_INDEX");

    public static final String ACCREDITATION_ENTITYTYPE_INDEX = Config.getProperty("ACCREDITATION_ENTITYTYPE_INDEX");

    public static final String ACCREDITATION_SERVICE_LOGIN_ID = Config.getEnv("ACCREDITATION_SERVICE_LOGIN_ID");

    public static final String ACCREDITATION_SERVICE_LOGIN_KEY = Config.getEnv("ACCREDITATION_SERVICE_LOGIN_KEY");

    public static final int REWARD_SMALL = Integer.parseInt(Config.getProperty("REWARD_SMALL"));

    public static final String SERVICE_LOGIN_URL = Config.getProperty("SERVICE_LOGIN_URL");

    public static final String REWARDS_URL = Config.getProperty("REWARDS_URL");

    public static final String ISSUE_PATH = Config.getProperty("ISSUE_PATH");

}
