package org.onestep.relief.utility.accreditation.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.*;
import static org.onestep.relief.utility.accreditation.util.Constants.BUCKETSUFFIX;

//TODO The class needs to throw exceptions instead of catching them. then they can eventually be passed
// back to the caller
public class S3Operations {


    static AwsBasicCredentials awsCreds = null;
    final static String suffix = BUCKETSUFFIX;

    private static final Logger logger = LoggerFactory.getLogger(S3Operations.class);

    /**
     * @return S3Client object. Needed to put/get/delete objects in S3
     */
    public static S3Client createS3Client() {
        awsCreds = AwsBasicCredentials.create(ACCREDITATION_S3_ACCESS_KEY,
                ACCREDITATION_S3_SECRET_ACCESS_KEY);

        return S3Client.builder()
                .region(Region.of(S3_REGION))
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .build();


    }


    /**
     * @param s3Client Client object to close the connection with.
     */
    public static void closeS3Client(S3Client s3Client) {
        s3Client.close();
    }


    /** Buckets already exist in S3, by the pattern 'YYYY-18411c72-b00e'
     *  This method determines the current year and then returns the appropriate bucket name to use
     *
     * @return String representing the bucket name, i.w. 2021-18411c72-b00e
     */
    public static String getBucketName() {

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("YYYY");
        String strYear = dateFormat.format(date);

        String bucketName = strYear+suffix;

        return bucketName;
    }

    /**
     * @param s3 S3Client object
     * @param objectName Name of object to save. Nominally the file name, however if objectName is of the format
     *                   xxxx/yyyyy then S3 will display 'xxxx' as a virtual directory with 'yyyy' as the file.
     * @param bucketName Bucket name. Retrieved via getBucketName(). Fixed names based upon current year.
     * @param fileToUpload Byte array of file to upload. Ideally this has been previously encrypted.
     */
    // TODO return status of putting file (boolean?)
    public static void putFile(S3Client s3,
                                      String objectName,
                                      String bucketName,
                                      byte[] fileToUpload) {

        try {
            PutObjectRequest objectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .serverSideEncryption("AES256")
                    .key(objectName)
                    .build();

            s3.putObject(objectRequest, RequestBody.fromBytes(fileToUpload));

        } catch (S3Exception e) {
            logger.warn("Unable to store object in S3" +  e.getMessage());
            throw e;
        }
    }

    /**
     * @param s3 S3Client object
     * @param objectName Name of object to save. Nominally the file name, however if objectName is of the format
     *                   xxxx/yyyyy then S3 will display 'xxxx' as a virtual directory with 'yyyy' as the file.
     * @param bucketName Bucket name. Retrieved via getBucketName(). Fixed names based upon current year.
     *
     * @return Byte array of file
     */
    public static byte[] getFile(S3Client s3,
                                 String objectName,
                                 String bucketName) {

        byte[] retrievedFile;
        try {
            GetObjectRequest objectRequest = GetObjectRequest.builder()
                    .key(objectName)
                    .bucket(bucketName)
                    .build();

            // Get the byte[] from the Amazon S3 bucket
            ResponseBytes<GetObjectResponse> objectBytes = s3.getObjectAsBytes(objectRequest);
            retrievedFile = objectBytes.asByteArray();

        } catch (S3Exception e) {
            logger.warn("Unable to get file from S3 " + e.awsErrorDetails().errorMessage());
            throw e;
        }
        return retrievedFile;
    }


    /** Given an entityId as the objectName, will delete All files for this entitity
     *
     * @param s3 S3 Client obtained from createS3Client()
     * @param objectName Normally the entityId. By passing this, ALL files for specific entity are deleted
     * @param bucketName Name of the 'year' bucket the entity's files are in
     * @throws Exception Throw for all AWS exceptions
     */
    public static  void deleteFile(S3Client s3,
                                 String objectName,
                                 String bucketName) throws Exception {

        ListObjectsRequest listObjectsRequest = ListObjectsRequest.builder()
                .bucket(bucketName)
                .prefix(objectName)
                .build();

        ListObjectsResponse objectListing = s3.listObjects(listObjectsRequest);

        if (objectListing.contents().size() == 0) {
            throw new Exception("Entity Id not found");
        } else
        {
            for (S3Object objectSummary : objectListing.contents()) {

                DeleteObjectRequest deleteObjectRequest =DeleteObjectRequest.builder()
                        .bucket(bucketName)
                        .key(objectSummary.key())
                        .build();

                s3.deleteObject(deleteObjectRequest);
            }
        }
    }

    /**
     * Helper function - Returns Murmur hash of input string. Used as it's less bytes and very little chance
     * of collisions
     *
     * @param toHash String to calculate hash of
     * @return Hash of input string.
     *
     */

    public static String calculateHash(byte[] toHash)  {

        return String.valueOf(MurmurHash.hash32(toHash, toHash.length));
    }



    /*
     * Helper function - Returns SHA-256 hash of input string.
     *
     * @param toHash String to calculate hash of
     * @return Hash of input string.
     * @throws NoSuchAlgorithmException if hash type is incorrect.
     *                                  Source - https://mkyong.com/java/java-how-to-convert-bytes-to-hex/
     */
    /*@SneakyThrows
    public static String calculateHash(byte[] toHash)  {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(toHash);
        byte[] byteHash = md.digest();

        //Convert bytes into Hex string
        StringBuilder result = new StringBuilder();
        for (byte b : byteHash) {
            result.append(String.format("%02X", b));
        }
        return result.toString();
    }*/
}
