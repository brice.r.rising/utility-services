package org.onestep.relief.utility.accreditation.util;

import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

import static org.onestep.relief.utility.accreditation.util.Constants.OUTPUT_FORMAT;
import static org.onestep.relief.utility.accreditation.util.Constants.OUTPUT_SIZE;

public class CreateThumbnail {

    private static final Logger logger = LoggerFactory.getLogger(CreateThumbnail.class);

    public static byte[] thumbnail(byte[] originalFile) throws IOException {

        InputStream is = new ByteArrayInputStream(originalFile);
        BufferedImage inputBi = null;
        BufferedImage outputBi = null;
        try {
            inputBi = ImageIO.read(is);
        } catch (IOException e) {
            logger.warn("Unable to read image ", e.getMessage());
            throw e;
        }

        try {
            outputBi = Thumbnails.of(inputBi)
                    .size(OUTPUT_SIZE, OUTPUT_SIZE)
                    .outputFormat(OUTPUT_FORMAT)
                    .asBufferedImage();
        } catch (IOException e) {
            logger.info("Unable to create thumbnail ", e.getMessage());
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(outputBi, OUTPUT_FORMAT, baos);
        } catch (IOException e) {
            logger.warn("Unable to write image ", e.getMessage());
            throw e;
        }
        return baos.toByteArray();
    }


}
