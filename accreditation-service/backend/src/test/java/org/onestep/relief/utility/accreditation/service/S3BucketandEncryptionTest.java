package org.onestep.relief.utility.accreditation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.*;
import org.onestep.relief.utility.accreditation.db.ApplicationDBService;
import org.onestep.relief.utility.accreditation.dto.AccreditationResponse;
import org.onestep.relief.utility.accreditation.model.Accreditation;
import org.onestep.relief.utility.accreditation.resource.ApplicationResource;
import org.onestep.relief.utility.accreditation.service.impl.AccreditationService;
import org.onestep.relief.utility.accreditation.util.*;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;

import javax.ws.rs.client.Client;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class S3BucketandEncryptionTest {

    final static String fileName = "Screen Shot.png";
    final static String hash = "297073252";
    static byte[] inputFile;
    static S3Client s3Client = null;
    //Now we added REST Client Resource named RESTClientController
    final Client client = new JerseyClientBuilder().build();


    @BeforeAll
    public static void setUp() {
        InputStream in = S3BucketandEncryptionTest.class.getClassLoader()
                .getResourceAsStream(fileName);
        inputFile = SdkBytes.fromInputStream(in).asByteArray();
        s3Client = S3Operations.createS3Client();

    }

    @AfterAll
    public static void tearDown() {
        s3Client.close();
    }

    @Test
    public void calculateHashTest() {
        String hashCalculated = S3Operations.calculateHash(inputFile);
        Assertions.assertEquals(hash, hashCalculated);
    }

    @Test
    public void encryptDecryptTest() throws NoSuchAlgorithmException {


        List<byte[]> keyAndFile;
        keyAndFile = KMSOperations.encryptUsingDataKey(inputFile);

        byte[] decryptedFile = KMSOperations.decryptUsingDataKey(keyAndFile.get(0), keyAndFile.get(1));

        String decryptedHash = S3Operations.calculateHash(decryptedFile);
        Assertions.assertEquals(hash, decryptedHash);
    }

    @Test
    public void clientTest() {

        Assertions.assertNotNull(s3Client);

    }

    /**
     * Tests that createBucket returns correct bucket for year; also that bucket exists
     */
    @Test
    public void bucketTest() {

        String bucketName = S3Operations.getBucketName();

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("YYYY");
        String strYear = dateFormat.format(date);

        // Test that the name created is correct for the current year
        String expectedBucketName = strYear + "-18411c72-b00e-b00e";
        Assertions.assertEquals(expectedBucketName, bucketName);

        boolean exists = false;

        HeadBucketRequest headBucketRequest = HeadBucketRequest.builder()
                .bucket(bucketName)
                .build();

        try {
            s3Client.headBucket(headBucketRequest);
            exists = true;
        } catch (NoSuchBucketException e) {

        }
        Assertions.assertTrue(exists);
    }

    @Test
    public void putFileTest() throws Exception {

        String bucketName = S3Operations.getBucketName();
        List<byte[]> keyAndFile;
        keyAndFile = KMSOperations.encryptUsingDataKey(inputFile);

        S3Operations.putFile(s3Client, fileName + "-key", bucketName, keyAndFile.get(0));
        S3Operations.putFile(s3Client, fileName, bucketName, keyAndFile.get(1));

        byte[] downloadedFile = S3Operations.getFile(s3Client, fileName, bucketName);
        byte[] downloadedKey = S3Operations.getFile(s3Client, fileName + "-key", bucketName);
        Assertions.assertNotNull(downloadedFile);
        Assertions.assertNotNull(downloadedKey);


        // Cleanup by deleting files
        S3Operations.deleteFile(s3Client, fileName, bucketName);

    }

    @Test
    public void deleteFileTest() throws Exception {

        String bucketName = S3Operations.getBucketName();
        UUID uuid = UUID.randomUUID();
        String entityId = uuid.toString().substring(0, 10);

        List<byte[]> keyAndFile;
        keyAndFile = KMSOperations.encryptUsingDataKey(inputFile);

        S3Operations.putFile(s3Client, entityId + "/" + fileName + "-key", bucketName, keyAndFile.get(0));
        S3Operations.putFile(s3Client, entityId + "/" + fileName, bucketName, keyAndFile.get(1));

        byte[] downloadedFile = S3Operations.getFile(s3Client, entityId + "/" + fileName, bucketName);
        byte[] downloadedKey = S3Operations.getFile(s3Client, entityId + "/" + fileName + "-key", bucketName);
        Assertions.assertNotNull(downloadedFile);
        Assertions.assertNotNull(downloadedKey);

        S3Operations.deleteFile(s3Client, entityId + "/", bucketName);


        Assertions.assertThrows(software.amazon.awssdk.services.s3.model.NoSuchKeyException.class,
                () -> S3Operations.getFile(s3Client, entityId + "/" + fileName, bucketName));

        Assertions.assertThrows(software.amazon.awssdk.services.s3.model.NoSuchKeyException.class,
                () -> S3Operations.getFile(s3Client, entityId + "/" + fileName + "-key", bucketName));


    }

    @Disabled("Used for troubleshooting DTO conversion")
    @Test
    public void createAppTest() {

        String application = """
                {
                    "entityId": "007cf123-e067-4dc8-adef-a6c15e94f294",
                    "userNote": {
                    "date": "2021-04-17",
                    "comments": "Documents have been provided" },
                    "documents": [{
                    "docType": "Passport",
                    "fileName": "passport1.png"
                    },
                    {
                    "docType": "Government ID",
                    "fileName": "license.jpg"
                    }],
                    "role": "Expert",
                    "entityType": "PER",
                    "accreditationState": "Pending"
                }
                """;

        ApplicationResource resource = new ApplicationResource
                (new AccreditationService(new ApplicationDBService(), new IssueReward(new RewardsClient())));

    }

    @Disabled("Used for troubleshooting DTO conversion")
    @Test
    public void mapDTOResponseTest() throws JsonProcessingException {

        final DozerBeanMapper dozerMapper = new DozerBeanMapper();

        String accreditationString = """
                                
                {
                  "date": "2021-04-18",
                  "accreditationId": "eef6612b-bdbd-401c-989b-a08ac1164996",
                  "entityId": "robert-test4-4dc8-adef-a6c15e94f294",
                  "role": "Supplier",
                  "documents": [
                      {
                        "bucket": "2021-18411c72-b00e",
                        "folder": "PER/robert-test4-4dc8-adef-a6c15e94f294",
                        "fileName": "license.jpg",
                        "docType": "Government ID",
                        "date": "2021-04-18",
                        "txId": null,
                        "hash": "815784165",
                        "tempLink": null,
                        "tempLinkThumb": null
                      },
                      {
                        "bucket": "2021-18411c72-b00e",
                        "folder": "PER/robert-test4-4dc8-adef-a6c15e94f294",
                        "fileName": "test.png",
                        "docType": "Passport",
                        "date": "2021-04-18",
                        "txId": null,
                        "hash": "-1584055462",
                        "tempLink": null,
                        "tempLinkThumb": null
                      }
                    ],
                  "accreditationState": "Pending",
                  "entityType": "ORG",
                  "adminNote": null,
                  "userNote": {
                    "comments": "test of update3",
                    "date": "2021-04-18",
                    "user": null
                  }
                }
                """;
        ObjectMapper objectMapper = new ObjectMapper();
        Accreditation accreditation = objectMapper.readValue(accreditationString, Accreditation.class);


        AccreditationResponse response = dozerMapper.map(accreditation, AccreditationResponse.class);
        response.setEntityType(accreditation.getEntityType().toString());
        response.setAccreditationState(accreditation.getAccreditationState().toString());
        response.setRole(accreditation.getRole().toString());

    }

    @Test
    @Disabled
    public void auditFileTest() {

        String accreditationId = "3ac8ab61-4d56-4bcf-8e8f-e8abd98153a3";
        String fileName = "license.jpg";

        AuditFile.auditFile(fileName, accreditationId);
    }
}
