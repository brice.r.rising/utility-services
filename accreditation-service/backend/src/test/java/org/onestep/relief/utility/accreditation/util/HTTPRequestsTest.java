package org.onestep.relief.utility.accreditation.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.onestep.relief.utility.accreditation.dto.AccountDTO;
import org.onestep.relief.utility.accreditation.dto.SendTransactionRequest;
import org.onestep.relief.utility.accreditation.dto.ServiceLoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.ACCREDITATION_SERVICE_LOGIN_ID;
import static org.onestep.relief.utility.accreditation.util.AccreditationConstants.ACCREDITATION_SERVICE_LOGIN_KEY;
import static org.onestep.relief.utility.accreditation.util.Constants.BLOCKCHAIN_URI;
import static org.onestep.relief.utility.accreditation.util.Constants.SEND_ALGO_ROUTE;

class HTTPRequestsTest {

    private static final Logger logger = LoggerFactory.getLogger(HTTPRequestsTest.class);


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    public void getRequestTest() {

        String url = BLOCKCHAIN_URI + "account/createAccount";

        Response response = HTTPRequests.GetRequest(url);
        logger.info(String.valueOf(response.getStatus()));

        assertEquals(response.getStatus(), 201);

    }

    @Test
    public void postRequestTest() {

        Map<String,Object> secretsResponse = Utils.getMnemonic();

        String mnemonic = secretsResponse.get("mnemonic").toString();
        String address = secretsResponse.get("address").toString();

        AccountDTO sender = new AccountDTO(address,mnemonic);
        AccountDTO receiver = new AccountDTO(address);
        SendTransactionRequest transactionRequest = new SendTransactionRequest();

        transactionRequest.setSender(sender)
                .setReceiver(receiver)
                .setNote("Regression Test")
                .setAmount(10L);

        Response response = HTTPRequests.PostRequest(BLOCKCHAIN_URI, SEND_ALGO_ROUTE,transactionRequest);

        assertEquals(response.getStatus(), 200);
    }

    @Test
    public void serviceLoginTest() throws Exception {

        ServiceLoginResponse response = HTTPRequests.serviceLogin(ACCREDITATION_SERVICE_LOGIN_ID,
                ACCREDITATION_SERVICE_LOGIN_KEY);

        Assertions.assertNotNull(response.getAccess_token());
    }

}