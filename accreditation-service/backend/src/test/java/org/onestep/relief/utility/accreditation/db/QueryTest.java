package org.onestep.relief.utility.accreditation.db;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.onestep.relief.utility.accreditation.model.Accreditation;

import java.util.List;
import java.util.stream.Collectors;

public class QueryTest {

    IApplicationDBService dbService = new ApplicationDBService();

    /** Deleted applications should never be returned. Ensures that they are not for various role types.
     * @param role Accreditation role
     */
    @ParameterizedTest
    @ValueSource(strings = {"Expert", "Supplier", "Community Member", "Relief Worker"})
    public void getDeletedApplicationsTest(String role) {

        List<Accreditation> results = dbService.queryByRole(role).stream()
                .filter(a -> a.getAccreditationState() == Accreditation.AccreditationStateEnum.DELETED)
                .collect(Collectors.toList());

        // Should not be any Deleted apps returned.
        Assertions.assertEquals(results.size(), 0);
    }

    /**
     * Tests that query only returns the role that was requested.
     *
     * @param role Role to return
     */
    @ParameterizedTest
    @ValueSource(strings = {"Expert", "Supplier", "Community Member", "Relief Worker"})
    public void getByRoleTest(String role) {

        List<Accreditation> results = dbService.queryByRole(role).stream()
                .filter(a -> !a.getRole().toString().equals(role))
                .collect(Collectors.toList());

        // Should not be any roles not requested.
        Assertions.assertEquals(results.size(), 0);
    }

    /**
     * Tests that query only returns the state that was requested.
     *
     * @param state App state to return
     */
    @ParameterizedTest
    @ValueSource(strings = {"Pending", "Approved", "Rejected", "More Info Needed"})
    public void getByStateTest(String state) {

        List<Accreditation> results = dbService.queryByState(state, null, null).stream()
                .filter(a -> !a.getAccreditationState().toString().equals(state))
                .collect(Collectors.toList());

        // Should not be any states not requested.
        Assertions.assertEquals(results.size(), 0);
    }
}
