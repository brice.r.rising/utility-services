#!/bin/bash
echo CONVERT MNENOMIC TO PRIVATE KEY
echo IN mnemonic.env
echo OUT key.env
export MNENOMIC=$(<mnemonic.env)
./algokey import --mnemonic "$MNENOMIC" --keyfile key.env > /dev/null 2>&1
unset MNENOMIC
