#!/usr/bin/env python3
# pip install py-algorand-sdk
from algosdk.v2client import algod
from algosdk import mnemonic
from algosdk import transaction

import json
import time
import base64
import os

print("\n")
print("SIGNED TRANSACTION signed.txn")

# read from file
txns = transaction.retrieve_from_file("./signed.txn")
signed_txn = txns[0]
print("\ntxid", ":", signed_txn.transaction.get_txid(), "\n")

for key in signed_txn.__dict__:
    print(key, ":", signed_txn.__dict__[key])

print("\n")
d = signed_txn.__dict__['transaction'].__dict__
for key in d:
    print(key, ":", d[key])

print("\n")
