import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'shards-ui/dist/css/shards.min.css'
import authenticationHelper from '@onestepprojects/authentication-helper'

type RewardsWidgetProps = {
  payId: string
  entityId: string
  width: number
}

type RewardsWidgetNavBarProps = {
  entityId: string
}

export const RewardsWidget = (props: RewardsWidgetProps) => {
  const [rewardsInfo, setRewardsInfo] = useState(null)

  useEffect(() => {
    async function GetRewardsInfo(entityId: string) {
      const authHelper = new authenticationHelper.AuthHelper()
      return authHelper.Post(
        `${process.env.REACT_APP_REWARDS_WIDGET_REWARDS_API_URI}/rewardlevel/calculate`,
        { rewardHolderId: entityId }
      )
    }

    if (props.payId && props.payId !== '') {
      GetRewardsInfo(props.entityId)
        .then((rewards) => {
          setRewardsInfo(rewards?.data.data)
        })
        .catch((error) =>
          console.log(
            `There was an error retrieving rewards info, details: ${error}`
          )
        )
    }
  }, [])

  console.log(`Rewards info after promise: ${JSON.stringify(rewardsInfo)}`)
  const widgetWidth = props.width ?? 100

  return props.payId && props.payId !== '' ? (
    <div className="row">
      {rewardsInfo &&
        rewardsInfo.map((reward) => (
          <div className="col-sm-3" style={{ textAlign: 'center' }}>
            <div className="card" style={{ width: `${widgetWidth}%` }}>
              <img
                className="card-img-top"
                src={reward.rewardLevel.imageUrl}
                alt={reward.rewardLevel.levelName}
              ></img>
              <div className="card-body">
                <h4 className="card-title">
                  {reward.rewardLevel.rewardType} -{' '}
                  {reward.rewardLevel.levelName}
                </h4>
                <p className="card-text">
                  {reward.rewardTotal} available points
                </p>
              </div>
            </div>
          </div>
        ))}
    </div>
  ) : (
    <span>
      <span style={{ fontWeight: 'bold' }}>PayId</span> is required to get
      rewards, please go here to setup your account:{' '}
      <a href="https://www.pagoservices.com/">
        Realtime Payment | Pago Services
      </a>
    </span>
  )
}

export const RewardsWidgetNavBar = (props: RewardsWidgetNavBarProps) => {
  const [rewardsInfo, setRewardsInfo] = useState(null)

  useEffect(() => {
    async function GetRewardsInfo(entityId: string) {
      const authHelper = new authenticationHelper.AuthHelper()
      return authHelper.Post(
        `${process.env.REACT_APP_REWARDS_WIDGET_REWARDS_API_URI}/rewardlevel/calculate`,
        { rewardHolderId: entityId }
      )
    }

    GetRewardsInfo(props.entityId)
      .then((rewards) => {
        setRewardsInfo(rewards?.data.data)
      })
      .catch((error) =>
        console.log(
          `There was an error retrieving rewards info, details: ${error}`
        )
      )
  }, [])

  console.log(`Rewards info after promise: ${JSON.stringify(rewardsInfo)}`)

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          {rewardsInfo &&
            rewardsInfo.map((reward) => (
              <li className="nav-item active">
                <span className="navbar-brand">
                  <img
                    src={reward.rewardLevel.imageUrl}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt={reward.rewardLevel.levelName}
                  />
                  {reward.rewardLevel.rewardType} -{' '}
                  {reward.rewardLevel.levelName}
                </span>
              </li>
            ))}
        </ul>
      </div>
    </nav>
  )
}
