package org.onestep.relief.utility.rewards.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.rewards.dto.CalculateRewardLevelRequest;
import org.onestep.relief.utility.rewards.dto.CalculateRewardLevelResponse;
import org.onestep.relief.utility.rewards.dto.GenericResponse;
import org.onestep.relief.utility.rewards.dto.RewardLevelDTO;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.service.RewardServiceInterface;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class RewardLevelResourceTest {

    private static final RewardServiceInterface service = mock(RewardServiceInterface.class);

    private static final ResourceExtension extension = ResourceExtension.builder()
            .addResource(new RewardLevelResource(service))
            .build();

    @Test
    public void createRewardLevelTest() {

        final RewardLevelDTO level = new RewardLevelDTO();

        when(service.createRewardLevel(any(RewardLevelDTO.class))).thenReturn(level);

        Entity<?> entity = Entity.entity(level, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/rewardlevel/create")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Created reward level successfully.");
        verify(service).createRewardLevel(any(RewardLevelDTO.class));

        when(service.createRewardLevel(any(RewardLevelDTO.class)))
                .thenThrow(new RewardServiceException("Failed to create reward level"));

        response = extension
                .target("/rewardlevel/create")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to create reward level");
        Assertions.assertEquals(response.getStatus(), 400);
    }

    @Test
    public void calculateRewardLevelTest() {

        final CalculateRewardLevelRequest request = new CalculateRewardLevelRequest();
        final CalculateRewardLevelResponse calculateRewardLevelResponse = new CalculateRewardLevelResponse();

        when(service.calculateRewardLevel(any(CalculateRewardLevelRequest.class)))
                .thenReturn(calculateRewardLevelResponse);

        Entity<?> entity = Entity.entity(request, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/rewardlevel/calculate")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Calculated reward level successfully.");
        Assertions.assertEquals(response.getStatus(), 200);
        verify(service).calculateRewardLevel(any(CalculateRewardLevelRequest.class));

        when(service.calculateRewardLevel(any(CalculateRewardLevelRequest.class)))
                .thenThrow(new RewardServiceException("Failed to calculate rewards"));

        response = extension
                .target("/rewardlevel/calculate")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to calculate rewards");
        Assertions.assertEquals(response.getStatus(), 400);
    }
}
