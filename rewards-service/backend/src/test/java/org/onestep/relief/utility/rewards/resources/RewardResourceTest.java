package org.onestep.relief.utility.rewards.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.utility.rewards.dto.*;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.service.RewardServiceInterface;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class RewardResourceTest {

    private static final RewardServiceInterface service = mock(RewardServiceInterface.class);

    private static final ResourceExtension extension = ResourceExtension.builder()
            .addResource(new RewardResource(service))
            .build();

    @Test
    public void issueRewardTest() {

        IssueRewardResponse issueRewardResponse = new IssueRewardResponse();
        IssueRewardRequest issueRewardRequest = new IssueRewardRequest();

        when(service.issueReward(any(), any(IssueRewardRequest.class)))
                .thenReturn(issueRewardResponse);

        Entity<?> entity = Entity.entity(issueRewardRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/reward/issue")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Issued reward successfully.");
        verify(service).issueReward(any(), any(IssueRewardRequest.class));

        when(service.issueReward(any(), any(IssueRewardRequest.class)))
                .thenThrow(new RewardServiceException("Failed to issue reward"));

        response = extension
                .target("/reward/issue")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to issue reward");
        Assertions.assertEquals(response.getStatus(), 400);
    }

    @Test
    public void redeemRewardTest(){

        RedeemRewardRequest redeemRewardRequest = new RedeemRewardRequest();
        RedeemRewardResponse redeemRewardResponse = new RedeemRewardResponse();

        when(service.redeemReward(any(), any(RedeemRewardRequest.class))).thenReturn(redeemRewardResponse);

        Entity<?> entity = Entity.entity(redeemRewardRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/reward/redeem")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Redeemed reward successfully.");
        verify(service).redeemReward(any(), any(RedeemRewardRequest.class));

        when(service.redeemReward(any(), any(RedeemRewardRequest.class)))
                .thenThrow(new RewardServiceException("Failed to redeem reward"));

        response = extension
                .target("/reward/redeem")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to redeem reward");
        Assertions.assertEquals(response.getStatus(), 400);
    }

    @Test
    public void showRewardsTest() {

        ShowRewardRequestDTO showRewardRequest = new ShowRewardRequestDTO();
        List<IssueRewardResponse> issueRewardResponses = Arrays.asList(new IssueRewardResponse());

        when(service.showRewards(any(ShowRewardRequestDTO.class))).thenReturn(issueRewardResponses);

        Entity<?> entity = Entity.entity(showRewardRequest, MediaType.APPLICATION_JSON_TYPE);

        Response response = extension
                .target("/reward/show")
                .request()
                .post(entity);

        GenericResponse genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Fetched rewards successfully.");
        verify(service).showRewards(any(ShowRewardRequestDTO.class));


        when(service.showRewards(any(ShowRewardRequestDTO.class)))
                .thenThrow(new RewardServiceException("Failed to show rewards"));

        response = extension
                .target("/reward/show")
                .request()
                .post(entity);

        genericResponse = response.readEntity(GenericResponse.class);

        Assertions.assertEquals(genericResponse.getMessage(), "Failed to show rewards");
        Assertions.assertEquals(response.getStatus(), 400);
    }
}
