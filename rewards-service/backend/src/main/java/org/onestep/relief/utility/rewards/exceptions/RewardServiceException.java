package org.onestep.relief.utility.rewards.exceptions;

public class RewardServiceException extends RuntimeException {

	public RewardServiceException(String message) {
		this(message, null);
	}

	public RewardServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}