package org.onestep.relief.utility.rewards.client.stubs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssetDTO {

	@JsonProperty
	private Long assetId;

	@JsonProperty
	private String txId;

}
