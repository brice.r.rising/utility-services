package org.onestep.relief.utility.rewards.db;

import org.onestep.relief.utility.rewards.exceptions.*;
import org.onestep.relief.utility.rewards.dao.*;
import org.onestep.relief.utility.rewards.RewardsServiceConfiguration;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.PutItemEnhancedRequest;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.enhanced.dynamodb.model.ScanEnhancedRequest;
import software.amazon.awssdk.enhanced.dynamodb.Expression;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.services.dynamodb.model.*;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// see https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-dynamodb-enhanced.html
public class RewardDBService implements RewardDBInterface {
	
	private static Logger logger = LoggerFactory.getLogger(RewardDBService.class);

	private String RewardTable = "Reward";
	private String RewardLevelTable = "RewardLevel";
	private String RewardTypeTable = "RewardType";

	private HashMap<String, DynamoDbTable> tableMap = new HashMap<String, DynamoDbTable>();

	public RewardDBService(RewardsServiceConfiguration config) {

		DynamoDbClient ddb = DynamoDbClient.builder()
			.region(
				Region.of(config.getAwsRegion())
			)
			.credentialsProvider(
				StaticCredentialsProvider.create(
					AwsBasicCredentials.create(config.getAwsAccessKeyID(), config.getAwsSecretAccessKey())
				)
			)
			.build();

		System.out.println(ddb);

		DynamoDbEnhancedClient enhancedClient = DynamoDbEnhancedClient.builder().dynamoDbClient(ddb).build();
		
		this.tableMap.put(RewardTable, enhancedClient.table(RewardTable, TableSchema.fromBean(RewardTransferDAO.class)));
		this.tableMap.put(RewardLevelTable, enhancedClient.table(RewardLevelTable, TableSchema.fromBean(RewardLevelDAO.class)));
		this.tableMap.put(RewardTypeTable, enhancedClient.table(RewardTypeTable, TableSchema.fromBean(RewardTypeDAO.class)));

		for (DynamoDbTable table : this.tableMap.values()) {
			try {
				table.createTable();
			} catch (ResourceInUseException e) {
				logger.info(e.getMessage());
			}
		}
	}

	public void addReward(RewardTransferDAO reward) {
		try {
			PutItemEnhancedRequest<RewardTransferDAO> req = PutItemEnhancedRequest
				.builder(RewardTransferDAO.class)
				.item(reward)
				.conditionExpression(
					Expression.builder().expression("attribute_not_exists(transactionId)").build()
				).build();
			this.tableMap.get(this.RewardTable).putItem(req);
		} catch (ConditionalCheckFailedException e) {
			throw new RewardDBException("A reward transfer with this transactionId already exists");
		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}
	}

	public List<RewardTransferDAO> queryRewards(ShowRewardRequestDAO request) {

		List<RewardTransferDAO> result = new ArrayList<RewardTransferDAO>();

		try {

			// build the key condition expression. We are guaranteed non-null receiverId from the service layer.
			QueryConditional conditional = QueryConditional.keyEqualTo(
				Key.builder().partitionValue(
					request.getReceiverId()
				).build()
			);

			List<String> clauseList = new ArrayList<String>();
			HashMap<String, AttributeValue> attributeValueMap = new HashMap<String, AttributeValue>();

			// probably there is a cleaner way to iterate over attributes via reflection
			if (request.getIssuerId() != null) {
				clauseList.add("initiatorId = :issuerId");
				attributeValueMap.put(":issuerId", AttributeValue.builder().s(request.getIssuerId()).build());
			}
			if (request.getRewardType() != null) {
				clauseList.add("rewardType = :rewardType");
				attributeValueMap.put(":rewardType", AttributeValue.builder().s(request.getRewardType()).build());
			}
			if (request.getFromDate() != null) {
				clauseList.add("transferDate >= :fromDate");
				attributeValueMap.put(":fromDate", AttributeValue.builder().n(String.valueOf(request.getFromDate())).build());
			}
			if (request.getToDate() != null) {
				clauseList.add("transferDate <= :toDate");
				attributeValueMap.put(":toDate", AttributeValue.builder().n(String.valueOf(request.getToDate())).build());
			}

			String fullExpression = String.join(" and ", clauseList);

			logger.debug("conditional = {}", conditional);
			logger.debug("expression = {}", fullExpression);
			logger.debug("attributes = {}", attributeValueMap);

			// build up the filter expression based on other request fields
			Expression expression = Expression.builder().expression(fullExpression).expressionValues(attributeValueMap).build();

			// assemble into build enhanced query
			QueryEnhancedRequest req = QueryEnhancedRequest
				.builder()
				.queryConditional(conditional)
				.filterExpression(expression)
				.build();

			Iterator<RewardTransferDAO> iterator = this.tableMap.get(this.RewardTable).query(req).items().iterator();

			while (iterator.hasNext()) {
				result.add(iterator.next());
			}

		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}

		logger.debug("result = {}", result);
		return result;
	}

	public void addRewardType(RewardTypeDAO rewardType) {
		try {

			PutItemEnhancedRequest<RewardTypeDAO> req = PutItemEnhancedRequest
				.builder(RewardTypeDAO.class)
				.item(rewardType)
				.conditionExpression(
					Expression.builder().expression("attribute_not_exists(rewardName)").build()
				).build();
			this.tableMap.get(this.RewardTypeTable).putItem(req);

		} catch (ConditionalCheckFailedException e) {
			throw new RewardDBException("A reward type with this name already exists");
		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		} 
	}

	public List<RewardTypeDAO> getRewardTypes() {

		List<RewardTypeDAO> result = new ArrayList<RewardTypeDAO>();

		try {
			Iterator<RewardTypeDAO> iterator = this.tableMap.get(this.RewardTypeTable).scan().items().iterator();

			while (iterator.hasNext()) {
				result.add(iterator.next());
			}

		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}

		return result;
	}


	public RewardTypeDAO getRewardTypeByName(String name) {

		RewardTypeDAO result;

		try {
			Key key = Key.builder().partitionValue(name).build();
			result = (RewardTypeDAO) this.tableMap.get(this.RewardTypeTable).getItem(key);
			if (result == null) {
				throw new RewardDBException("A reward type with this name does not exist");
			}
		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}

		return result;
	}


	public void addRewardLevel(RewardLevelDAO rewardLevel) {
		try {
			// check if reward type exists
			RewardTypeDAO rewardType = this.getRewardTypeByName(rewardLevel.getRewardType());

			PutItemEnhancedRequest<RewardLevelDAO> req = PutItemEnhancedRequest
				.builder(RewardLevelDAO.class)
				.item(rewardLevel)
				.conditionExpression(
					Expression.builder().expression("(levelName <> :level) or (rewardType <> :type)")
						.putExpressionValue(":level", AttributeValue.builder().s(rewardLevel.getLevelName()).build())
						.putExpressionValue(":type", AttributeValue.builder().s(rewardLevel.getRewardType()).build())
						.build()
				).build();
			this.tableMap.get(this.RewardLevelTable).putItem(req);
		} catch (ConditionalCheckFailedException e) {
			throw new RewardDBException("This reward level already exists");
		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}
	}

	@Override
	public void deleteRewardLevel(RewardLevelDAO rewardLevel){

		try {

			Key key = Key.builder()
					.partitionValue(rewardLevel.getLevelName()).build();

			this.tableMap.get(this.RewardLevelTable).deleteItem(key);

		} catch (Exception e) {
			throw new RewardDBException(e.getMessage());
		}
	}

	@Override
	public void deleteRewardType(RewardTypeDAO rewardType){

		try {

			Key key = Key.builder()
					.partitionValue(rewardType.getRewardName()).build();

			this.tableMap.get(this.RewardTypeTable).deleteItem(key);

		} catch (Exception e) {
			throw new RewardDBException(e.getMessage());
		}
	}

	public RewardLevelDAO queryRewardLevels(String rewardType, Integer rewardAmount) {
		
		RewardLevelDAO result = null;
		RewardLevelDAO curr;
		Integer highest = 0;

		try {

			// build up the filter expression for all reward levels that have minimums under rewardAmount
			Expression expression = Expression.builder().expression(
					"rewardType = :rewardType and minimumAmount <= :rewardAmount"
				)
				.putExpressionValue(":rewardType", AttributeValue.builder().s(rewardType).build())
				.putExpressionValue(":rewardAmount", AttributeValue.builder().n(String.valueOf(rewardAmount)).build())
				.build();

			// assemble into build enhanced query
			ScanEnhancedRequest req = ScanEnhancedRequest
				.builder()
				// .queryConditional(conditional)
				.filterExpression(expression)
				.build();

			Iterator<RewardLevelDAO> iterator = this.tableMap.get(this.RewardLevelTable).scan(req).items().iterator();

			// find the level with the hishest minimumAmount
			while (iterator.hasNext()) {

				curr = iterator.next();

				if (result != null) {
					if (curr.getMinimumAmount() > result.getMinimumAmount()) {
						result = curr;
					}
				} else {
					result = curr;
				}
			}

		} catch (DynamoDbException e) {
			throw new RewardDBException(e.getMessage());
		}

		return result;
	}

}