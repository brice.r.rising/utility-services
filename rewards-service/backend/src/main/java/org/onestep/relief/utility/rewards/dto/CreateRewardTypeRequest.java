package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.validation.constraints.*;

@Data
public class CreateRewardTypeRequest {

	@JsonProperty
	@NotEmpty
	private String rewardName;

	@JsonProperty
	@NotEmpty
	private String rewardUnit;
	
	@JsonProperty
	@NotNull
	@Positive
	private Integer totalAmount;

}