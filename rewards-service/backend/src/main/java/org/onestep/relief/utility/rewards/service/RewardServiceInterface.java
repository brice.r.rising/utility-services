package org.onestep.relief.utility.rewards.service;

import org.onestep.relief.utility.rewards.dto.*;
import org.onestep.relief.utility.rewards.model.RewardType;

import java.util.List;

public interface RewardServiceInterface {

	IssueRewardResponse issueReward(String token, IssueRewardRequest request);

	List<IssueRewardResponse> showRewards(ShowRewardRequestDTO request);

	CreateRewardTypeResponse createRewardType(CreateRewardTypeRequest request);

	List<CalculateRewardLevelResponse> calculateRewardLevel(CalculateRewardLevelRequest request);

	RewardLevelDTO createRewardLevel(RewardLevelDTO request);

	List<CreateRewardTypeResponse> getRewardTypes();

	void deleteRewardLevel(RewardLevelDTO request);

	void deleteRewardType(RewardType request);
}