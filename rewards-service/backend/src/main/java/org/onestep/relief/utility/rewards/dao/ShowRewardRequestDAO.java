package org.onestep.relief.utility.rewards.dao;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShowRewardRequestDAO {

	private String issuerId;

	private String receiverId;

	private String rewardType;

	private Long fromDate;

	private Long toDate;

}