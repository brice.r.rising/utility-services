package org.onestep.relief.utility.rewards.auth;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AuthorizeWith {
    
	String RoleName();

}