package org.onestep.relief.utility.rewards;

import org.onestep.relief.utility.rewards.client.impl.*;
import org.onestep.relief.utility.rewards.db.*;
import org.onestep.relief.utility.rewards.service.*;
import org.onestep.relief.utility.rewards.resources.*;

import org.onestep.relief.utility.rewards.auth.AuthorizationFilter;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import javax.servlet.*;
import java.util.EnumSet;

public class RewardsServiceApplication extends Application<RewardsServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new RewardsServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "RewardsService";
    }

    @Override
    public void initialize(final Bootstrap<RewardsServiceConfiguration> bootstrap) {
    }

    @Override
    public void run(final RewardsServiceConfiguration configuration,
                    final Environment environment) {

		// enable CORS 
		final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
		cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
		cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

		RewardServiceInterface rewardService = new RewardService(
			new RewardDBService(configuration),
			new OrganizationService(configuration.getOrganizationServiceURL()),
			new BlockchainService(configuration.getBlockchainServiceURL()),
			new AuthorizationService(configuration.getAuthzServiceURL()),
			configuration
		);

		// enable the authorization wrapper
		environment.jersey().register(AuthorizationFilter.class);

		environment.jersey().register(new RewardResource(rewardService));
		environment.jersey().register(new RewardLevelResource(rewardService));
		environment.jersey().register(new RewardTypeResource(rewardService));
    }

}
