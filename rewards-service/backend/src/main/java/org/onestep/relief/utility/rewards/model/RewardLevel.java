package org.onestep.relief.utility.rewards.model;

import lombok.Data;

@Data
public class RewardLevel {

	private String levelName;

	private String rewardType;

	private Integer minimumAmount;

	private String imageUrl;

}