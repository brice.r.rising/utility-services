package org.onestep.relief.utility.rewards.resources;

import org.onestep.relief.utility.rewards.model.RewardType;
import org.onestep.relief.utility.rewards.service.*;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.dto.*;

import org.onestep.relief.utility.rewards.auth.AuthorizeWith;

import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.validation.constraints.*;
import javax.validation.Valid;


@Path("/rewardtype") //TODO - this must be camelcase
@Produces(MediaType.APPLICATION_JSON)
public class RewardTypeResource {
	
	private RewardServiceInterface rewardService;

	public RewardTypeResource(RewardServiceInterface rewardService) {
		this.rewardService = rewardService;
	}

	@POST
	@AuthorizeWith(RoleName = "admin")
	public Response create(@NotNull @Valid CreateRewardTypeRequest request) {
		
		GenericResponse responseBody;
		CreateRewardTypeResponse rewardType;
		Response.Status status;

		try {
			rewardType = rewardService.createRewardType(request);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Created reward type successfully."), rewardType);

		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}

	@GET
	@AuthorizeWith(RoleName = "admin")
	public Response getRewardType() {
		
		GenericResponse responseBody;
		List<CreateRewardTypeResponse> rewardTypeList;
		Response.Status status;

		try {
			rewardTypeList = rewardService.getRewardTypes();
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Fetched reward types successfully."), rewardTypeList);

		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}

	@DELETE
	@AuthorizeWith(RoleName = "admin")
	public Response deleteRewardType(@NotNull @Valid RewardType rewardType){

		GenericResponse response;
		Response.Status status;

		try {
			rewardService.deleteRewardType(rewardType);
			status = Response.Status.GONE;
			response = new GenericResponse(String.format("Reward type deleted successfully."), rewardType);
		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			response = new GenericResponse(e.getMessage());
		}

		return Response.status(status).entity(response).build();
	}
}