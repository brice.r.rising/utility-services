package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.client.AuthorizationServiceInterface;
import org.onestep.relief.utility.rewards.client.stubs.*;
import org.onestep.relief.utility.rewards.exceptions.AuthorizationServiceException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.JerseyClientBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthorizationService implements AuthorizationServiceInterface {

	private static Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

	private String url;
	private Client client;

	public AuthorizationService(String url) {
		this.url = url;
		this.client = new JerseyClientBuilder().build();
	}

	public UserDTO getUserFromToken(String token) {

		UserDTO user;

		WebTarget webTarget = this.client.target(url + "/user");
		Invocation.Builder invocationBuilder =  webTarget.request()
			.header("Authorization", token)
			.accept(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		logger.debug("authz response = {}", response);

		GetUserFromTokenResponse body = response.readEntity(GetUserFromTokenResponse.class);
		
		if (response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode()) {
			user = body.getData();
			logger.info("user from authz = {}", user);
		} else {
			throw new AuthorizationServiceException(body.getMessage());
		}

		return user;
	}

}