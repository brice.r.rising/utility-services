package org.onestep.relief.utility.rewards.dao;

import lombok.Data;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.*;

@Data
@DynamoDbBean
public class RewardTypeDAO {

	private String assetId;

	private String rewardName;

	private String rewardUnit;

	private Integer totalAmount;

	@DynamoDbPartitionKey
	public String getRewardName() {
		return this.rewardName;
	}

}