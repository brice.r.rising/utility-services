package org.onestep.relief.utility.rewards.resources;

import org.onestep.relief.utility.rewards.service.*;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.dto.*;

import org.onestep.relief.utility.rewards.auth.AuthorizeWith;

import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.validation.constraints.*;
import javax.validation.Valid;


@Path("/reward")
@Produces(MediaType.APPLICATION_JSON)
public class RewardResource {
	
	private RewardServiceInterface rewardService;

	public RewardResource(RewardServiceInterface rewardService) {
		this.rewardService = rewardService;
	}

	@POST
	@Path("/issue")
	@AuthorizeWith(RoleName = "IssueReward")
	public Response issue(@HeaderParam("Authorization") String token, IssueRewardRequest request) {
		
		GenericResponse responseBody;
		IssueRewardResponse reward;
		Response.Status status;

		try {
			reward = rewardService.issueReward(token, request);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Issued reward successfully."), reward);

		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}

	@GET
	@Path("/{receiverId}")
	public Response show(@NotNull @PathParam("receiverId") String receiverId) {
		
		GenericResponse responseBody;
		List<IssueRewardResponse> rewardList;
		Response.Status status;
		ShowRewardRequestDTO request = new ShowRewardRequestDTO();

		try {
			request.setReceiverId(receiverId);
			rewardList = rewardService.showRewards(request);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Fetched rewards successfully."), rewardList);

		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}
}