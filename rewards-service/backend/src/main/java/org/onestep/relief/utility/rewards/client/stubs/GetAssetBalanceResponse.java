package org.onestep.relief.utility.rewards.client.stubs;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class GetAssetBalanceResponse {

	@JsonProperty
	private Integer algoBalance;

	@JsonProperty
	private List<AssetBalanceDTO> assets;

}