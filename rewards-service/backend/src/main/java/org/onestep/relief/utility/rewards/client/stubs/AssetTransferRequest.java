package org.onestep.relief.utility.rewards.client.stubs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssetTransferRequest {

	@JsonProperty
	private BlockchainAccountDTO sender;

	@JsonProperty
	private BlockchainAccountDTO receiver;

	@JsonProperty
	private Integer amount;

	@JsonProperty
	private String note;

	@JsonProperty
	private String assetId;

}