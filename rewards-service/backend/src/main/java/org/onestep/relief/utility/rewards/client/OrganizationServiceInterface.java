package org.onestep.relief.utility.rewards.client;

import org.onestep.relief.utility.rewards.client.stubs.PersonDTO;
import org.onestep.relief.utility.rewards.client.stubs.OrgDTO;

public interface OrganizationServiceInterface {

	public PersonDTO getPerson(String id);

	public OrgDTO getOrg(String id);

}