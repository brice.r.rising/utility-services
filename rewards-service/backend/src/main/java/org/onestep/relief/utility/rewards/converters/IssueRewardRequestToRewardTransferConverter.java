package org.onestep.relief.utility.rewards.converters;

import org.onestep.relief.utility.rewards.dto.IssueRewardRequest;
import org.onestep.relief.utility.rewards.dto.IssueRewardResponse;
import org.onestep.relief.utility.rewards.model.RewardTransfer;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import org.dozer.CustomConverter;

public class IssueRewardRequestToRewardTransferConverter implements CustomConverter {
	
	@Override
	public Object convert(Object dest, Object src, Class<?> destClass, Class<?> srcClass) {

		Object result = null;

		if (src == null) 
			return result;

		if (src instanceof IssueRewardRequest && destClass == RewardTransfer.class) {

			IssueRewardRequest req = (IssueRewardRequest) src;

			result = new RewardTransfer(
				req.getIssuerId(),
				req.getReceiverId(),
				req.getReason(),
				req.getRewardType(),
				req.getAmount(),
				RewardTransfer.Operation.REWARD,
				null,
				Long.valueOf(Instant.now().toEpochMilli()),
				RewardTransfer.ReceiverType.values()[req.getReceiverType().ordinal()]
			);

		} else if (src instanceof RewardTransfer && destClass == IssueRewardResponse.class) {

			RewardTransfer trans = (RewardTransfer) src;

			result = new IssueRewardResponse(
				trans.getReason(),
				trans.getRewardType(),
				trans.getAmount(),
				trans.getInitiatorId(),
				trans.getCollectorId(),
				IssueRewardResponse.ReceiverType.values()[trans.getReceiverType().ordinal()],
				trans.getTransactionId(),
				LocalDateTime.ofInstant(Instant.ofEpochMilli(trans.getTransferDate().longValue()), ZoneId.systemDefault())
			);

		}

		return result;
	}
}