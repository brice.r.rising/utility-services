package org.onestep.relief.utility.rewards.client.stubs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class PersonDTO {

	@JsonProperty
	private String uuid;

	@JsonProperty
	private PaymentAccountDTO paymentAccount;

}
