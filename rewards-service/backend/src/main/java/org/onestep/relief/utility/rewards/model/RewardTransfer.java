package org.onestep.relief.utility.rewards.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RewardTransfer {

	private String initiatorId;

	private String collectorId;

	private String reason;

	private String rewardType;

	private Integer amount;

	private Operation operation;

	private String transactionId;

	private Long transferDate;

	private ReceiverType receiverType;

	public enum ReceiverType {
		Person,
		Org;
	}

	public enum Operation {
		REWARD,
		REDEEM;
	}

}