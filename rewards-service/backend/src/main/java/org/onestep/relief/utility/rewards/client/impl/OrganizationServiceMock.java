package org.onestep.relief.utility.rewards.client.impl;

import org.onestep.relief.utility.rewards.exceptions.*;
import org.onestep.relief.utility.rewards.client.OrganizationServiceInterface;
import org.onestep.relief.utility.rewards.client.stubs.*;

public class OrganizationServiceMock implements OrganizationServiceInterface {


	public PersonDTO getPerson(String id) {

		PersonDTO person;

		if (id != null) {
			person = new PersonDTO(null, null);
		} else {
			throw new OrganizationServiceException("invalid person id");
		}

		return person;
	}

	public OrgDTO getOrg(String id) {
		return null;
	}

}