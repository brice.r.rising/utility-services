package org.onestep.relief.utility.rewards.auth;

import java.io.IOException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.JerseyClientBuilder;
import javax.ws.rs.core.Form;
import javax.ws.rs.client.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.glassfish.jersey.server.model.AnnotatedMethod;


public class AuthorizationFilter implements DynamicFeature {

	@Override
	public void configure(final ResourceInfo resourceInfo, final FeatureContext configuration) {
		final AnnotatedMethod method = new AnnotatedMethod(resourceInfo.getResourceMethod());

		AuthorizeWith annotation = method.getAnnotation(AuthorizeWith.class);
		if (annotation != null) {
			configuration.register(
				new AuthorizeWithRequestFilter(
					annotation.RoleName(),
					new JerseyClientBuilder().build().target(System.getenv("ONESTEP_AUTHZ_URL") + "/is-authorized")
				)
			);
			return;
		}

	}

	private static class AuthorizeWithRequestFilter implements ContainerRequestFilter {

		private static Logger logger = LoggerFactory.getLogger(AuthorizeWithRequestFilter.class);

		private final String RoleName;
		private WebTarget target;

		AuthorizeWithRequestFilter(final String RoleName, WebTarget target) {
			this.RoleName = RoleName;
			this.target = target;
			this.target = this.target.queryParam("roleName", this.RoleName);
		}

		@Override
		public void filter(final ContainerRequestContext requestContext) throws IOException {

			String token = requestContext.getHeaders().getFirst("Authorization");

			// System.out.println(token);

			logger.debug("Checking for role = {}", this.RoleName);

			Invocation.Builder invocationBuilder =  target.request()
				.header(
					"Authorization", requestContext.getHeaders().getFirst("Authorization")
				);

			Response response = invocationBuilder.get();
			logger.debug("authz response = {}", response);

			if (response.getStatusInfo().getStatusCode() == Response.Status.OK.getStatusCode()) {
				return;
			} else {
				requestContext.abortWith(response);
			}

		}

	}
}