package org.onestep.relief.utility.rewards.model;

import lombok.Data;

@Data
public class RewardType {

	private String assetId;

	private String rewardName;

	private String rewardUnit;

	private Integer totalAmount;

}