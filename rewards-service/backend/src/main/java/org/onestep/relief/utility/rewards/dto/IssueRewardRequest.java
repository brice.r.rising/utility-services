package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import javax.validation.constraints.*;

@Data
public class IssueRewardRequest {

	@JsonProperty
	@NotEmpty
	private String reason;

	@JsonProperty
	@NotEmpty
	private String rewardType;

	@JsonProperty
	@NotNull
	@Positive
	private Integer amount;

	@JsonProperty
	@NotEmpty
	private String issuerId;

	@JsonProperty
	@NotEmpty
	private String receiverId;

	@JsonProperty
	@NotEmpty
	private ReceiverType receiverType;

	public enum ReceiverType {
		Person,
		Org;
	}

}