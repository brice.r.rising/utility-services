package org.onestep.relief.utility.rewards.converters;

import org.onestep.relief.utility.rewards.dao.ShowRewardRequestDAO;
import org.onestep.relief.utility.rewards.dto.ShowRewardRequestDTO;

import java.time.LocalDateTime;
import java.time.ZoneId;
import org.dozer.CustomConverter;

public class ShowRewardRequestDTOtoDAOConverter implements CustomConverter {
	
	@Override
	public Object convert(Object dest, Object src, Class<?> destClass, Class<?> srcClass) {

		Object result = null;

		if (src == null) 
			return result;

		if (src instanceof ShowRewardRequestDTO && destClass == ShowRewardRequestDAO.class) {

			ShowRewardRequestDTO req = (ShowRewardRequestDTO) src;

			Long fromDate = Long.valueOf(-1);
			Long toDate = Long.MAX_VALUE;

			if (req.getFromDate() != null)
				fromDate = Long.valueOf(req.getFromDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
			if (req.getToDate() != null)
				toDate = Long.valueOf(req.getToDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

			result = new ShowRewardRequestDAO(
				req.getIssuerId(),
				req.getReceiverId(),
				req.getRewardType(),
				fromDate,
				toDate
			);

		}

		return result;
	}
}