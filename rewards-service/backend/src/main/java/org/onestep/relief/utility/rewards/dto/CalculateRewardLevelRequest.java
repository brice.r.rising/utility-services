package org.onestep.relief.utility.rewards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.validation.constraints.*;

@Data
public class CalculateRewardLevelRequest {

	@JsonProperty
	@NotEmpty
	private String rewardHolderId;

	// TODO
	@JsonProperty
	// @NotEmpty
	private RewardHolderType rewardHolderType;

	public enum RewardHolderType {
		Person,
		Organization;
	}

}