package org.onestep.relief.utility.rewards.client;

import org.onestep.relief.utility.rewards.client.stubs.UserDTO;

public interface AuthorizationServiceInterface {

	public UserDTO getUserFromToken(String token);

}