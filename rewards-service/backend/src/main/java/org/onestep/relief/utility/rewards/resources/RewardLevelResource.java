package org.onestep.relief.utility.rewards.resources;

import org.onestep.relief.utility.rewards.service.*;
import org.onestep.relief.utility.rewards.exceptions.RewardServiceException;
import org.onestep.relief.utility.rewards.dto.*;

import org.onestep.relief.utility.rewards.auth.AuthorizeWith;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.validation.constraints.*;
import javax.validation.Valid;


@Path("/rewardlevel")
@Produces(MediaType.APPLICATION_JSON)
public class RewardLevelResource {
	
	private RewardServiceInterface rewardService;

	public RewardLevelResource(RewardServiceInterface rewardService) {
		this.rewardService = rewardService;
	}

	@POST
	@AuthorizeWith(RoleName = "admin")
	public Response create(@NotNull @Valid RewardLevelDTO request) {
		
		GenericResponse responseBody;
		RewardLevelDTO rewardLevel;
		Response.Status status;

		try {
			rewardLevel = rewardService.createRewardLevel(request);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Created reward level successfully."), rewardLevel);

		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}


	@POST
	@Path("/calculate")
	public Response calculate(@NotNull @Valid CalculateRewardLevelRequest request) {
		
		GenericResponse responseBody;
		List<CalculateRewardLevelResponse> resp;
		Response.Status status;

		try {
			resp = rewardService.calculateRewardLevel(request);
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Calculated reward level successfully."), resp);
		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
		
		return Response.status(status).entity(responseBody).build();
	}

	@DELETE
	@AuthorizeWith(RoleName = "admin")
	public Response deleteRewardLevel(@NotNull @Valid RewardLevelDTO rewardLevel){

		GenericResponse response;
		Response.Status status;

		try {
			rewardService.deleteRewardLevel(rewardLevel);
			status = Response.Status.GONE;
			response = new GenericResponse(String.format("Reward level deleted successfully."), rewardLevel);
		} catch (RewardServiceException e) {
			status = Response.Status.BAD_REQUEST;
			response = new GenericResponse(e.getMessage());
		}

		return Response.status(status).entity(response).build();
	}
}