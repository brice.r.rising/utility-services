swagger: '2.0'
info:
  title: Reward Service
  description: The APIs listed in this specification can be used to interact with the reward service of the OneStep Relief Platform.
  version: '1.0'
host: test.onesteprelief.org
schemes:
  - https
consumes:
  - application/json
produces:
  - application/json
paths:
  '/onestep/rewards/reward':
    get:
      tags:
        - Reward
      operationId: list_rewards
      description: Get reward transactions for a single person or organzation using their id
      parameters:
        - in: body
          name: request
          description: Show reward request
          schema:
            $ref: '#/definitions/ShowRewardRequest'
      responses:
        '200':
          description: Got reward data successfully.
          schema:
            $ref: '#/definitions/ShowRewardResponse'
          examples:
            application/json:
              message: Fetched rewards successfully.
              data:
              - reason: Completed Profile
                rewardType: Step Token
                amount: 100
                issuerId: OrganizationModule
                receiverId: 98hydffc-5f17-43e7-b909-b52dsa6se184
                receiverType: Person
                transactionId: 2HB6LLJJGSNDT6QKXCXO2DKZKJEBBR7FBFT2NJPRV7PCHIOQVWFA
                date: '2021-04-24 12:40:52'
        '400':
          description: Failed to get reward data.
  '/onestep/rewards/reward/issue':
    post:
      tags:
        - Reward
      operationId: issue_reward
      description: Issue a reward to a person or organization
      parameters:
        - $ref: '#/parameters/accessTokenHdr'
        - in: body
          name: request
          description: Show reward request
          schema:
            $ref: '#/definitions/IssueRewardRequest'
      responses:
        '200':
          description: Got reward data successfully.
          schema:
            $ref: '#/definitions/IssueRewardResponse'
          examples:
            application/json:
              message: Issued reward successfully.
              data:
                reason: Completed Profile
                rewardType: Step Token
                amount: 100
                issuerId: OrganizationModule
                receiverId: 98hydffc-5f17-43e7-b909-b52dsa6se184
                receiverType: Person
                transactionId: 2HB6LLJJGSNDT6QKXCXO2DKZKJEBBR7FBFT2NJPRV7PCHIOQVWFA
                date: '2021-04-24 12:40:52'
        '400':
          description: Failed to issue reward. 
  '/onestep/rewards/rewardlevel/calculate':
    post:
      tags:
        - Reward Level
      operationId: calculate_reward_level
      description: Calculate reward levels for all reward types for a user
      parameters:
        - in: body
          name: request
          description: Show reward request
          schema:
            $ref: '#/definitions/CalculateRewardLevelRequest'
      responses:
        '200':
          description: Calculated reward level successfully.
          schema:
            $ref: '#/definitions/CalculateRewardLevelResponse'
          examples:
            application/json:
              message: Calculated reward level successfully.
              data:
              - rewardLevel:
                  levelName: Beginner
                  rewardType: Step Token
                  minimumAmount: 0
                  imageUrl: https://test.onesteprelief.org
                rewardTotal: 1380
        '400':
          description: Failed to calculate reward level. 
  '/onestep/rewards/rewardlevel':
    post:
      description: Create a reward level
      tags:
        - Reward Level
    delete:
      description: Delete a reward level
      tags:
        - Reward Level
  '/onestep/rewards/rewardtype':
    get:
      description: Show all reward types
      tags:
        - Reward Type
    post:
      description: Create a reward type
      tags:
        - Reward Type
    delete:
      description: Delete a reward type
      tags:
        - Reward Type
definitions:
  GenericResponse:
    type: object
    description: Common structure of all responses
    properties:
      message:
        readOnly: true
        type: string
        description: 'Short description of results of action'
      data:
        readOnly: true
        type: object
        description: 'Results data of the operation'
  ShowRewardRequest:
    type: object
    properties:
      rewardHolderId:
        type: string
        description: 'Person or organization ID'
  IssueRewardRequest:
    type: object
    properties:
      reason:
        type: string
        description: Reward reason
      rewardType:
        type: string
        description: Reward type name
      amount:
        type: integer
        description: Reward amount
      receiverId:
        type: string
        description: Reward receiver person or org Id
      receiverType:
        type: string
        description: Reward receiver type ("Person" or "org")
  ShowRewardResponse:
    allOf:
      - $ref: '#/definitions/GenericResponse'
    type: object
    properties:
      data:
        readOnly: true
        type: array
        description: 'User data'
        items:
          type: object
          properties:
            reason:
              type: string
              description: 'Reward reason'
            rewardType:
              type: string
              description: 'Reward type'
            amount:
              type: integer
              description: 'Reward amount'
            issuerId:
              type: string
              description: 'Reward issuer Id'
            receiverId:
              type: string
              description: 'Reward type'
            receiverType:
              type: integer
              description: 'Reward receiver type ("Person" or "org")'
            transactionId:
              type: string
              description: 'Blockchain transaction Id'
            date:
              type: string
              description: 'Reward issue date'
  IssueRewardResponse:
    allOf:
      - $ref: '#/definitions/GenericResponse'
    type: object
    properties:
      data:
        readOnly: true
        type: object
        description: 'User data'
        properties:
          reason:
            type: string
            description: 'Reward reason'
          rewardType:
            type: string
            description: 'Reward type'
          amount:
            type: integer
            description: 'Reward amount'
          issuerId:
            type: string
            description: 'Reward issuer Id'
          receiverId:
            type: string
            description: 'Reward type'
          receiverType:
            type: integer
            description: 'Reward receiver type ("Person" or "org")'
          transactionId:
            type: string
            description: 'Blockchain transaction Id'
          date:
            type: string
            description: 'Reward issue date'
  CalculateRewardLevelRequest:
    type: object
    properties:
      rewardHolderId:
        type: string
        description: 'Person or organization ID'
  CalculateRewardLevelResponse:
    allOf:
      - $ref: '#/definitions/GenericResponse'
    type: object
    properties:
      data:
        readOnly: true
        type: array
        description: 'User data'
        items:
          type: object
          properties:
            rewardLevel:
              type: object
              properties:
                levelName:
                  type: string
                  description: 'Reward level name'
                rewardType:
                  type: string
                  description: 'Reward type name'
                minimumAmount:
                  type: integer
                  description: 'Minimum reward amount for the level'
                imageUrl:
                  type: string
                  description: 'URL to the image for the reward level'
            rewardTotal:
              type: integer
              description: 'Reward balance'
parameters:
  accessTokenHdr:
    name: Authorization
    in: header
    required: true
    type: string
    description: Access token for the reward issuer. The issuer must have "IssueReward" permission in the authorization service.
