#!/bin/sh
set -e #exit immediately if a command exits with a non-zero status.
cd /rewards-service
java -jar ./target/rewards-1.0-SNAPSHOT.jar server config.yml
