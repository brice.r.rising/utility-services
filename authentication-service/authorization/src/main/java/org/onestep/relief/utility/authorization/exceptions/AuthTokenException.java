package org.onestep.relief.utility.authorization.exceptions;

public class AuthTokenException extends RuntimeException {

	public AuthTokenException(String message) {
		this(message, null);
	}

	public AuthTokenException(String message, Throwable cause) {
		super(message, cause);
	}

}