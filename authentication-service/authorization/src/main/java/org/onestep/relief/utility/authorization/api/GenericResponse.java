package org.onestep.relief.utility.authorization.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse<T> {

	public GenericResponse(String message) {
		this.message = message;
		this.data = null;
	}

	@JsonProperty
	private String message;

	@JsonProperty
	private T data;

}