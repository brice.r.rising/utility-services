package org.onestep.relief.utility.authorization.core.impl;

import org.onestep.relief.utility.authorization.exceptions.AuthTokenException;
import org.onestep.relief.utility.authorization.exceptions.AuthorizationServiceException;
import org.onestep.relief.utility.authorization.api.UserResponse;
import org.onestep.relief.utility.authorization.core.IAuthorizationService;
import org.onestep.relief.utility.authorization.core.AWSCognitoClient;
import org.onestep.relief.utility.authorization.core.AuthToken;

import software.amazon.awssdk.services.cognitoidentityprovider.model.CognitoIdentityProviderException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthorizationService implements IAuthorizationService {
	
	private static Logger logger = LoggerFactory.getLogger(AuthorizationService.class);
	private AWSCognitoClient client;

	public AuthorizationService() {
		this.client = AWSCognitoClient.getInstance();
	}

	public Boolean isAuthenticated(String token) {

		AuthToken authToken = new AuthToken(token);
		return authToken.verify();

	}

	public Boolean isAuthorized(String token, String Role) {
		
		Boolean authorized = false;
		
		if (Role != null) {
			
			logger.debug("Checking with role '{}'.", Role);

			UserResponse user = this.getUserFromToken(token);

			for (String role : user.getRoles()) {
				if (Role.equals(role)) {
					authorized = true;
					break;
				}
			}

		} else {
			logger.error("Role is null.");
		}
				
		return authorized;
	}

	public void createRole(String role) {

		try {
			this.client.createGroup(role);
		} catch (CognitoIdentityProviderException e){
			throw new AuthorizationServiceException(e.awsErrorDetails().errorMessage());
        }

	}

	public void createRoleBinding(String username, String role) {

		try {
			this.client.addUserToGroup(username, role);
		} catch (CognitoIdentityProviderException e){
			throw new AuthorizationServiceException(e.awsErrorDetails().errorMessage());
        }

	}

	public ArrayList<String> getUserRoles(String username) {

		ArrayList<String> roleList = new ArrayList<String>();

		try {
			roleList = this.client.getGroupsForUser(username);
		} catch (CognitoIdentityProviderException e){
			logger.error("AWS error: {}", e.awsErrorDetails().errorMessage());
		}

		return roleList;
	}

	public UserResponse getUserFromToken(String token) {

		AuthToken authToken = new AuthToken(token);
		final String usernamePrefix = "authz/username.";
		UserResponse user = null;
		String userId, username;
		ArrayList<String> roles;

		userId = username = null;

		if (authToken.verify()) {
			userId = authToken.getBodyValue("sub");

			if (userId == null)
				throw new AuthorizationServiceException("Failed to get userId from token");

			username = authToken.getBodyValue("username");
			logger.debug("username claim = {}", username);
			if (username == null) {
				for (String scope : authToken.getScopeList()) {
					if (scope.startsWith(usernamePrefix)) {
						username = scope.substring(usernamePrefix.length());
					}
				}
			}
			if (username == null)
				throw new AuthorizationServiceException("Failed to get username from token");

			roles = this.getUserRoles(username);
			for (String scopeRole : this.getRolesFromToken(authToken)) {
				roles.add(scopeRole);
			}

			user = new UserResponse(userId, username, roles);

		} else {
			throw new AuthorizationServiceException("Invalid token");
		}


		return user;
	}

	private ArrayList<String> getRolesFromToken(AuthToken authToken) {
		final String rolePrefix = "authz/roles.";
		ArrayList<String> roles = new ArrayList<String>();

		for (String scope : authToken.getScopeList()) {
			if (scope.startsWith(rolePrefix)) {
				roles.add(scope.substring(rolePrefix.length()));
			}
		}

		return roles;
	}

}