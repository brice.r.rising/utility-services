package org.onestep.relief.utility.authorization.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateRoleRequest {

	@JsonProperty
	private String RoleId;

}