package org.onestep.relief.utility.authorization.annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AuthorizeWith {
    
	String RoleName();
	String ResourceParamLocation() default "";
	String ResourceParam() default "";

}