package org.onestep.relief.utility.authorization.resources;

import org.onestep.relief.utility.authorization.core.impl.AuthorizationService;
import org.onestep.relief.utility.authorization.exceptions.AuthorizationServiceException;
import org.onestep.relief.utility.authorization.api.GenericResponse;
import org.onestep.relief.utility.authorization.api.CreateRoleBindingRequest;
import org.onestep.relief.utility.authorization.annotations.AuthorizeWith;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/rolebinding")
@Produces(MediaType.APPLICATION_JSON)
public class RoleBindingResource {
	
	private AuthorizationService authzService;

	public RoleBindingResource() {
		authzService = new AuthorizationService();
	}

	@POST
	@AuthorizeWith(RoleName = "admin")
	public Response createRoleBinding(CreateRoleBindingRequest request) {
		
		GenericResponse responseBody;
		Response.Status status;
							
		try {
			authzService.createRoleBinding(request.getUserName(), request.getRoleName());
			status = Response.Status.OK;
			responseBody = new GenericResponse(String.format("Added role '%s' to user '%s'", request.getRoleName(), request.getUserName()));
		} catch (AuthorizationServiceException e) {
			status = Response.Status.BAD_REQUEST;
			responseBody = new GenericResponse(e.getMessage());
		}
								
		return Response.status(status).entity(responseBody).build();
	}
}