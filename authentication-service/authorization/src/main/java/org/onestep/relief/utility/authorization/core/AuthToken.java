package org.onestep.relief.utility.authorization.core;

import org.onestep.relief.utility.authorization.GlobalConfig;
import org.onestep.relief.utility.authorization.exceptions.AuthTokenException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import java.security.interfaces.RSAPublicKey;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.InvalidPublicKeyException;
import com.auth0.jwk.JwkException;
import java.util.ArrayList;
import java.lang.NullPointerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthToken {
	
	private String tokenStr;

	private static String cognitoRegion = GlobalConfig.getConfig().getAwsConfig().getRegion();
	private static String cognitoUserPoolId = GlobalConfig.getConfig().getAwsConfig().getCognitoUserPoolId();
	private static JwkProvider jwkProvider = null;
	private static Logger logger = LoggerFactory.getLogger(AuthToken.class);

	public AuthToken(String tokenStr) {
		this.tokenStr = tokenStr;
	}

	public Boolean verify() {

		Boolean verified = false;

		if (jwkProvider == null) {
			jwkProvider = downloadJwkProvider();
		}
		
		try {
			String tokenKeyId = this.decode().getKeyId();
			logger.debug("kid from token: {}", tokenKeyId);

			Jwk jwk = jwkProvider.get(tokenKeyId);
			logger.debug("jwk: {}", jwk);

			Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
			logger.debug("jwk alg: {}", algorithm);

			String issuer = "https://cognito-idp." + cognitoRegion +".amazonaws.com/"+ cognitoUserPoolId;
			logger.debug("verifier iss: {}", issuer);

			JWTVerifier verifier = JWT.require(algorithm).withIssuer(issuer).build();

			verifier.verify(this.tokenStr);

			verified = true;
			
		} catch (JWTDecodeException | NullPointerException e) {
			logger.error("Failed to decode token: {}", e.getMessage());
		} catch (JWTVerificationException e) {
			logger.error("Token not good: {}", e.getMessage());
		} catch (InvalidPublicKeyException e) {
			logger.error("Failed to get public key in JWK: {}", e.getMessage());
		} catch (JwkException e) {
			logger.error("Failed to get kid in JWK: {}", e.getMessage());
		}		
		return verified;
	}

	public String getBodyValue(String key) {
		
		String value = null;
		
		Claim claim = this.decode().getClaim(key);

		if (! claim.isNull()) {
			value = claim.toString().replace("\"", "");
			logger.debug("Token body value for '{}' = '{}'", key, value);
		}

		return value;
	}

	public ArrayList<String> getScopeList() {

		ArrayList<String> scopeList = new ArrayList<String>();

		String scopeStr = this.getBodyValue("scope");
		if (scopeStr != null) {
			for (String scope : scopeStr.split(" ")) {
				scopeList.add(scope);
			}
		}

		return scopeList;
	}

	private DecodedJWT decode() {
		return JWT.decode(this.tokenStr);
	}

	private static JwkProvider downloadJwkProvider() {

		JwkProvider provider = null;

		String jwkStoreUrl = "https://cognito-idp." + cognitoRegion +".amazonaws.com/"+ cognitoUserPoolId +"/";

		try {
			provider = new UrlJwkProvider(jwkStoreUrl);
			logger.info("Downloaded JWKs: {}", provider);
		} catch (Exception e) {
			logger.error("Unable to download JWKs: {}", e.getMessage());
			System.exit(1);
		}

		return provider;
	}

}