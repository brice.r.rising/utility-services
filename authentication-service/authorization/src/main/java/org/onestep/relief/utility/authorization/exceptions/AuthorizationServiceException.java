package org.onestep.relief.utility.authorization.exceptions;

public class AuthorizationServiceException extends RuntimeException {

	public AuthorizationServiceException(String message) {
		this(message, null);
	}

	public AuthorizationServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}