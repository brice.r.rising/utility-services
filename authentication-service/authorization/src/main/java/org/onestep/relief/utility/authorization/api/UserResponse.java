package org.onestep.relief.utility.authorization.api;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

	@JsonProperty
	private String userId;

	@JsonProperty
	private String username;

	@JsonProperty
	private ArrayList<String> roles;

}