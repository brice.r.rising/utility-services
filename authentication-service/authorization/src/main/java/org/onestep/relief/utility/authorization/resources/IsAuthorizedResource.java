package org.onestep.relief.utility.authorization.resources;

import org.onestep.relief.utility.authorization.core.impl.AuthorizationService;
import org.onestep.relief.utility.authorization.api.GenericResponse;
import org.onestep.relief.utility.authorization.utils.HttpUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.exceptions.JWTDecodeException;


@Path("/is-authorized")
@Produces(MediaType.APPLICATION_JSON)
public class IsAuthorizedResource {
	
	private AuthorizationService authzService;

	public IsAuthorizedResource() {
		authzService = new AuthorizationService();
	}	

	@GET
	public Response isAuthorized(@HeaderParam("Authorization") String authHeader, @QueryParam("roleName") String Role) {
		
		GenericResponse responseBody;
		Response.Status status;
		
		String token = HttpUtils.getTokenFromAuthHeader(authHeader);

		if (authzService.isAuthenticated(token)) {
				if (authzService.isAuthorized(token, Role)) {
						status = Response.Status.OK;
				} else {
					status = Response.Status.FORBIDDEN;
				}
		} else {
			status = Response.Status.UNAUTHORIZED;
		}

		responseBody = new GenericResponse(status.toString());

		return Response.status(status).entity(responseBody).build();
	}
}