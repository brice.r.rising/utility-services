package org.onestep.relief.utility.authorization.core;

import org.onestep.relief.utility.authorization.GlobalConfig;

import java.util.ArrayList;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.CognitoIdentityProviderException;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminGetUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminGetUserRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.paginators.AdminListGroupsForUserIterable;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminListGroupsForUserRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminListGroupsForUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.GroupType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.CreateGroupResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.CreateGroupRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminAddUserToGroupRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminAddUserToGroupResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AWSCognitoClient {

	private static Logger logger = LoggerFactory.getLogger(AWSCognitoClient.class);
	private static AWSCognitoClient signletonClient = null;
	private static CognitoIdentityProviderClient cognitoClient = null;
	private static String cognitoUserPoolId = GlobalConfig.getConfig().getAwsConfig().getCognitoUserPoolId();
		
	public AWSCognitoClient() {
		Region regionObj = Region.of(GlobalConfig.getConfig().getAwsConfig().getRegion());
		cognitoClient = CognitoIdentityProviderClient.builder().region(regionObj).build();
	}
	
    public static AWSCognitoClient getInstance() {

		if (signletonClient == null) {
			signletonClient = new AWSCognitoClient();
        }
		return signletonClient;
    }

	
    public ArrayList<String> getGroupsForUser(String username) {
		
		ArrayList<String> groupList = new ArrayList<String>();

		AdminListGroupsForUserIterable response = cognitoClient.adminListGroupsForUserPaginator(
			AdminListGroupsForUserRequest.builder().userPoolId(cognitoUserPoolId).username(username).build()
		);
		
		for (GroupType g : response.groups()) {
			groupList.add(g.groupName());
		}
		
		return groupList;
    }
	
	
    public void createGroup(String groupname) {
		
		CreateGroupResponse response = cognitoClient.createGroup(
			CreateGroupRequest.builder().userPoolId(cognitoUserPoolId).groupName(groupname).build()
		);

    }
	
    public void addUserToGroup(String username, String groupname) {
		
		AdminAddUserToGroupResponse response = cognitoClient.adminAddUserToGroup(
			AdminAddUserToGroupRequest.builder().userPoolId(cognitoUserPoolId).username(username).groupName(groupname).build()
		);

    }
}