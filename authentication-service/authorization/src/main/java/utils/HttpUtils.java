package org.onestep.relief.utility.authorization.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtils {

	private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);
	
	public static String getTokenFromAuthHeader(String authHeader) {
		
		String token = "";

		String[] splitHeader = authHeader.split(" ");

		if (splitHeader.length == 2) {
			if (splitHeader[0].equalsIgnoreCase("bearer")) {
				token = splitHeader[1];
			}
		}

		logger.debug("parsed token from header = '{}'", token);
		return token;

	}
}