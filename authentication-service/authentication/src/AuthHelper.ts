import Axios from 'axios'
import { Auth } from 'aws-amplify'

export class AuthHelper {
  async Get(uri: string) {
    try {
      const response = await Axios.get(uri, {
        headers: await this.GetAuthToken()
      })
      console.log(response)
      return response
    } catch (error) {
      console.log(error)
      console.log(`Not able to execute get call, error: ${error}`)
      return error.response
    }
  }

  async Put(uri: string, data?: any) {
    try {
      const response = await Axios.put(uri, data, {
        headers: await this.GetAuthToken()
      })
      console.log(response)
      return response
    } catch (error) {
      console.log(error)
      console.log(`Not able to execute put call, error: ${error}`)
      return error.response
    }
  }

  async Post(uri: string, data?: any) {
    try {
      const response = await Axios.post(uri, data, {
        headers: await this.GetAuthToken()
      })
      console.log(response)
      return response
    } catch (error) {
      console.log(error)
      console.log(`Not able to execute post call, error: ${error}`)
      return error.response
    }
  }

  async Patch(uri: string, data?: any) {
    try {
      const response = await Axios.patch(uri, data, {
        headers: await this.GetAuthToken()
      })
      console.log(response)
      return response
    } catch (error) {
      console.log(error)
      console.log(`Not able to execute patch call, error: ${error} `)
      return error.response
    }
  }

  async Delete(uri: string) {
    try {
      const response = await Axios.delete(uri, {
        headers: await this.GetAuthToken()
      })
      console.log(response)
      return response
    } catch (error) {
      console.log(error)
      console.log(`Not able to execute delete call, error: ${error}`)
      return error.response
    }
  }

  async GetUserAccessToken() {
    try {
      const userInfo = await this.GetUser()
      return userInfo?.signInUserSession.accessToken.jwtToken
    } catch (error) {
      console.log(error)
      console.log(`Not able to retrieve user information, error: ${error}`)
      return error.response
    }
  }

  async GetUserIdToken() {
    try {
      const userInfo = await this.GetUser()
      return userInfo?.signInUserSession.idToken.jwtToken
    } catch (error) {
      console.log(error)
      console.log(`Not able to retrieve user information, error: ${error}`)
      return error.response
    }
  }

  private async GetUser() {
    try {
      return await Auth.currentAuthenticatedUser()
    } catch (error) {
      console.log(`Not signed in, error: ${error}`)
      throw error
    }
  }

  private async GetAuthToken() {
    try {
      const token = await this.GetUserAccessToken()
      const authHeader = {
        Authorization: `bearer ${token}`
      }

      return authHeader
    } catch (error) {
      console.log(`Not able to retrieve access token, error: ${error}`)
      throw error
    }
  }
}
